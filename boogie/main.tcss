/*
This file mainly contains visual styling, CSS rules that are important
for functionality is usually specified in DEFAULT_CSS on each widget.
*/

/* Variables */
$focus-border: outer white;


/* /\* Global *\/ */
/* * { */
/*     link-color: $primary-lighten-3; */
/* } */

Button {
    border: outer transparent;
    &.-active {
        background: $panel-darken-1;
    }
}

Button.small {
    margin: 0;
    padding: 0;
    min-width: 6;
    border: none;
    border-left: outer $panel-lighten-2;
    border-right: outer $panel-darken-3;
    width: auto;
    &.-active {
        border-left: outer $panel-darken-2;
        border-right: outer $panel-lighten-2;
    }

    &.-primary {
        border-right: outer $primary-darken-2;
        border-left: outer $primary-lighten-2;
    }

    &.-primary.-active {
        border-left: outer $primary-darken-2;
        border-right: outer $primary-lighten-2;
    }

    &.-warning {
        border-left: outer $warning-lighten-3;
        border-right: outer $warning-darken-2;
    }

    &.-warning.-active {
        border-right: outer $warning-darken-3;
        border-left: outer $warning-lighten-2;
    }

    &.-error {
        border-left: outer $error-lighten-3;
        border-right: outer $error-darken-2;
    }

    &.-error.-active {
        border-right: outer $error-darken-3;
        border-left: outer $error-lighten-2;
    }

}

Input {
    border: solid #ffffff80;
    background: transparent;
    margin: 0 1;
}
Input:focus {
    border: solid $accent;
    background: transparent;
}

Input.small {
    border: none;
    background: $background-lighten-1;
    padding: 0;
    width: 1fr;
    &:focus {
        background: $background-lighten-2;
    }
    &.-invalid {
        background: $error-darken-2;
    }
    &.-invalid:focus {
        background: $error-darken-1;
    }
}

/* Make tab bars more compact */
#root-tabs {
    &> Tabs {
        height: 1;
        background: $background;
        Tab {
            padding: 0 1;
            color: white;
            &.-active {
                opacity: 100%;
                color: white;
                background: $secondary;
            }
        }
    }
}
Tabs {
    padding: 0;
    margin: 0;
    height: 2;
    #tabs-list {
        min-height: 1;
    }
    Tab {
        margin: 0;
        height: 1;
        padding: 0 1;
    }
}
TabPane {
    padding: 0;
    height: 1fr;
}


Header {
    background: $background;
    color: #b0b0b0;
}

TextArea {
    border: none;
    padding: 0 0;
    margin: 0;
    background: $background;
    height: auto;
}

ModalScreen .frame {
    border: panel $secondary;
    border-title-color: black;
    padding: 0 1;
}

Browser {

    scrollbar-size-vertical: 1;

    Path {
        background: $secondary;
        .separator {
            color: #ffffff60
        }
    }

    /* Panels, the main parts of the browser UI */
    .panel {
        padding-left: 1;
    }

    .panel:focus-within {
        padding-left: 0;
        border-left: $focus-border;
    }

    #tree {
        background: $background-lighten-1
    }

    CustomTree {
        background: transparent;
        .tree--cursor {
            background: $accent;
            color: $background;
        }
    }

    #right {

        background: $background;
        hatch: right $background-lighten-1;

        Banner {
            /* background: $background-lighten-1; */
        }

        /* Details */
        DetailsList {
            background: $background;
            hatch: right $background-lighten-1;
            Input.pattern {
                border: solid $background-lighten-3;
                &:focus {
                    border: solid white;
                }
            }
        }
        Details {

            hatch: right $background-lighten-1;
            border-title-align: center;
            border-title-color: $text;
            border-title-style: bold;

            &.header-border {
                border-top: heavy $background-lighten-3;
                &:focus-within {
                    border-top: heavy $accent;
                }
            }

            .section {
                padding: 0 1;
                border: heavy $primary-background;
                border-title-style: bold;
                border-title-color: $text 50%;
                &:focus-within {
                    border: heavy $accent;
                    border-title-color: $accent-lighten-2;
                }
            }
            .actions {
                /* background: $background-lighten-1; */
            }
        }
    }

    Error {
        color: $error-lighten-1;
        border-top: panel $error-darken-2;
        border-title-color: $text;
        border-title-align: center;
        &:focus-within {
            border-top: panel $error;
        }
        height: auto;
        max-height: 50%;
        overflow-y: auto;
    }
}

SearchScreen {
    height: auto;
    &>Vertical {
        border: hkey $primary;
        border-title-align: center;
        border-title-color: $text;
        border-title-background: $primary;
        background: $background-lighten-1;
        height: auto;
        max-height: 1fr;
        margin: 3 6;
        Horizontal {
            width: 1fr;
            height: auto;
            padding: 1;
        }
        Input.argument {
            height: 1;
            border: hidden;
            background: transparent;
            width: 1fr;
            &:focus {
                border: none;
            }
        }
        OptionList {
            border: none;
            width: 100%;
            height: auto;
            display: none;
            background: transparent;
            margin: 1 2;
            &:focus {
                border: none;
            }
        }
    }
}
