from __future__ import annotations
import asyncio
import logging
from typing import TypeVar, Sequence, Callable, Awaitable

from textual.containers import Container, Horizontal, Vertical
from textual.widget import Widget
from textual.app import ComposeResult
from textual.css.query import NoMatches, TooManyMatches
from textual.message import Message
from textual.events import Key
from textual.binding import Binding

from .entry import Entry
from .messages import NavigateTo
from .path import Path
from .widgets.entry import EntryWidget
from .widgets.details import Banner, DetailsList, Details
from .tree import BoogieTree
from .search import SearchScreen
from .exceptions import BoogieInternalError


logger = logging.getLogger(__name__)


WidgetType = TypeVar("WidgetType", bound=EntryWidget)


class Browser(EntryWidget):
    """Main interface for a plugin root.  Displays the current path,
    a browsable tree, and details for the currently selected entry.

    Widgets for each part of the UI are found automatically from the
    plugin's widgets, by checking the widgets' generic argument.

    Then there are widget base classes "Banner" (top row), "DetailsList"
    (middle part) and "Details" (bottom). Inheriting from one of these
    determines where a widget will appear. These classes also provide
    some convenience functionality.
    """

    DEFAULT_CSS = """
    Browser {
        layout: vertical;
        height: 1fr;

        #path {
            width: 1fr;
            height: auto;
        }
        Path {
            width: 1fr;
        }
        #tree {
            width: 25%;  /* Note: specify in percent, for grow/shrink to work */
        }
        #right {
            width: 1fr;
        }
        #banner {
            height: auto;
            display: none;
        }
        #details-list {
            height: 1fr;
            display: none;
        }
        #details {
            height: 1fr;
            display: none;
        }
    }
    """

    BINDINGS = [
        Binding("ctrl+b", "create_bookmark", "Bookmark", show=False),
        Binding("alt+b", "create_bookmark_with_note", "Bookmark+", show=False),
        Binding("/", "show_search", "Search"),
    ]

    class NavigateTo(Message):
        def __init__(self, entry: Entry):
            self.entry = entry

    def __init__(
        self,
        get_title: Callable[[], Awaitable[str]],
        get_bookmark: Callable[[str], int | None],
        search=None,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.get_title = get_title
        self.get_bookmark = get_bookmark
        self.search = search
        self.rooted = asyncio.Event()

    def compose(self) -> ComposeResult:
        yield Path()
        with Horizontal(id="main"):
            yield BoogieTree(self.get_bookmark, id="tree", classes="panel")
            with Vertical(id="right"):
                yield Container(id="banner")
                yield Container(id="details-list", classes="panel")
                yield Container(id="details", classes="panel")

    def on_boogie_tree_selected(self, message: BoogieTree.Selected) -> None:
        self.post_message(NavigateTo(message.entry))

    async def on_path_selected(self, message: Path.Selected) -> None:
        # self.entry = message.entry
        # await self.query_one(BoogieTree).select_entry(message.entry)
        self.post_message(NavigateTo(message.entry))

    def _get_widgets(
        self,
        entry: Entry,
    ) -> tuple[
        tuple[Sequence[Entry], type[EntryWidget]] | None,
        tuple[Sequence[Entry], type[EntryWidget]] | None,
        tuple[Sequence[Entry], type[EntryWidget]] | None,
    ]:
        """
        Given a path, figure out what widgets we should be displaying.
        """
        banner = None
        listing = None
        details = None
        path = entry.path
        subpath: Sequence[Entry] = []
        for i, _ in enumerate(path):
            subpath = path[i:]
            entry = subpath[0]
            if listing_widget_class := DetailsList.get_widget_class(type(entry)):
                listing = (subpath, listing_widget_class)
                details = None
            elif details_widget_class := Details.get_widget_class(type(entry)):
                details = (subpath, details_widget_class)
            else:
                # # TODO old behavior was to show details for the closest parent
                # # that has a widget, if any. Kind of confusing I think, though it
                # # makes sense for the "banner" widget... not sure.
                if listing:  # if details:
                    pass
                else:
                    listing = details = None
            if entry_banner_widget_class := Banner.get_widget_class(type(entry)):
                banner = (subpath, entry_banner_widget_class)
        return banner, listing, details

    def _update_widget(
        self,
        container: Container,
        subpath: Sequence[Entry],
        widget_base_class: type[WidgetType],
        widget_class: type[WidgetType],
        reload=False,
    ) -> Widget:
        """
        Update a "right side" widget, i.e. "banner", "listing", or "details"
        """
        try:
            try:
                current_widget = container.query_one(widget_base_class)
            except TooManyMatches as e:
                # TODO can this be caught earlier?
                main, *widgets = container.query(widget_base_class)
                raise BoogieInternalError(
                    f"Oops, it seems {widget_base_class.__name__} widget {main}"
                    f" contains other {widget_base_class.__name__} widgets: "
                    + ", ".join([repr(w) for w in widgets])
                    + ". This is a bug, there must be at most one instance of"
                    + f" {widget_base_class} mounted."
                ) from e

            if current_widget.subpath == subpath:
                # Path hasn't changed, so we must already show the right widget
                if reload:
                    current_widget.set_entry(*subpath, reload=reload)
            elif isinstance(current_widget, widget_class):
                # The currently mounted widget is already of the correct type
                current_widget.set_entry(*subpath, reload=reload)
            else:
                current_widget.remove()
                self.log(
                    "Creating new widget",
                    container=container,
                    base=widget_base_class,
                    clss=widget_class,
                )
                widget = widget_class()
                container.mount(widget)
                container.display = True
                widget.set_entry(*subpath, reload=reload)
                return widget
            container.display = True
            return current_widget
        except NoMatches:
            # The container is currently not displaying a widget
            widget = widget_class()
            container.mount(widget)
            container.display = True
            widget.set_entry(*subpath, reload=reload)
            return widget

    async def _set_title(self):
        try:
            title = await asyncio.wait_for(self.get_title(), timeout=1)
            screen = self.app.get_screen("main")  # TODO typing?
            screen.set_sub_title(str(title))
        except Exception as e:
            raise RuntimeError(f"Could not get title for {self.entry}: {e}")

    async def reload(self) -> None:
        "Refresh the pane, reloading all widget data"
        assert self.entry is not None
        path = tuple(str(p) for p in self.entry.path)
        entry = await self.app.get_entry(path)
        self.entry = entry
        self._subsubpath = []
        await self.entry_changed(entry, reload=True)

        tree = self.query_one(BoogieTree)
        tree.reload()

    async def entry_changed(self, entry: Entry, reload: bool = False) -> None:
        """
        When the path changes, rearrange the UI accordingly
        """

        self.query_one(Path).path = entry.path

        tree = self.query_one(BoogieTree)
        if not self.rooted.is_set():
            self.log("Setting browser root", root=entry)
            try:
                await self._set_title()
            except RuntimeError as e:
                # If this fails, we presume the plugins can't be initialized.
                # TODO This isn't very graceful...
                self.log("Could not find title: {e}")
                self.app.notify(str(e), severity="error")
                self.rooted.set()
                return
            tree.set_entry(entry)
            self.rooted.set()
        else:
            self.run_worker(tree.select_entry(entry), exclusive=True)

        banner, listing, details = self._get_widgets(entry)

        # Update the right side parts according to the new entry
        with self.app.batch_update():
            banner_container = self.query_one("#banner", expect_type=Container)
            if banner:
                subpath, banner_widget_class = banner
                self._update_widget(
                    banner_container,
                    subpath,
                    Banner,
                    banner_widget_class,
                    reload=reload,
                )
            else:
                banner_container.display = False
                banner_container.remove_children()

            listing_container = self.query_one("#details-list", expect_type=Container)
            if listing:
                subpath, listing_widget_class = listing
                self._update_widget(
                    listing_container,
                    subpath,
                    DetailsList,
                    listing_widget_class,
                    reload=reload,
                )
            else:
                listing_container.display = False
                listing_container.remove_children()

            details_container = self.query_one("#details", expect_type=Container)
            if details:
                subpath, details_widget_class = details
                self._update_widget(
                    details_container,
                    subpath,
                    Details,
                    details_widget_class,
                    reload=reload,
                )
            else:
                details_container.display = False
                details_container.remove_children()

    async def action_create_bookmark(self) -> None:
        if self.entry:
            await self.app.create_bookmark(self.entry)

    async def action_create_bookmark_with_note(self) -> None:
        if self.entry:
            await self.app.create_bookmark_with_note(self.entry)

    async def action_show_search(self) -> None:
        """
        Pop up a search dialog, allowing the user to type parts of a name
        and select one of the matching results.
        """

        async def maybe_go_to_path(path=None):
            if path:
                entry = await self.app.get_entry(path)
                if entry:
                    self.post_message(NavigateTo(entry))

        if self.search:
            self.app.push_screen(SearchScreen(self.search), maybe_go_to_path)

    async def on_key(self, event: Key) -> None:
        if event.key == "escape":
            event.stop()
            # Focus tree widget
            (self.query_one(BoogieTree).query_one("Tree").focus())
