class BoogieInternalError(Exception):
    """An error that should not happen and is most likely a bug in Boogie itself"""
