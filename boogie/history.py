from datetime import datetime, timezone
import json
import sqlite3
from typing import Sequence, Any, Iterator

from textual.app import ComposeResult
from textual.screen import ModalScreen
from textual.widgets import DataTable, Footer, Input, Select
from textual.containers import Vertical, Grid
from textual.reactive import var

from .util import NumpyEncoderClass
from .widgets.misc import BoogieHeader


class History:
    """
    Stores path changes (currently) in a sqlite database.
    """

    def __init__(self, db_file):
        self.conn = sqlite3.connect(db_file)
        self.conn.row_factory = sqlite3.Row
        self.conn.execute("PRAGMA journal_mode = wal;")
        self.conn.execute("PRAGMA synchronous = NORMAL;")
        self.conn.execute("PRAGMA cache_size = -20000;")
        self.conn.execute("PRAGMA temp_store = MEMORY;")
        cursor = self.conn.cursor()
        # TODO check that the database has the correct format and give a helpful
        # error otherwise.
        cursor.execute(
            """
        CREATE TABLE IF NOT EXISTS history (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            timestamp TEXT DEFAULT CURRENT_TIMESTAMP,
            key TEXT NOT NULL,
            path TEXT,
            action TEXT NOT NULL,
            data TEXT
        )
        """
        )
        try:
            cursor.execute(
                """
            CREATE UNIQUE INDEX history_id_key_path
                ON history(id, key, path) ORDER BY id DESC
            CREATE UNIQUE INDEX history_id_time_key
                ON history(id, timestamp, key) ORDER BY id DESC
            CREATE UNIQUE INDEX history_action
                ON history(action)
            """
            )
        except sqlite3.OperationalError:
            pass
        self.conn.commit()

    def append(self, key: str, path: Sequence[str], action: str, data=None) -> int:
        "Add an entry to the history"
        cursor = self.conn.cursor()
        if data is not None:
            try:
                data = json.dumps(data, cls=NumpyEncoderClass)
            except Exception as e:
                print(f"Failed to JSON encode arguments: {e}")
                data = "{}"
        cursor.execute(
            "INSERT INTO history (key, path, action, data) VALUES (?, ?, ?, ?)",
            (key, ":".join(path), action, data),
        )
        self.conn.commit()
        return cursor.lastrowid

    def items(
        self,
        key: str | None = None,
        path: str | None = None,
        action: str | None = None,
        offset: int = 0,
        limit: int = 100,
    ) -> Iterator[tuple[int, str, str, str, str]]:
        "Get some path entries"
        print(f"Getting history items {offset} to {offset + limit}, action '{action}'")
        cursor = self.conn.cursor()
        query: tuple[str] | tuple[str, Sequence[Any]]
        path = "%" if not path else f"%{path.replace('', '%')}%"
        action_filter = f"AND action = '{action}'" if action else ""  # TODO fiddly
        if key:
            query = (
                f"""
            SELECT id, timestamp, path, action, data
            FROM history
            WHERE key = ? AND path LIKE ? {action_filter}
            ORDER BY id DESC
            LIMIT {limit}
            OFFSET {offset}
            """,
                (key, path),
            )
        else:
            query = (
                f"""
            SELECT id, timestamp, path, action, data
            FROM history
            WHERE path LIKE ? {action_filter}
            ORDER BY id DESC
            LIMIT {limit}
            OFFSET {offset}
            """,
                (path,),
            )
        for row in cursor.execute(*query):
            yield row

    def get_item(self, id_: int) -> tuple[int, str, str, str, str, str]:
        "Get a particular history entry"
        cursor = self.conn.cursor()
        return list(
            cursor.execute(
                """
            SELECT *
            FROM history
            WHERE id = ?
            ORDER BY id DESC
            LIMIT 1
            """,
                (id_,),
            )
        )[0]

    def get_last_matching_item(
        self, id_: int | None, path: str, action: str
    ) -> tuple[int, str, str, str, str, str]:
        cursor = self.conn.cursor()
        if id_ is None:
            return list(
                cursor.execute(
                    """
                    SELECT *
                    FROM history
                    WHERE path = ?
                    ORDER BY id DESC
                    LIMIT 1
                    """,
                    (path,),
                )
            )[0]
        else:
            return list(
                cursor.execute(
                    """
                    SELECT *
                    FROM history
                    WHERE id < ? AND path = ?
                    ORDER BY id DESC
                    LIMIT 1
                    """,
                    (
                        id_,
                        path,
                    ),
                )
            )[0]

    def get_actions(self, key: str) -> list[str]:
        cursor = self.conn.cursor()
        return [
            action
            for (action,) in cursor.execute(
                """
            SELECT DISTINCT action
            FROM history
            WHERE key = ?
            """,
                (key,),
            )
        ]

    def get_previous(self, key: str, id_: int | None = None) -> tuple[int, list[str]]:
        "Given a history id, get the previous one"
        cursor = self.conn.cursor()
        if id_ is None:
            id2, path = list(
                cursor.execute(
                    """
                    SELECT id, path
                    FROM history
                    WHERE key = ? AND action = 'path'
                    ORDER BY id DESC
                    LIMIT 1
                    """,
                    (key,),
                )
            )[0]
            return id2, path.split(":")
        else:
            item = self.get_item(id_)
            current_path = item[3]
            try:
                id2, path = next(
                    cursor.execute(
                        """
                        SELECT id, path
                        FROM history
                        WHERE key = ? AND action = 'path'
                        AND id < ? AND path != ?
                        ORDER BY id DESC
                        LIMIT 1
                        """,
                        (
                            key,
                            id_,
                            current_path,
                        ),
                    )
                )
                return id2, path.split(":")
            except StopIteration:
                raise IndexError

    def get_next(self, key, id_=None) -> tuple[int, list[str]]:
        "Given a history id, get the previous one"
        cursor = self.conn.cursor()
        if id_ is None:
            id_, path = list(
                cursor.execute(
                    """
                    SELECT id, path
                    FROM history
                    WHERE key = ? AND action = 'path'
                    ORDER BY id DESC
                    LIMIT 1
                    """,
                    (key,),
                )
            )[0]
            return id_, path.split(":")
        else:
            item = self.get_item(id_)
            current_path = item[3]
            id_, path = list(
                cursor.execute(
                    """
                    SELECT id, path
                    FROM history
                    WHERE key = ? AND action = 'path'
                    AND id > ? and path != ?
                    LIMIT 1
                    """,
                    (
                        key,
                        id_,
                        current_path,
                    ),
                )
            )[0]
            return id_, path.split(":")


class HistoryBrowser(ModalScreen):
    DEFAULT_CSS = """
    HistoryBrowser {
        Grid {
            grid-size: 2;
            height: auto;
        }
        DataTable {
            height: 1fr;
            display: none;
        }
    }
    """

    BINDINGS = [
        ("escape", "close", "Close"),
        ("m", "load_more", "Load more"),
    ]

    path_filter: var[str | None] = var(None, init=False)
    action_filter: var[str | None] = var(None, init=False)

    def __init__(
        self, history: History, key: str, current_id: int | None = None, *args, **kwargs
    ):
        self.history = history
        self.key = key
        self.current_id = current_id
        self._history_offset = 0
        self._history_limit = 100
        super().__init__(*args, **kwargs)

    def compose(self) -> ComposeResult:
        self.title = "History"
        self.sub_title = self.key
        yield BoogieHeader()
        filters = Grid(id="filters")
        # filters.display = False
        with filters:
            yield Input(id="path-filter", placeholder="Path")
            yield Select(id="action-filter", prompt="Action", options=[])
        vertical = Vertical()
        with vertical:
            dt: DataTable[tuple[str, str, str, str]] = DataTable()
            dt.add_columns("Timestamp", "Path", "Action", "Args")
            dt.cursor_type = "row"
            dt.zebra_stripes = True
            dt.display = False
            yield dt
        yield Footer()

    def on_mount(self):
        # TODO be more clever about this. Maybe some kind of
        # lazy loading?
        self.load()
        self.query_one(DataTable).display = True

    def watch_path_filter(self, _, __) -> None:
        self.load(restart=True)

    def watch_action_filter(self, _, __) -> None:
        self.load(restart=True)

    def load(self, restart=False) -> None:
        with self.app.batch_update():
            data_table = self.query_one(DataTable)
            if restart:
                self._history_offset = 0
                data_table.clear()

            paths = self.history.items(
                self.key,
                path=self.path_filter,
                action=self.action_filter,
                offset=self._history_offset,
                limit=self._history_limit,
            )
            data_table.fixed_columns = 1
            i = 0
            for i, (id_, time, path, action, args) in enumerate(paths):
                timestamp = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
                timestamp = timestamp.replace(tzinfo=timezone.utc).astimezone()
                path = path.replace(":", "·")
                data_table.add_row(
                    # id_,
                    timestamp.strftime("%Y-%m-%d %H:%M:%S"),
                    path,
                    action,
                    (args or "")[:20],
                    key=str(id_),
                )
                if self.current_id == id_:
                    data_table.move_cursor(row=i)
            # TODO if i < self._history_limit, we reached end of history.
            # Perhaps signal this somehow, e.g. disable the "more" action?
            self._history_offset += self._history_limit
            data_table.focus()

            actions = self.history.get_actions(self.key)
            self.query_one(Select).set_options(zip(actions, actions))

    def on_data_table_row_selected(self, message) -> None:
        history_id = int(message.row_key.value)
        id_, _, timestamp, path, _, _ = self.history.get_item(history_id)
        self.dismiss((id_, path.split(":")))

    def action_close(self) -> None:
        self.dismiss()

    def action_load_more(self) -> None:
        data_table = self.query_one(DataTable)
        data_table.fixed_columns = 1
        offset = self._history_offset
        self.load()
        data_table.move_cursor(row=offset)

    def on_input_submitted(self, message) -> None:
        if message.control.id == "path-filter":
            value = message.value
            if value:
                self.path_filter = message.value
            else:
                self.path_filter = None

    def on_select_changed(self, message) -> None:
        if message.control.id == "action-filter":
            value = message.value
            self.action_filter = value
