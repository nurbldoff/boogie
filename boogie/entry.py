from __future__ import annotations
from abc import ABC
from functools import lru_cache
from typing import ClassVar, Sequence, TypeVar

from rich.text import Text
from rich.style import Style
from textual.color import Color


class Entry(ABC):
    """
    Abstract base type for entries.

    Entries form hierarchies, linked via their "parent" property.

    To implement an entry, mainly the get_children/get_child methods
    should be implemented, unless the node never has children.

    Apart from that, any useful helper methods can be added, for use
    in widgets.

    Note that the entries should be considered as *immutable*; don't
    modify them after creation. To reload an entry, we instead create
    a new one. This is also why we use the "frozen" setting of the
    dataclass, to prevent accidental modification.

    Use caching to handle performance issues.
    """

    parent: Entry | None  # The Entry's parent Entry if any
    name: str  # Some string name identifying the Entry among siblings

    # If true, any children of this node won't be shown in tree
    leaf_node: ClassVar[bool] = False

    # get_children and get_child are used for navigation and
    # should be overridden, unless it's a node with no children.
    async def get_children(self) -> Sequence[Entry]:
        """
        Return all the children of the entry.
        This function may raise an exception if there's a problem,
        it will be displayed to the user so try to make it helpful.
        """
        return []

    async def get_child(self, name: str) -> Entry:
        "Return the named child of the given name, or raise KeyError"
        raise KeyError()

    # === Helpers ===

    @property
    def path(self) -> Sequence[Entry]:
        "Follow the hierarchy all the way up, giving a path to the entry"
        p = self
        path = [p]
        while p.parent:
            path.append(p.parent)
            p = p.parent
        return list(reversed(path))

    async def get_label(self) -> Text:
        "Label will be used to represent the node in the tree"
        return Text(self.name, style=self._get_label_style())

    @lru_cache(1)
    def _get_label_style(self) -> Style:
        level = len(self.path)
        style = Color.from_hsl(level / 7, 0.6, 0.8)
        return Style(color=style.rich_color)

    # === Magic methods ===

    def __str__(self) -> str:
        return self.name

    @lru_cache(1)
    def __repr__(self) -> str:
        # Note: this is used for caching, to generate keys
        # It needs to be uniquely identified with the entry
        # but should also be stable.
        # Also nice if it's human readable of course!
        return f"{type(self).__name__}({self.path_str()})"

    def path_str(self) -> str:
        return ":".join(p.name for p in self.path)

    def __eq__(self, other) -> bool:
        if isinstance(other, Entry):
            # Note: case insensitive, as this is the norm in Tango
            # TODO should this be optional per entry class?
            return self.path_str().lower() == other.path_str().lower()
        raise TypeError("Incompatible types")

    def __hash__(self) -> int:
        return hash(self.path_str())


EntryType = TypeVar("EntryType", bound=Entry)


async def get_entry(root, path_str: Sequence[str]) -> tuple[Entry, str | None]:
    step = root(None, root.name)
    path = [step]
    part: str
    # This is kind of slow, because of the way entries work we need
    # to create them in sequence. Maybe something can be done about it.
    error = None
    for part in path_str[1:]:
        try:
            step = await step.get_child(part)
        except KeyError as e:
            error = (
                str(e)
                or f"Path step '{step.name}' has no child '{part}'; truncating path"
            )
            break
        path.append(step)
    return path[-1], error
