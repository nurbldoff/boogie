from __future__ import annotations
import asyncio
from functools import cache, lru_cache
from inspect import ismethod
import logging
from time import time
from typing import Any, Awaitable

import numpy
import tango
from tango.asyncio import AttributeProxy

from boogie.cache import cached


logger = logging.getLogger(__name__)


async def get_device_proxy(devname: str) -> DeviceProxy:
    full_name = get_full_name(devname)
    return await _get_device_proxy(full_name)


@cached
async def _get_device_proxy(full_name: str) -> DeviceProxy:
    t0 = time()
    proxy = await tango.get_device_proxy(full_name, green_mode=tango.GreenMode.Asyncio)
    print(f"Got proxy for {full_name} in {time() - t0}s.")
    return proxy


@cached
async def get_attribute_proxy(devname: str, attrname: str) -> AttributeProxy:
    return await AttributeProxy(f"{devname}/{attrname}")


@cached
async def get_short_name(maybe_full_name: str) -> str:
    prefix = get_attr_prefix()
    if maybe_full_name.startswith(prefix):
        return maybe_full_name[len(prefix) :]
    return maybe_full_name


@lru_cache(1)
def get_attr_prefix() -> str:
    # db = await get_db()
    # return f"tango://{await db.get_db_host()}:{await db.get_db_port()}/"
    return f"tango://{tango.ApiUtil.get_env_var('TANGO_HOST')}/"


# @cache
# def get_full_name(maybe_full_name: str) -> str:
#     "Get the full Tango 'URL' for the attribute"
#     # TODO is this too naive?
#     attr_prefix = get_attr_prefix()
#     if maybe_full_name.startswith("tango://"):
#         return maybe_full_name.lower()
#     return f"{attr_prefix}{maybe_full_name}".lower()


@cache
def get_full_name(maybe_full_name: str) -> str:
    "Get the full Tango 'URL' for the attribute"
    # TODO is this too naive?
    attr_prefix = get_attr_prefix()
    if not maybe_full_name.lower().startswith("tango://"):
        return maybe_full_name.lower()
    # if maybe_full_name.startswith(attr_prefix):
    #     return maybe_full_name[len(attr_prefix):]
    return f"{attr_prefix}{maybe_full_name}".lower()


class AsyncTangoDatabase:
    def __init__(self, db=None):
        self.db = None
        self.done = asyncio.Event()

    async def init(self):
        tango_host = tango.ApiUtil.get_env_var("TANGO_HOST")
        print(f"Creating AsyncTangoDatabase {tango_host}")
        t0 = time()
        self.db = await asyncio.to_thread(tango.Database)
        self.done.set()
        print(f"Created AsyncTangoDatabase in {time() - t0:.3f} s.")
        return self

    async def get(self) -> AsyncTangoDatabase:
        if not self.done.is_set():
            logger.debug("Waiting for Tango db connection")
            await self.done.wait()
            logger.debug("Returning AsyncTangoDatabase")
        return self

    @cached
    async def get_uri(self) -> str:
        host = await self.get_db_host()
        port = await self.get_db_port()
        return f"tango://{host}:{port}/"

    def __getattr__(self, attr) -> Any:
        cmd = getattr(self.db, attr)

        if ismethod(cmd) and not asyncio.iscoroutine(cmd):

            def wrapper(*args):
                t0 = time()
                # TODO not sure this makes any difference, as the methods may
                # not release the GIL. See PyTango issue #620.
                result = asyncio.to_thread(cmd, *args)
                logger.debug(f"Async DB call: {attr} {args} took {time() - t0:.3e} s")
                return result

            return wrapper

    async def query(self, query: str) -> list[str]:
        "Make a SQL select query to the Tango database and return the rows"
        _, results = await self.db.command_inout(
            "DbMySQLSelect", query, green_mode=tango.GreenMode.Asyncio
        )
        return results


_db: AsyncTangoDatabase | None = None


def get_db() -> Awaitable[AsyncTangoDatabase]:
    """
    Get an async DB object.
    Make sure we only create the DB once, and return the same object
    on any subsequent calls. Even if the DB is not done when the next
    call comes in. It can take ~1s to connect to the Tango DB.
    Note that this returns a coroutine!
    """
    global _db
    if _db is None:
        _db = AsyncTangoDatabase()
        return _db.init()
    elif _db:
        return _db.get()
    else:
        raise RuntimeError("Should not happen!")


def to_pytype(value: tango.CmdArgType) -> object:
    if isinstance(value, (list, int, float, bool, str, numpy.generic)):
        return value
    devtype = type(value)
    if tango.utils.is_scalar(devtype):
        if tango.utils.is_int(devtype):
            return int(value)
        if tango.utils.is_float(devtype):
            return float(value)
        if tango.utils.is_str(devtype):
            return str(value)
        if tango.utils.is_bool(devtype):
            return bool(value)
    elif tango.utils.is_array(devtype):
        if tango.utils.is_int(devtype, inc_array=True):
            return [int(v) for v in value]
        if tango.utils.is_float(devtype, inc_array=True):
            return [float(v) for v in value]
        if tango.utils.is_str(devtype, inc_array=True):
            return [str(v) for v in value]
        if tango.utils.is_bool(devtype, inc_array=True):
            return [bool(v) for v in value]
    raise NotImplementedError(f"Unsupported type: {devtype}!")
