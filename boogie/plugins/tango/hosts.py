from __future__ import annotations
from dataclasses import dataclass
import logging

from rich.text import Text
import tango

from boogie.cache import cached
from .utils import get_db, get_device_proxy
from .entry import TangoEntry
from .servers import TangoServer, TangoServerInstance


logger = logging.getLogger("textual")


@dataclass(frozen=True, repr=False, eq=False)
class TangoHosts(TangoEntry):
    parent: None = None
    name: str = "Host"

    async def get_children(self):
        db = await get_db()
        collections = await db.query(
            """
                SELECT DISTINCT value FROM property_device
                JOIN device ON property_device.device = device.name
                WHERE class = 'Starter' AND property_device.name = 'HostCollection'
                """
        ) + ["Misc"]
        return [TangoHostCollection(self, collection) for collection in collections]

    async def get_child(self, name: str):
        return TangoHostCollection(self, name)


@dataclass(frozen=True, repr=False, eq=False)
class TangoHostCollection(TangoEntry):
    parent: TangoHosts
    name: str

    async def get_children(self):
        db = await get_db()
        if self.name == "Misc":
            starters = await db.query(
                """
            SELECT DISTINCT device.name FROM device
            WHERE class = 'Starter'
            AND NOT EXISTS (
                SELECT * FROM property_device
                WHERE device = device.name
                AND property_device.name = 'HostCollection'
            )"""
            )
        else:
            starters = await db.query(
                f"""
            SELECT DISTINCT device.name FROM device
            JOIN property_device ON property_device.device = device.name
            WHERE class = 'Starter'
            AND property_device.name = 'HostCollection'
            AND property_device.value = '{self.name}'
            """
            )
        hosts = (starter.rsplit("/")[-1] for starter in starters)
        return [TangoHost(self, host) for host in hosts]

    async def get_child(self, host):
        db = await get_db()
        hosts = {h.split(".")[0].lower() for h in await db.get_host_list()}
        if host.lower() in hosts:
            return TangoHost(self, host)
        raise KeyError(f"Could not find host {host}")


@dataclass(frozen=True, repr=False, eq=False)
class TangoHost(TangoEntry):
    """
    A Tango host. Also provides an interface to the "starer"
    instance on that host if available.
    """

    parent: TangoHostCollection
    name: str

    def get_starter(self) -> str:
        # TODO sure this is always the starter name?
        return f"tango/admin/{self.name}"

    def _parse_server(self, server_row):
        name, state, mode, level, *_ = server_row.split("\t")
        return {
            "name": name,
            "state": tango.DevState.names[state.strip()],
            "mode": int(mode),
            "level": int(level),
        }

    async def get_label(self):
        usage = await self.get_usage()
        if usage:
            return Text(f"{self.name} ({usage})", style=self._get_label_style())
        return Text(self.name, style=self._get_label_style())

    @cached
    async def get_usage(self) -> str | None:
        "Return the primary purpose of the host, if available"
        db = await get_db()
        starter_device = self.get_starter()
        result = await db.get_device_property(starter_device, "HostUsage")
        if usage := result["HostUsage"]:
            return usage[0]
        return None

    async def get_servers(self):
        proxy = await get_device_proxy(self.get_starter())
        result = await proxy.read_attribute("Servers")
        return [self._parse_server(row) for row in result.value or []]

    async def get_children(self):
        servers = await self.get_servers()
        servers = sorted(set(inst["name"].split("/")[0] for inst in servers))
        return [TangoHostServer(self, server, host=self.name) for server in servers]

    async def get_child(self, name: str):
        return TangoHostServer(self, name, host=self.name)


@dataclass(frozen=True, repr=False, eq=False)
class TangoHostServer(TangoServer):
    parent: TangoHost  # type: ignore # TODO
    name: str

    host: str = "something"

    async def get_children(self):
        db = await get_db()
        host_servers = await db.get_host_server_list(self.host)
        instances = sorted(
            {
                s.split("/")[1]
                for s in host_servers
                if s.lower().startswith(self.name.lower())
            }
        )
        return [TangoHostServerInstance(self, instance) for instance in instances]

    async def get_child(self, name: str):
        return TangoHostServerInstance(self, name)


@dataclass(frozen=True, repr=False, eq=False)
class TangoHostServerInstance(TangoServerInstance):
    pass
