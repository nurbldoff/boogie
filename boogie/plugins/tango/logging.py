"""
Listen to logging from Tango devices.

See https://tango-controls.readthedocs.io/projects/rfc/en/latest/14/Logging.html?highlight=%22-file%22

This is a HUGE hack, and is probably pretty fragile.

Uses a local "file database" device as logging target, to prevent having
to add stuff to the actual Tango database.

In principle it should always reset the logging levels of the devices to
whatever it was before. This is not extremely well tested.
"""

import asyncio
from contextlib import closing, contextmanager
from datetime import datetime
from functools import partial
from getpass import getuser
import os
import signal
import socket
import sys
from tempfile import NamedTemporaryFile
from threading import Thread
from typing import List, Dict
from queue import Queue

import tango
from tango.server import Device, command


class LoggingTarget(Device):
    """A local device that works as a Tango 'logging target'"""

    log_queue: Queue[tuple[str, ...]] = Queue()

    def init_device(self):
        print("Device is starting")

    def delete_device(self):
        print("Device is exiting")

    @command(dtype_in=[str])
    def Log(self, event):
        print("event", event)
        self.log_queue.put(event)


# Note: this will appear in the __SubDevices property of the affected device.
# This should not be a problem, but may appear confusing. Therefore it's just
# common courtesy to leave the user name so people can figure out what's
# going on.
LOGGING_DEVICE_NAME = f"zingo/{getuser()}/logger"


async def device_logs(
    devices: List[str], level: int, device_class=LoggingTarget, host=None, port=None
):
    """
    Connect to a remote device and collect logs. Optionally configure
    logging level.

    The tango logging system requires a tango device to be configured
    on the device we want logs from as a "logging target". In order to
    not have to configure a new device in the control system every time,
    we run our logging target device locally without a db.
    See https://tango-controls.readthedocs.io/en/latest/administration/deployment/without-sql-db.html
    """

    host = host or socket.gethostbyname(socket.gethostname())
    port = port or find_free_port()

    with NamedTemporaryFile(mode="w") as f:
        # Create tango dbfile needed for starting our server w/o database
        class_name = device_class.__name__
        f.write(f'{class_name}/1/DEVICE/{class_name}: "{LOGGING_DEVICE_NAME}"\n')
        f.flush()

        # Run the target device
        dev_thread = await create_device_thread(host, port, f.name)

        # Set up our device as logging target on the remote device
        logging_target = f"tango://{host}:{port}/{LOGGING_DEVICE_NAME}#dbase=no"
        logging_levels = setup_logging(devices, logging_target, level)

        # Make sure that we restore the devices to original logging state afterwards
        signal.signal(
            signal.SIGINT,
            lambda signal, frame: teardown_logging(
                dev_thread, devices, logging_target, logging_levels
            ),
        )
        while True:
            print("Fiskal")
            # Now we just wait for stuff to appear in the queue.
            try:
                while not device_class.log_queue.empty():
                    (
                        timestamp,
                        level,
                        device,
                        message,
                        _,
                        thread,
                    ) = device_class.log_queue.get()
                    t = datetime.fromtimestamp(float(timestamp) / 1000)
                    yield (t, level, device, thread, message)
                await asyncio.sleep(0.1)
            except asyncio.CancelledError:
                break

        teardown_logging(dev_thread, devices, logging_target, logging_levels)


@contextmanager
def swallow_output():
    """Context manager that redirects stderr and stdout somewhere else"""
    # This is a hack to get rid of some unwanted output

    stdout_fileno = sys.stdout.fileno()
    stdout_save = os.dup(stdout_fileno)
    stdout_pipe = os.pipe()
    os.dup2(stdout_pipe[1], stdout_fileno)
    os.close(stdout_pipe[1])

    stderr_fileno = sys.stderr.fileno()
    stderr_save = os.dup(stderr_fileno)
    stderr_pipe = os.pipe()
    os.dup2(stderr_pipe[1], stderr_fileno)
    os.close(stderr_pipe[1])

    yield

    os.close(stdout_pipe[0])
    os.dup2(stdout_save, stdout_fileno)
    os.close(stdout_save)

    os.close(stderr_pipe[0])
    os.dup2(stderr_save, stderr_fileno)
    os.close(stderr_save)


async def create_device_thread(host, port, dbfile):
    "Create a database-less log receiver device that runs in a thread."
    args = ["1", f"-file={dbfile}", "-ORBendPoint", f"giop:tcp:{host}:{port}"]
    print(args)

    thread = Thread(target=partial(LoggingTarget.run_server, args=args))

    print(thread)

    thread.start()
    await asyncio.sleep(1)

    # Tango devices always spit out some stuff on stdout when starting up,
    # we don't want to see that so we redirect output for a little while
    # with swallow_output():
    #     await asyncio.sleep(0.5)  # TODO crude...

    return thread


def setup_logging(devices: List[str], logger_device: str, logging_level: int):
    "Configure the given devices to send logs to our local device"
    print("setting up logging")
    previous_log_levels = {}
    try:
        for device in devices:
            proxy = tango.DeviceProxy(device)
            proxy.add_logging_target(f"device::{logger_device}")
            previous_log_levels[device] = proxy.get_logging_level()
            proxy.set_logging_level(logging_level)
    except Exception as e:
        print("Error", e)
    print("Logging setup done")
    return previous_log_levels


def teardown_logging(
    thread,
    devices: List[str],
    logger_device: str,
    original_logging_levels: Dict[str, int],
):
    "Remove our logging config from the devices"

    for device in devices:
        proxy = tango.DeviceProxy(device)
        proxy.remove_logging_target(f"device::{logger_device}")
        proxy.set_logging_level(original_logging_levels[device])

    # Stop the device thread
    signal.pthread_kill(thread.ident, signal.SIGKILL)
    thread.join()


def find_free_port():
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(("", 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s.getsockname()[1]


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument("device", nargs="+", help="Device(s) to start listening to.")
    parser.add_argument(
        "--level",
        type=int,
        default=4,
        help="Log level to use. 1=FATAL, 2=ERROR, 3=WARNING, 4=INFO, 5=DEBUG",
    )

    parser.add_argument(
        "--host", type=str, help="Host address to use. If not given, will try to guess."
    )
    parser.add_argument("--port", type=int, help="Port number to use.")

    args = parser.parse_args()

    host = args.host
    port = args.port

    async def run():
        log_gen = device_logs(args.device, args.level)
        async for t, level, device, thread, message in log_gen:
            print("\t".join([t.isoformat(), level, device, thread, message]))

    asyncio.run(run())
