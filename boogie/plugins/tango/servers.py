"""
This is an abstraction over a Tango control system, to turn it into a
convenient tree-like structure. It defines a hierarchy of "entries"
that can be traversed easily. Also each entry has various helper methods
and properties that wrap up some of the thornier parts of the Tango API,
and handles things like caching.
"""

from __future__ import annotations
from dataclasses import dataclass
import logging
from typing import Sequence

import tango
from tango.asyncio import DeviceProxy
from textual.app import App

from boogie.cache import cached
from .utils import get_device_proxy, get_db
from .entry import TangoEntry
from .device import TangoDevice


logger = logging.getLogger("textual")


@dataclass(frozen=True, repr=False, eq=False)
class TangoServers(TangoEntry):
    parent: TangoEntry | None = None
    name: str = "Server"

    include_dservers: bool = False

    @cached
    async def _get_servers(self) -> Sequence[str]:
        db = await get_db()
        return await db.get_server_name_list()

    async def get_children(self):
        servers = await self._get_servers()
        return [
            TangoServer(self, name=server, include_dservers=self.include_dservers)
            for server in servers
        ]

    async def get_child(self, server: str):
        try:
            servers = list(await self._get_servers())
        except tango.DevFailed as e:
            raise KeyError(f"Could not find server {server}: {e.args[0].desc}")
        if server.lower() not in {s.lower() for s in servers}:
            raise KeyError(f"No server called {server} found!")
        # TODO return proper casing
        return TangoServer(self, server, include_dservers=self.include_dservers)

    async def create(
        self, server: str, instance: str, class_devices: dict[str, Sequence[str]]
    ) -> None:
        db = await get_db()
        server_name = f"{server}/{instance}"
        dev_infos = []
        for clss, devices in class_devices.items():
            for device in devices:
                dev_info = tango.DbDevInfo()
                dev_info.server = server_name
                dev_info._class = clss
                dev_info.name = device
                dev_infos.append(dev_info)
        await db.add_server(server_name, dev_infos, with_dserver=True)

    @cached
    async def get_db_info(self):
        db = await get_db()
        return await db.get_info()


@dataclass(frozen=True, repr=False, eq=False)
class TangoServer(TangoEntry):
    parent: TangoServers
    name: str

    include_dservers: bool = False

    async def get_children(self):
        db = await get_db()
        instances = await db.get_instance_name_list(self.name)
        return [TangoServerInstance(self, instance) for instance in instances]

    async def get_child(self, instance):
        return TangoServerInstance(self, instance)

    async def create(
        self, instance: str, class_devices: dict[str, Sequence[str]]
    ) -> None:
        await self.parent.create(self.name, instance, class_devices)

    async def rename(self, new_name: str) -> None:
        raise NotImplementedError()
        # TODO Jive does this by moving all devices to a new server, and then
        # deleting this one.


@dataclass(frozen=True, repr=False, eq=False)
class TangoServerInstance(TangoEntry):
    parent: TangoServer
    name: str

    include_dservers: bool = False

    @property
    def server(self) -> str:
        return self.parent.name

    @property
    def instance(self) -> str:
        return self.name

    @property
    def full_name(self) -> str:
        return f"{self.server}/{self.instance}"

    @property
    def dserver(self) -> str:
        return f"dserver/{self.full_name}"

    async def get_dserver_proxy(self) -> DeviceProxy:
        return await get_device_proxy(self.dserver)

    async def kill(self) -> None:
        proxy = await self.get_dserver_proxy()
        await proxy.command_inout("Kill")

    async def restart(self) -> None:
        proxy = await self.get_dserver_proxy()
        await proxy.command_inout("RestartServer")

    async def get_dserver_info(self) -> tango.DbDevFullInfo:
        db = await get_db()
        return await db.get_device_info(self.dserver)

    async def get_server_info(self) -> tango.DbDevFullInfo:
        db = await get_db()
        return await db.get_server_info(self.full_name)

    async def get_import_info(self) -> tango.DbDevImportInfo:
        db = await get_db()
        return await db.import_device(self.dserver)

    @cached
    async def get_device_class_list(self):
        server_instance = f"{self.server}/{self.instance}"
        db = await get_db()
        return await db.get_device_class_list(server_instance)

    async def get_children(self):
        dev_cls = await self.get_device_class_list()
        classes = sorted(set(dev_cls[1::2]))
        return [
            TangoDeviceClass(self, clss)
            for clss in classes
            if clss != "DServer" or self.include_dservers
        ]

    async def get_child(self, clss):
        dev_cls = await self.get_device_class_list()
        classes = {c.lower(): c for c in set(dev_cls[1::2])}
        if clss.lower() not in classes:
            raise KeyError(f"Unknown class '{clss}'")
        return TangoDeviceClass(self, classes[clss.lower()])

    async def delete(self, *args) -> None:
        "Remove this server instance from the Tango DB."
        db = await get_db()
        return await db.delete_server(self.full_name)

    async def create(self, class_devices: dict[str, Sequence[str]]) -> None:
        await self.parent.create(self.instance, class_devices)


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceClass(TangoEntry):
    parent: TangoServerInstance

    @property
    def server(self) -> TangoEntry:
        return self.parent.parent

    @property
    def instance(self) -> TangoEntry:
        return self.parent

    @property
    def server_instance(self) -> str:
        return f"{self.parent.parent}/{self.parent}"

    @property
    def clss(self) -> str:
        return self.name

    async def get_children(self):
        db = await get_db()
        devices = await db.get_device_name(self.server_instance, self.clss)
        return [TangoDevice(self, device) for device in devices]

    async def get_child(self, device):
        # TODO sanity check?
        db = await get_db()
        try:
            info = await db.get_device_info(device)
        except tango.DevFailed as e:
            raise KeyError(e.args[0].desc)
        if info.class_name != self.name:
            raise KeyError(f"Unexpected class for {device}: {info.class_name}!?")
        return TangoDevice(self, device)

    async def create(self, devices: Sequence[str]) -> None:
        await self.parent.create({self.name: devices})

    def create_device(self, app: App) -> None:
        "Create a new device of this class"
        app.notify("Not implemented yet")
