from .utils import get_db


async def search_devices(pattern):
    "Search for devices by name"
    db = await get_db()
    if "*" in pattern:
        pattern = pattern.replace("*", "%")
    else:
        pattern = f"%{pattern}%"
    query = f"""
        SELECT DISTINCT name FROM device
        WHERE name LIKE '{pattern}'
        ORDER BY name
        LIMIT 100
    """
    results = await db.query(query)
    return [(result, ["Device", *result.split("/")]) for result in results]


async def search_aliases(pattern):
    "Search for aliases"
    db = await get_db()
    if "*" in pattern:
        pattern = pattern.replace("*", "%")
    else:
        pattern = f"%{pattern}%"
    query = f"""
        SELECT DISTINCT alias FROM device
        WHERE alias LIKE '{pattern}'
        ORDER BY alias
        LIMIT 100
    """
    results = await db.query(query)
    return [(result, ["Alias", result]) for result in results]


async def search_servers(pattern):
    "Search for device servers by name"
    db = await get_db()
    if "*" in pattern:
        pattern = pattern.replace("*", "%")
    else:
        pattern = f"%{pattern}%"
    query = f"""
        SELECT DISTINCT server FROM device
        WHERE server LIKE '{pattern}'
        ORDER BY server
        LIMIT 100
    """
    results = await db.query(query)

    return [(result, ["Server", *result.split("/")]) for result in results]


async def search_properties(device, name, value):
    """Search device properties"""
    # You can filter on any combination
    # of device, property name and value, with "*" wildcards.
    db = await get_db()
    if device:
        if "*" in device:
            device = device.replace("*", "%")
        else:
            device = f"%{device}%"
    if name:
        if "*" in name:
            name = name.replace("*", "%")
        else:
            name = f"%{name}%"
    if value:
        if "*" in value:
            value = value.replace("*", "%")
        else:
            value = f"%{value}%"

    query = f"""
        SELECT DISTINCT device, name FROM property_device
        WHERE device LIKE '{device or '%'}'
        AND name LIKE '{name or '%'}'
        AND value LIKE '{value or '%'}'
        ORDER BY device, name
        LIMIT 100
    """
    results = await db.query(query)

    return [
        (
            " ▶ ".join([device, "Properties", prop]),
            ["Device", *device.split("/"), "properties", prop],
        )
        for device, prop in zip(results[::2], results[1::2])
    ]
