import asyncio
from functools import partial

from textual.command import Hit, Hits, Provider

from boogie.command import command_class
from boogie.messages import NavigateTo
from .utils import get_db
from .getters import get_server_instance_entry, get_device_entry, get_device_alias_entry


@command_class()
class TestCommand:
    "This is a test command"

    name = "A little test"

    async def check(self, app, entry):
        "Returns whether the command should be shown in the palette"
        return True

    async def run(self, app, entry):
        "Perform the command"
        print("Test")


class TangoSearch(Provider):
    """
    Command provider that searches the DB and returns navigation commands.
    """

    async def go_to(self, name, kind):
        if kind == "device":
            entry = await get_device_entry(name)
        elif kind == "alias":
            entry = await get_device_alias_entry(name)
        elif kind == "server":
            entry = await get_server_instance_entry(name)
        self.post_message(NavigateTo(entry))

    async def search(self, query: str) -> Hits:
        """Search for Python files."""

        # Sleep a little here, as a poor man's throttle. New key presses
        # will cancel this, and only once no new letters have been entered
        # for 0.5 seconds we'll actually do a search. This is to go a bit
        # easy on the DB.
        await asyncio.sleep(0.5)

        db = await get_db()
        if "*" in query:
            # If there are wildcards already, assume the user wants it like this
            pattern = query.replace("*", "%")
        else:
            # Otherwise wrap in wildcards
            pattern = f"%{query}%"

        sql = f"""
        SELECT name, 'device' FROM device
        WHERE name LIKE '{pattern}' AND NOT name LIKE 'dserver/%'
        UNION
        SELECT alias as name, 'alias' FROM device
        WHERE alias LIKE '{pattern}'
        UNION
        SELECT server as name, 'server' FROM device
        WHERE server LIKE '{pattern}'
        ORDER BY name
        LIMIT 25
        """
        results = await db.query(sql)

        for name, kind in zip(results[::2], results[1::2]):
            yield Hit(
                1,
                name,
                partial(self.go_to, name, kind),
                help=f"Go to {kind}",
            )
