import struct
from typing import Any

from dateutil.parser import parse
from textual import work
from textual.app import ComposeResult
from textual.reactive import reactive
from textual.containers import Horizontal
from textual.widgets import Static, Button

import tango
from tango.test_context import parse_ior  # TODO: move

from boogie.widgets.misc import InfoTable
from boogie.widgets.modal import ConfirmModal
from ..servers import TangoServerInstance
from ..getters import get_device_entry
from ..listener import get_listener
from .error import TangoError
from .details import TangoEntryWidget, TangoDetails, TangoBanner
from .device import DeviceState


class ServerInfoTable(InfoTable):
    FIELDS = [
        ("Name", "name"),
        ("Instance", "instance"),
        ("DServer", "dserver"),
        ("Exported", "exported"),
        ("Started", "started"),
        ("Stopped", "stopped"),
        ("PID", "pid"),
        ("Host", "host"),
        ("Port", "port"),
    ]


class TangoServerInstanceDetails(TangoDetails[TangoServerInstance]):
    DEFAULT_CSS = """
    TangoServerInstanceDetails {
        border: none;
        height: 1fr;
    }
    """

    BINDINGS = [*TangoDetails.BINDINGS, ("delete", "delete", "Delete")]

    ACTIONS = [
        ("kill", "Kill", "error", "Run the 'Kill' command on the DServer"),
        (
            "restart",
            "Restart",
            "error",
            "Run the 'RestartServer' command on the DServer.",
        ),
    ]

    info: reactive[dict | None] = reactive(None)

    def compose(self) -> ComposeResult:
        yield ServerInfo()
        with Horizontal(classes="actions"):
            for method, action, variant, tooltip in self.ACTIONS:
                button = Button(
                    action, variant=variant, classes="action", id=method, disabled=False
                )
                button.tooltip = tooltip
                yield button

        yield TangoError()

    async def entry_changed(self, entry):
        for w in self.query(TangoEntryWidget):
            w.set_entry(entry)
        self._check_state(entry)

    @work(exclusive=True)
    async def _check_state(self, entry):
        state_attr = f"{entry.dserver}/state"
        listener = get_listener()
        async for _, data in listener.listen([state_attr]):
            if isinstance(data, tango.DevFailed) and self._error != data:
                self._error = data
            elif self._error:
                self._error = None

    async def action_delete(self):
        async def maybe_delete(doit):
            if doit:
                msg = f"Deleted server instance '{self.entry.full_name}'"
                await self.app.run_entry_method(self.entry, self.entry.delete)
                await self.screen.reload()
                self.app.notify(msg)

        message = f"Really delete server instance {self.entry.full_name}?"
        proxy = await self.entry.get_dserver_proxy()
        try:
            await proxy.ping()
            message += "\n(Server is running, you should probably stop it first.)"
        except tango.DevFailed:
            pass
        self.app.push_screen(ConfirmModal(message), maybe_delete)


class ServerInfo(TangoEntryWidget):
    DEFAULT_CSS = """
    ServerInfo {
        width: 1fr;
        height: auto;
        display: none;
    }
    """

    def compose(self) -> ComposeResult:
        self.classes = "section"
        self.border_title = "Info"
        yield ServerInfoTable()

    async def entry_changed(self, entry: TangoServerInstance):
        config: dict[str, Any] = {}
        dserver = entry.dserver
        dserver_path = ":".join(["Device", *dserver.split("/")])
        config["dserver"] = f"[@click=app.go_to('{dserver_path}')]{dserver}[/]"
        info = await entry.get_dserver_info()
        server_info = await entry.get_server_info()

        config.update(
            {
                "name": entry.server,
                "instance": entry.instance,
                "exported": bool(info.exported),
                "started": (
                    parse(info.started_date).isoformat(sep=" ")
                    if info.started_date
                    else ""
                ),
                "stopped": (
                    parse(info.stopped_date).isoformat(sep=" ")
                    if info.stopped_date
                    else ""
                ),
                "host": server_info.host,
            }
        )
        try:
            ior = parse_ior(info.ior)
            port = ior.port
        except (AssertionError, struct.error):
            port = None
        if port is not None:
            config["port"] = port
        if info.pid:
            config["pid"] = info.pid

        w = self.query_one(ServerInfoTable)
        await w.set_config(config)
        self.display = True


class TangoServerBanner(TangoBanner[TangoServerInstance]):
    DEFAULT_CSS = """
    TangoServerBanner {
        height: 3;
    }
    TangoServerBanner #name {
        width: 1fr;
        padding: 1 2;
        content-align: center middle;
    }
    TangoServerBanner DeviceState {
        width: 15;
    }
    """

    def compose(self) -> ComposeResult:
        with Horizontal():
            yield Static(id="name")
            yield DeviceState()

    async def entry_changed(self, entry: TangoServerInstance) -> None:
        try:
            dserver_entry = await get_device_entry(entry.dserver)
            self.query_one(DeviceState).set_entry(dserver_entry)
        except KeyError:
            # Guess there's no DServer
            pass
        self.query_one("#name", expect_type=Static).update(
            f"Server: [b]{entry.full_name}[/]"
        )
