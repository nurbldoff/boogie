from collections import deque

from textual.widgets import Static, Digits
import numpy as np
import tango
from rich.console import RenderableType
from rich.pretty import Pretty
from rich.text import Text
from rich.table import Table

from boogie.widgets.entry import MonitorWidget
from boogie.plot import plot, time_plot, category_time_plot
from boogie.util import all_subclasses
from .device import DeviceState
from ..device import TangoDevice, TangoDeviceAttribute
from ..listener import get_listener


NUMERIC_TYPES = {
    value
    for value in tango.CmdArgType.values.values()
    if tango.utils.is_numerical(value)
}

STATE_COLORS = {
    tango.DevState.INIT: "rgb(204,204,122)",
    tango.DevState.DISABLE: "rgb(255,0,255)",
    tango.DevState.STANDBY: "rgb(255,255,0)",
    tango.DevState.ON: "#00FF00",
    tango.DevState.OPEN: "#00FF00",
    tango.DevState.EXTRACT: "#00FF00",
    tango.DevState.OFF: "white",
    tango.DevState.CLOSE: "white",
    tango.DevState.INSERT: "white",
    tango.DevState.FAULT: "red",
    tango.DevState.RUNNING: "rgb(128,160,255)",
    tango.DevState.MOVING: "rgb(128,160,255)",
    tango.DevState.ALARM: "rgb(255,140,0)",
    tango.DevState.UNKNOWN: "rgb(128,128,128)",
}


class TangoAttributeMonitorWidget(MonitorWidget[TangoDeviceAttribute]):
    """
    Base class for a tango attribute monitor widget.
    Handles picking the correct subclass depending on attribute type.
    """

    DEFAULT_CSS = """
    TangoAttributeMonitorWidget {
        height: auto;
        width: 1fr;
        layout: grid;
        grid-size: 4 2;
        grid-columns: auto 1fr auto 2;
        grid-rows: auto 5;
        grid-gutter: 0 1;
        #name {
            text-style: bold;
        }
        #quality {

        }
        #spacer {
        }
        #value {
            height: auto;
            text-style: bold;
            text-align: right;
        }
        &.error, &.failed {
            background: rgba(255,0,0,0.1);
            hatch: right #ff0000;
        }
    }
    """

    @classmethod
    async def get_widgets(cls, entry):
        info = await entry.get_config()
        data_type = tango.CmdArgType.values[info.data_type]
        subclasses = all_subclasses(TangoAttributeMonitorWidget)
        matching_subclasses = [
            widget_class
            for widget_class in reversed(subclasses)
            if (
                info.data_format in widget_class.DATA_FORMATS
                and (
                    not hasattr(widget_class, "DATA_TYPES")
                    or data_type in widget_class.DATA_TYPES
                )
                and (
                    not hasattr(widget_class, "WRITE_TYPES")
                    or info.writable in widget_class.WRITE_TYPES
                )
            )
        ]
        return matching_subclasses

    def _format_tooltip(self) -> RenderableType:
        if not self.attr_name and self.attr_config:
            return "..."
        config = self.attr_config
        table = Table.grid("Name", "Value")
        table.show_header = False
        table.min_width = 100
        table.add_row("Name:", self.attr_name)
        table.add_row("Type:", str(tango.CmdArgType.values[config.data_type]))
        table.add_row("Format:", str(config.data_format))
        return table

    def update_name(self, name):
        widget = self.get_widget_by_id("name", expect_type=Static)
        dev, attr = name.rsplit("/", 1)
        widget.update(f"[#808080]{dev}/[/]{attr}")
        self.attr_name = name

    def update_config(self, config):
        widget = self.get_widget_by_id("unit", expect_type=Static)
        widget.update(config.unit)
        self.attr_config = config

    def should_render(self):
        """
        No point in rendering if we are not on screen. Note that monitor widgets
        are live updating and might rerender often, causing high CPU load.
        """
        return self.app.screen == self.screen and self.is_on_screen

    def update_data(self, data):
        if self.should_render():
            value_widget = self.get_widget_by_id("value", expect_type=Static | Digits)
            value_str, classes = self._format_value(data)
            value_widget.update(value_str)
            if classes:
                value_widget.classes = classes
            else:
                value_widget.classes = ""
            quality_widget = self.get_widget_by_id("quality")
            quality_widget.classes = f"quality-{str(data.quality)}"
            self.attr_data = data


class MonitorScalar(TangoAttributeMonitorWidget):
    DATA_FORMATS = {tango.AttrDataFormat.SCALAR}

    attr_name: str | None = None
    attr_name_dirty: bool = True
    attr_config: tango.AttributeInfoEx | None = None
    attr_config_dirty: bool = True
    attr_data: tango.DeviceAttribute | tango.DevFailed | None = None
    attr_data_dirty: bool = True

    def compose(self):
        self.can_focus = True
        yield Static(id="name")
        yield Static(id="value")
        yield Static(id="unit")
        yield Static("▪", id="quality")

    def _format_value(self, data) -> tuple[RenderableType, str | None]:
        """Return value as a renderable, and optional classes"""
        if isinstance(data, tango.DevFailed):
            return "Oops", None
        value = data.value
        if isinstance(value, tango.DevState):
            color = STATE_COLORS[value]
            return f"[{color}]{value}[/]", None
        if self.attr_config and self.attr_config.data_type == tango.DevEnum:
            try:
                return self.attr_config.enum_labels[value], None
            except IndexError:
                return "???", None
        if (
            self.attr_config
            and self.attr_config.format
            and self.attr_config.format != "Not specified"
        ):
            try:
                return self.attr_config.format % value, None
            except TypeError:
                pass
        return Pretty(value, justify="right"), None

    async def entry_changed(self, entry):
        self.update_name(await self.entry.get_name())
        self.update_config(await self.entry.get_config())
        self.tooltip = self._format_tooltip()
        listener = get_listener()
        attr = await entry.get_full_name()
        async for _, data in listener.listen([attr]):
            if isinstance(data, tango.DevFailed):
                self.error = data
                continue
            if isinstance(data, tango.DeviceAttribute):
                self.update_data(data)


class MonitorScalarLarge(MonitorScalar):
    DATA_FORMATS = {tango.AttrDataFormat.SCALAR}
    DATA_TYPES = NUMERIC_TYPES

    def compose(self):
        self.can_focus = True
        yield Static("name", id="name")
        yield Digits("value", id="value")
        yield Static("unit", id="unit")
        yield Static("▪", id="quality")


class MonitorScalarTrend(MonitorScalar):
    DATA_FORMATS = {tango.AttrDataFormat.SCALAR}
    DATA_TYPES = {*NUMERIC_TYPES, tango.CmdArgType.DevBoolean}

    DEFAULT_CSS = """
    MonitorScalarTrend {
        #plot {
            column-span: 4;
            height: 5;
        }
    }
    """

    def __init__(self, *args, **kwargs):
        self._timestamps = deque(maxlen=1024)
        self._values = deque(maxlen=1024)
        super().__init__(*args, **kwargs)

    def compose(self):
        self.can_focus = True
        yield Static("name", id="name")
        yield Static(id="value")
        yield Static(id="unit")
        yield Static("▪", id="quality")
        yield Static(id="plot")

    def draw(self, time_window, time_ticks):
        css_vars = self.app.get_css_variables()
        plot_widget = self.get_widget_by_id("plot")
        p = time_plot(
            data=[
                (
                    self._timestamps,
                    self._values,
                    self.attr_name,
                    css_vars["primary-lighten-3"],
                )
            ],
            x_limits=time_window,
            height=plot_widget.size.height,
            width=plot_widget.size.width,
            time_ticks=time_ticks,
            show_x_axis=False,
            show_legend=False,
        )
        self.get_widget_by_id("plot").update(Text.from_ansi(p))

    async def entry_changed(self, entry):
        self.update_name(await self.entry.get_name())
        self.update_config(await self.entry.get_config())
        self.tooltip = self._format_tooltip()
        listener = get_listener()
        attr = await entry.get_full_name()
        async for _, data in listener.listen([attr], throttle=False):
            if isinstance(data, tango.DevFailed):
                self.error = data
            elif isinstance(data, tango.DeviceAttribute):
                self.update_data(data)  # type: ignore
                self._values.append(data.value)
                self._timestamps.append(data.time.todatetime().astimezone(tz=None))


class MonitorStateTrend(MonitorScalarTrend):
    DATA_TYPES = {tango.CmdArgType.DevState}

    DEFAULT_CSS = """
    MonitorStateTrend {
        #plot {
            column-span: 4;
            height: 1;
        }
    }
    """

    def draw(self, time_window, time_ticks):
        css_vars = self.app.get_css_variables()
        plot_widget = self.get_widget_by_id("plot")
        p = category_time_plot(
            data=[
                (self._timestamps, self._values, self.attr_name, css_vars["secondary"])
            ],
            colors=STATE_COLORS,
            t_limits=time_window,
            height=plot_widget.size.height,
            width=plot_widget.size.width,
        )
        self.get_widget_by_id("plot").update(Text.from_ansi(p))


class MonitorStateLarge(MonitorScalar):
    DATA_TYPES = {tango.CmdArgType.DevState}

    DEFAULT_CSS = """
    MonitorStateLarge {
        height: auto;
        width: 1fr;
        layout: grid;
        grid-size: 2 1;
        grid-columns: 1fr auto;
        grid-gutter: 0 1;
        #name {
            margin: 1;
            text-style: bold;
        }
        &.error, &.failed {
            background: rgba(255,0,0,0.1);
            hatch: right #ff0000;
        }
        DeviceState {
            width: 10;
            background: red;
        }
    }
    """

    def compose(self):
        yield Static(id="name")
        yield DeviceState()

    async def entry_changed(self, entry):
        self.query_one("#name").update(entry.name)
        self.query_one(DeviceState).set_entry(entry)
    
        
class MonitorSpectrum(MonitorScalar):
    DATA_FORMATS = {tango.AttrDataFormat.SPECTRUM}
    DATA_TYPES = NUMERIC_TYPES

    def compose(self):
        np.set_printoptions(threshold=5)
        self.can_focus = True
        yield Static("name", id="name")
        yield Static("value", id="value")
        yield Static("unit", id="unit")
        yield Static("▪", id="quality")

    def _format_value(self, data) -> tuple[RenderableType, str | None]:
        np.set_printoptions(threshold=10, linewidth=50)
        return str(data.value), None


class MonitorSpectrumPlot(TangoAttributeMonitorWidget):
    DATA_FORMATS = {tango.AttrDataFormat.SPECTRUM}
    DATA_TYPES = NUMERIC_TYPES

    DEFAULT_CSS = """
    MonitorSpectrumPlot {
        #plot {
            hatch: right $background-lighten-1;
            column-span: 4;
            height: 5;
            width: 1fr;
            content-align: center middle;
        }
    }
    """

    attr_name: str | None = None
    attr_name_dirty: bool = True
    attr_config: tango.AttributeInfoEx | None = None
    attr_config_dirty: bool = True
    attr_data: tango.DeviceAttribute | tango.DevFailed | None = None
    attr_data_dirty: bool = True

    def compose(self):
        np.set_printoptions(threshold=5)
        self.can_focus = True
        yield Static(id="name")
        yield Static(id="size")
        yield Static(id="unit")
        yield Static("▪", id="quality")
        yield Static(id="plot", expand=True, shrink=True)

    def update_data(self, data):
        if self.should_render():
            if data.value is None:
                self.get_widget_by_id("plot", expect_type=Static).update("No data")
                self.query_one("#size", expect_type=Static).update("")
                self.query_one("#quality").classes = f"quality-{str(data.quality)}"
                return
            self.query_one("#size", expect_type=Static).update(Pretty([data.dim_x]))
            self.query_one("#quality").classes = f"quality-{str(data.quality)}"
            plot_widget = self.get_widget_by_id("plot")
            css_vars = self.app.get_css_variables()
            p = plot(
                data=data.value,
                color=css_vars["accent"],
                height=plot_widget.size.height,
                width=plot_widget.size.width,
                show_x_axis=False,
                show_legend=False,
            )
            plot_widget.update(Text.from_ansi(p))

    async def entry_changed(self, entry):
        self.update_name(await self.entry.get_name())
        self.update_config(await self.entry.get_config())
        self.tooltip = self._format_tooltip()
        listener = get_listener()
        attr = await entry.get_full_name()
        async for _, data in listener.listen([attr]):
            if isinstance(data, tango.DevFailed):
                self.error = data
                continue
            if isinstance(data, tango.DeviceAttribute):
                self.update_data(data)  # type: ignore


class TangoDeviceMonitorWidget(MonitorWidget[TangoDevice]):
    @classmethod
    async def get_widgets(cls, entry):
        return [DeviceMonitor]


class DeviceMonitor(TangoDeviceMonitorWidget):
    DEFAULT_CSS = """
    DeviceMonitor {
        height: auto;
        width: 1fr;
        layout: grid;
        grid-size: 2 1;
        grid-columns: 1fr auto;
        grid-gutter: 0 1;
        #name {
            margin: 1;
            text-style: bold;
        }
        &.error, &.failed {
            background: rgba(255,0,0,0.1);
            hatch: right #ff0000;
        }
        DeviceState {
            width: 10;
            background: red;
        }
    }
    """

    def compose(self):
        yield Static(id="name")
        yield DeviceState()

    async def entry_changed(self, entry):
        self.query_one("#name").update(entry.name)
        self.query_one(DeviceState).set_entry(entry)
