"""
Widget for configuring Tango logging for a device.

For info on the Tango logging system (aka "TLS"):
https://tango-controls.readthedocs.io/en/latest/development/advanced/reference.html#the-device-logging
https://tango-controls.readthedocs.io/projects/rfc/en/latest/14/Logging.html
"""

from __future__ import annotations
from typing import cast

from textual import on
from textual.app import ComposeResult
from textual.containers import Horizontal, Vertical, Grid
from textual.widgets import Label, Input, Button, Select, Log, DataTable
from textual.screen import ModalScreen
from textual.events import Key
import tango

from .details import TangoDetails
from ..device import TangoDeviceLogging
from .error import TangoError


class DeviceLoggingDetails(TangoDetails[TangoDeviceLogging]):
    DEFAULT_CSS = """
    DeviceLoggingDetails {
        .level {
            layout: grid;
            grid-size: 2 3;
            grid-columns: auto 1fr;
            grid-gutter: 0 1;
            height: auto;
            Label {
                height: 3;
                width: 1fr;
                content-align: right middle;
            }
        }
        .targets {
            layout: grid;
            grid-gutter: 1;
            DataTable {
                width: 100%;
            }
            Vertical {
                height: 1fr;
                width: auto;
                content-align: right middle;
                hatch: right $background-lighten-1;
                margin-left: 1;
                dock: right;
                Button {
                    min-width: 10;
                }
            }
            #rft {
                border: solid $primary;
            }
        }
    }
    """

    TANGO_LOG_LEVELS = {
        0: "OFF",
        5: "DEBUG",
        4: "INFO",
        3: "WARNING",
        2: "ERROR",
        1: "FATAL",
    }

    def compose(self) -> ComposeResult:
        with Vertical(classes="level section") as v:
            v.border_title = "Logging configuration"
            logging_level = Label("Logging level:")
            logging_level.tooltip = (
                "Logging level setting, stored in a property to persist after restart."
                + " Changing this does not affect current logging."
            )
            yield logging_level
            choices = [(name, value) for value, name in self.TANGO_LOG_LEVELS.items()]
            level_sel: Select[int] = Select(
                choices, prompt="Not configured", id="level"
            )
            yield level_sel

            current_logging_level = Label("Current logging level:")
            current_logging_level.tooltip = (
                "Current logging level setting. This setting is taken from the above"
                + " on startup, but can be changed independently during runtime."
            )
            yield current_logging_level
            with self.prevent(Select.Changed):
                yield Select(choices, allow_blank=False, id="current-level")

            rft = Label("File rotation threshold:")
            rft.tooltip = (
                "Maximum size (in kB) of a logfile before rotation. Minimum 500 kB, max 1 GB. Default is 20 MB."
                + " Will take effect after the device server is restarted."
            )
            yield rft
            yield Input(placeholder="default", type="integer", id="rft")

        with Grid(classes="targets section") as v:
            v.border_title = "Logging targets"
            targets: DataTable = DataTable(id="targets")
            targets.cursor_type = "row"
            targets.add_columns("Type", "Target")
            targets.tooltip = (
                "Logging targets (e.g. logfiles), stored in a property to persist after restart."
                + " Changing this does not affect current logging."
            )
            yield targets
            with Vertical():
                yield Button("Add", id="add-target", variant="primary")
                yield Button("Remove", id="remove-target", variant="error")

        with Grid(classes="targets section") as v:
            v.border_title = "Current logging targets"
            current_targets: DataTable = DataTable(id="current-targets")
            current_targets.cursor_type = "row"
            current_targets.add_columns("Type", "Target")
            current_targets.tooltip = (
                "Current logging targets (e.g. logfiles). This setting is taken from the"
                + " above setting on startup, but can be changed independently during runtime."
            )
            yield current_targets
            with Vertical():
                yield Button("Add", id="add-current-target", variant="primary")
                yield Button("Remove", id="remove-current-target", variant="error")

        yield TangoError()

    async def entry_changed(self, entry):
        try:
            with self.app.batch_update():
                p = await self.entry.get_proxy()

                prop = (await p.get_property("logging_level")).get("logging_level", [])
                try:
                    level = int(prop[0])
                    sel = self.query_one("#level")
                    with self.prevent(Select.Changed):
                        sel.value = level
                except (IndexError, ValueError):
                    pass

                logging_level = p.get_logging_level()
                sel = self.query_one("#current-level")
                with self.prevent(Select.Changed):
                    sel.value = logging_level

                prop = (await p.get_property("logging_rft")).get("logging_rft", [])
                rft = self.query_one("#rft")
                with self.prevent(Input.Submitted):
                    if prop:
                        rft.value = prop[0]
                    else:
                        rft.clear()

                # Logging target property
                prop = (await p.get_property("logging_target")).get(
                    "logging_target", []
                )
                targets = self.query_one("#targets")
                targets.clear()
                for row in prop:
                    target_type, target = row.split("::")
                    targets.add_row(target_type, target)

                # Current logging target
                logging_targets = p.get_logging_target()
                current_targets = self.query_one("#current-targets")
                current_targets.clear()
                for item in logging_targets:
                    if item == "telemetry_logs_appender":
                        target_type = "telemetry"
                        target = item
                    else:
                        target_type, target = item.split("::")
                    current_targets.add_row(target_type, target)
            self.query_one("#current-level").disabled = False
            self.query_one("#add-current-target").disabled = False
            self.query_one("#remove-current-target").disabled = False

        except tango.DevFailed as e:
            # Guess the device is offline, disable "live" controls
            self.query_one("#current-level").disabled = True
            self.query_one("#add-current-target").disabled = True
            self.query_one("#remove-current-target").disabled = True
            self._error = e

    @on(Select.Changed, selector="#level")
    async def level_changed(self, event: Select.Changed) -> None:
        assert self.entry
        level = cast(int, event.value)  # TODO There should be a better way
        proxy = await self.entry.get_proxy()
        if level == Select.BLANK:
            await proxy.delete_property("logging_level")
        else:
            level_name = self.TANGO_LOG_LEVELS[level]
            # Property stores level as string, methods use integer...
            await proxy.put_property({"logging_level": [level_name]})

    @on(Select.Changed, selector="#current-level")
    async def current_level_changed(self, event: Select.Changed) -> None:
        assert self.entry
        p = await self.entry.get_proxy()
        p.set_logging_level(event.value)

    async def on_input_submitted(self, event: Input.Submitted) -> None:
        assert self.entry
        p = await self.entry.get_proxy()
        if event.value:
            await p.put_property({"logging_rft": [str(event.value)]})
        else:
            await p.delete_property("logging_rft")

    @on(Button.Pressed, selector="#add-target")
    async def add_target(self):
        async def maybe_save(target: str | None):
            if target:
                try:
                    assert self.entry
                    p = await self.entry.get_proxy()
                    props = await p.get_property("logging_target")
                    value = props.get("logging_target", [])
                    value.append(target)
                    await p.put_property({"logging_target": value})
                    self.reload()
                except tango.DevFailed as e:
                    self.app.notify(f"Failed to set target: {e.args[0].desc}")

        self.app.push_screen(LoggingTargetModal(), maybe_save)

    @on(Button.Pressed, selector="#add-current-target")
    async def add_current_target(self):
        async def maybe_save(target: str | None):
            if target:
                assert self.entry
                try:
                    p = await self.entry.get_proxy()
                    p.add_logging_target(target)
                    self.reload()
                except tango.DevFailed as e:
                    self.app.notify(f"Failed to set current target: {e.args[0].desc}")

        self.app.push_screen(LoggingTargetModal(), maybe_save)

    @on(Button.Pressed, selector="#remove-target")
    async def remove_target(self):
        targets = self.query_one("#targets")
        index = targets.cursor_row
        if index is not None:
            p = await self.entry.get_proxy()
            value = list(
                (await p.get_property("logging_target")).get("logging_target", [])
            )
            try:
                value.pop(index)
                await p.put_property({"logging_target": value})
                self.reload()
            except IndexError:
                pass

    @on(Button.Pressed, selector="#remove-current-target")
    async def remove_current_target(self):
        current_targets = self.query_one("#current-targets")
        index = current_targets.cursor_row
        if index is not None:
            type_, target = current_targets.get_row_at(index)
            p = await self.entry.get_proxy()
            if type_ == "file":
                p.remove_logging_target(f"file::{target}")
                # In case the target was added as just a filename
                # it will be in the default tango log path. In that
                # case we can't remove it using the full path, but
                # must use only the filename. I don't know of a way
                # to detect this, so let's just do both :(
                _, filename = target.rsplit("/", 1)
                p.remove_logging_target(f"file::{filename}")
            elif type_ == "console":
                p.remove_logging_target("console::")
            else:
                p.remove_logging_target(f"{type_}::{target}")
            self.reload()


class LoggingTargetModal(ModalScreen[str | None]):
    DEFAULT_CSS = """
    LoggingTargetModal {
        align: center middle;
        height: auto;
        .frame {
            width: 75%;
            height: auto;
            border: panel $secondary;
            padding: 0 1;
        }
        .body {
            height: auto;
            layout: grid;
            grid-size: 2 2;
            grid-columns: auto 1fr;
            grid-gutter: 1;
            padding: 1;
        }
        Horizontal {
            height: auto;
        }
        Button {
             width: 1fr;
        }
    }
    """

    def compose(self):
        with Vertical(classes="frame") as v:
            v.border_title = "Add logging target"
            with Vertical(classes="body") as v:
                yield Label("Type:")
                targets = [
                    ("file - [i]an XML format logfile", "file"),
                    ("device - [i]a logger Tango device", "device"),
                    ("console - [i]log to stderr", "console"),
                ]
                yield Select(targets)
                yield Label("Target:")
                yield Input()
            with Horizontal():
                yield Button("Add", id="add", variant="primary")
                yield Button("Cancel", id="cancel", variant="warning")

    @on(Button.Pressed, "#add")
    def add(self):
        self.action_add()

    @on(Button.Pressed, "#cancel")
    def cancel(self):
        self.dismiss(None)

    def on_key(self, event: Key) -> None:
        if event.key == "escape":
            event.prevent_default()
            self.dismiss(None)

    def action_add(self):
        target_type = self.query_one(Select).value
        target = self.query_one(Input).value
        self.dismiss(f"{target_type}::{target}")

    def action_cancel(self):
        self.dismiss(None)


class DeviceLogs(TangoDetails):
    entry_class = TangoDeviceLogging

    DEFAULT_CSS = """
    TangoDeviceLogs {
        height: 1fr;
        Log {
            height: 1fr;
            background: blue;
        }
    }
    """

    def compose(self):
        yield Log(auto_scroll=False)

    async def entry_changed(self, entry):
        log_widget = self.query_one(Log)
        logs = await entry.logs()
        async for t, level, device, thread, message in logs:
            log_widget.write_line(f"{t}\t{level}\t{device}\t{thread}\t{message}")
