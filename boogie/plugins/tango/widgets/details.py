"""
Some small wrapper classes that add Tango specific error handling.
"""

from typing import Generic

from textual import on
from textual.widgets import Button
from textual.widgets._button import ButtonVariant
import tango

from boogie.widgets.details import Details, EntryWidget, Banner
from boogie.widgets.modal import ConfirmModal
from boogie.entry import EntryType
from .error import TangoError


class TangoEntryWidget(EntryWidget, Generic[EntryType]):
    error_widget = TangoError


class TangoDetails(Details, Generic[EntryType]):
    error_widget = TangoError

    ACTIONS: list[tuple[str, str, ButtonVariant, str]] = []

    @on(Button.Pressed, ".action")
    async def button_pressed(self, message):
        "User preseed an 'action' button."

        async def maybe_run(doit):
            if doit:
                try:
                    method = getattr(self.entry, message.button.id)
                    await self.app.run_entry_method(self.entry, method)
                except tango.DevFailed as e:
                    self.log(f"Failed to run {method}", exception=e)
                    self.app.notify(
                        f"An error occurred: {e.args[-1].desc}", severity="error"
                    )
                except Exception as e:
                    self.log("Unexpected error", exception=e)

        question = f"{message.button.tooltip}?"
        self.app.push_screen(ConfirmModal(question), maybe_run)

    def watch__error(self, old_error, error):
        super().watch__error(old_error, error)
        for button in self.query("Button#command"):
            button.disabled = bool(error)


class TangoBanner(Banner, TangoEntryWidget, Generic[EntryType]):
    DEFAULT_CSS = """
    TangoBanner {
        height: 3;
        #name {
            width: 1fr;
            padding: 1 2;
            content-align: center middle;
        }
        DeviceState {
            width: 15;
        }
    }
    """
