from datetime import datetime
import json

from rich.text import Text
from rich.pretty import Pretty
import tango
from textual import on, work
from textual.binding import Binding
from textual.containers import Vertical, Horizontal
from textual.validation import Function
from textual.widgets import (
    Static,
    Input,
    TabbedContent,
    TabPane,
    TextArea,
    Button,
)

from boogie.widgets.misc import PersistentLog
from ..device import TangoDeviceCommand
from .details import TangoDetails
from .error import TangoError, format_devfailed
from ..utils import to_pytype


class TangoDeviceCommandDetails(TangoDetails[TangoDeviceCommand], can_focus=False):
    # TODO we can be more helpful with arguments, some don't make much sense
    # as string inputs, e.g boolean, state...

    DEFAULT_CSS = """
    TangoDeviceCommandDetails {
        height: 1fr;
        TabbedContent {height: 1fr;}
        TabPane#run {
            height: auto;
            layout: grid;
            grid-rows: auto 1fr;
            grid-gutter: 1 1;
        }
        #info {
            padding: 0 1;
            layout: grid;
            grid-size: 3 1;
            grid-gutter: 2;
            grid-columns: auto 1fr auto;
            height: auto;
            width: 100%;
        }
        #arguments {
            height: auto;
            padding: 0 0;
            background: $background-lighten-1;
        }
        #arguments:focus-within {
            background: $background-lighten-2;
        }
        #arguments Input {
            border: none red;
            height: 1;
            padding: 0;
        }
        #arguments TextArea {
            max-height: 4;
        }
        #arguments Static {
            width: 1fr;
            height: 1;
            background: $background;
        }
        PersistentLog {
            background: $background-darken-1;
            height: 1fr;
        }
        Input.-invalid {
            background: $error-darken-3 60%;
        }
        Input.-invalid:focus {
            background: $error-darken-3;
        }
    }
    """

    BINDINGS = [
        Binding("ctrl+enter", "run", "Run", show=False),  # TODO does not work!
        ("ctrl+enter", "run", "Run"),  # TODO does not work!
        ("ctrl+up", "back", "Back"),
    ]

    def __init__(self, *args, **kwargs):
        self._last_id = None
        self._history = []  # Previous arguments
        super().__init__(*args, **kwargs)

    def compose(self):
        # TODO convert to using LazyEntryTabs
        with TabbedContent(name="Something"):
            with TabPane("Run", id="run") as pane:
                pane.can_focus = False
                with Horizontal(id="info"):
                    run_button = Button(
                        "Run", id="run", variant="warning", classes="small"
                    )
                    run_button.can_focus = False
                    yield run_button
                    yield Vertical(id="arguments")
                    clear_button = Button(
                        "Clear", id="clear", variant="primary", classes="small"
                    )
                    clear_button.can_focus = False
                    yield clear_button
                yield PersistentLog()
            with TabPane("Polling", id="polling"):
                pass
        yield TangoError()

    def on_mount(self):
        self.query_one("Tabs").can_focus = False
        self.classes = "header-border"

    def focus(self, **kwargs):
        w = self.query_one("#arguments > *")
        w.focus()

    @on(TabbedContent.TabActivated)
    async def on_tab_activated(self, message):
        message.stop()  # Don't leak these up
        # TODO proper tab handling, when there are more than one...

    async def entry_changed(self, entry: TangoDeviceCommand):
        self.query_one(PersistentLog).set_entry(entry)
        self.border_title = str(entry)
        with self.app.batch_update():
            args = self.query_one("#arguments", expect_type=Vertical)
            await args.remove_children()
            if entry.takes_arguments:
                if entry.info.in_type.name.endswith("Array"):
                    # Multi-line editor, one line per argument
                    widget1 = TextArea(show_line_numbers=True, soft_wrap=False)
                    args.mount(widget1)
                else:
                    # Single argument editor
                    widget2 = Input(
                        validators=[Function(self._is_valid_argument)],
                        id="single-argument",
                        placeholder="Argument",
                    )
                    args.mount(widget2)
                    # TODO don't know why CSS doesn't bite here...
                    widget2.styles.border = "none", "red"
            else:
                # No argument
                args.mount(Static())

    def _is_valid_argument(self, value: str) -> bool:
        if self.entry:
            try:
                self.entry.validate_argument(value)
            except ValueError:
                return False
            else:
                return True
        return False

    def _get_arguments(self):
        if self.entry.takes_arguments:
            widget = self.query_one("#arguments > *")
            if isinstance(widget, Input):
                value = widget.value
            else:
                value = widget.text.splitlines()
            if value:
                self._history.append(value)
            return value

            # except Exception as e:
            #     print("Error converting arguments", e)
            #     self.error = e

    def action_run(self):
        self.run()

    def action_back(self):
        "Get argument from history"
        while True:
            try:
                item = self.app.history.get_last_matching_item(
                    id_=self._last_id, path=self.entry.path_str(), action="run"
                )
            except IndexError:
                # No more calls in the history
                break
            self._last_id = int(item[0])
            try:
                args = json.loads(item[5])["args"]
                # Found one!
                if isinstance(args, list):
                    self.query_one(TextArea).load_text("\n".join(str(a) for a in args))
                else:
                    self.query_one("#single-argument").value = str(args)
                break
            except (TypeError, IndexError, KeyError, ValueError) as e:
                print(f"Could not load historical value {self._last_id}: {e}")
                continue

    @on(Button.Pressed, "#run")
    def run_button(self):
        self.run()

    @on(Button.Pressed, "#clear")
    async def on_clear(self):
        self.query_one(PersistentLog).clear()

    @work
    async def run(self):
        log = self.query_one(PersistentLog)
        start = datetime.now()
        self._last_id = None
        if self.entry.info.in_type is tango.DevVoid:
            arguments = None
        else:
            try:
                arguments = to_pytype(
                    self.entry.convert_arguments(self._get_arguments())
                )
            except Exception as e:
                # Problem parsing arguments
                # TODO parse args "on the fly" for more immediate feedback
                # TODO argument "preview" to show how we parse the input?
                # TODO validate as we type
                # self.error = e
                log.write(Text("Error parsing arguments:", style="red"))
                log.write(Pretty(e))
                return
        try:
            result = await self.app.run_entry_method(
                self.entry, self.entry.run, args=arguments
            )
            duration = (datetime.now() - start).total_seconds()

            log.write(
                Text(f"--- {start.isoformat()} ({duration:.3f} s) ---", style="blue")
            )
            if self.entry.takes_arguments:
                log.write(f"In:  {arguments}")
            if self.entry.returns_values:
                log.write(f"Out: {result}")
        except tango.DevFailed as e:
            log.write(Text(f"--- {start.isoformat()} ---", style="red"))

            log.write(format_devfailed(e))
