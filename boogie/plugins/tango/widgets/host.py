from __future__ import annotations
import asyncio
from itertools import groupby

import tango
from textual import work
from textual.widgets import Static, Collapsible, ListView, ListItem
from textual.containers import Horizontal
from textual.css.query import NoMatches
from textual.reactive import reactive

# from boogie.widgets.misc import StyledCollapsible
from boogie.widgets.modal import ConfirmModal
from .details import TangoEntryWidget, TangoDetails, TangoBanner
from .device import DeviceState, DeviceStatus
from ..hosts import TangoHost
from ..getters import get_device_entry
from ..listener import get_listener
from ..starter import start_server, stop_server


class TangoHostDetails(TangoDetails[TangoHost]):
    DEFAULT_CSS = """
    TangoHostDetails {
        height: 1fr;
        DeviceStatus {
            height: 5;
        }
    }
    """

    def compose(self):
        yield DeviceStatus()
        yield TangoServerLevels()

    async def entry_changed(self, entry):
        starter_entry = await get_device_entry(entry.get_starter())
        self.query_one(DeviceStatus).set_entry(starter_entry)
        self.query_one(TangoServerLevels).set_entry(entry)


class ServerState(DeviceState):
    DEFAULT_CSS = """
    ServerState {
        height: 1;
        border: none;
    }
    """


class ServerLabel(Static):
    "Show compact info about a server"

    DEFAULT_CSS = """
    ServerLabel {
        padding-left: 1;
    }
    """

    state: reactive[tango.DevState | None] = reactive(None)

    def __init__(self, server, *args, **kwargs):
        self._server = server
        super().__init__(*args, **kwargs)

    def watch_state(self, _, state: tango.DevState):
        # path = "Server:" + self._server.replace('/', ':')
        # link = f"[@click=screen.go_to('{path}')]{self._server}[/]"
        if state == tango.DevState.ON:
            self.update(f"[#00ff00]●[/] {self._server}")
        elif state == tango.DevState.MOVING:
            self.update(f"[#ffff00]●[/] {self._server}")
        else:
            self.update(f"[#ff0000]⨯[/] {self._server}")


class StarterLevel(Static):
    DEFAULT_CSS = """
    StarterLevel {
        height: auto;
        ListView {
            height: auto;
            background: transparent;
        }
        ListItem {
            background: transparent;
        }
    }
    """

    servers: reactive[list] = reactive([], init=False)

    BINDINGS = [
        ("s", "start", "Start"),
        ("t", "stop", "Stop"),
        ("k", "kill", "Kill"),
    ]

    def __init__(self, hostname, level, *args, **kwargs):
        self._hostname = hostname
        self._starter_level = level
        self._title = (
            f"Level {self._starter_level}"
            if self._starter_level > 0
            else "Not controlled"
        )
        super().__init__(*args, **kwargs)

    def compose(self):
        with Collapsible(title=self._title):
            yield ListView()

    async def watch_servers(self, old_servers, servers):
        listview = self.query_one(ListView)
        to_remove = {srv["name"] for srv in old_servers} - {
            srv["name"] for srv in servers
        }
        to_add = {srv["name"] for srv in servers} - {srv["name"] for srv in old_servers}
        with self.app.batch_update():
            for srvname in sorted(to_add):
                # Node id can only contain letters, numbers, underscores, or hyphens,
                # and must not begin with a number.
                # TODO find a safer way. Maybe using ids is a bad idea.
                srv_id = srvname.replace("/", "-").replace("+", "-").lower()
                w = ServerLabel(srvname, id=srv_id)
                await listview.append(ListItem(w))
            for srvname in to_remove:
                srv_id = srvname.replace("/", "-").lower()
                w = self.get_widget_by_id(srv_id)
                w.parent.remove()
            for srv in servers:
                try:
                    srv_id = srv["name"].replace("/", "-").lower()
                    w = self.get_widget_by_id(srv_id)
                    w.state = srv["state"]
                except NoMatches:
                    pass

            # Update level title
            n_servers = len(servers)
            n_running = len([s for s in servers if s["state"] == tango.DevState.ON])
            n_stopped = n_servers - n_running
            coll = self.query_one(Collapsible)
            if n_stopped:
                coll.title = (
                    f"{self._title}  [#00ff00]●[/]{n_running}  [#ff0000]⨯[/]{n_stopped}"
                )
            else:
                coll.title = f"{self._title}  [#00ff00]●[/]{n_running}"

    def action_start(self):
        listview = self.query_one(ListView)
        if listview.has_focus:
            list_item = listview.highlighted_child
            server_label = list_item.children[0]
            srv = server_label._server
            self._start_servers(srv)
        else:
            servers = [s["name"] for s in self.servers]
            self._start_servers(*servers)

    def action_stop(self):
        listview = self.query_one(ListView)
        if listview.has_focus:
            list_item = listview.highlighted_child
            server_label = list_item.children[0]
            srv = server_label._server
            self._stop_servers(srv)
        else:
            servers = [s["name"] for s in self.servers]
            self._stop_servers(*servers)

    def action_kill(self):
        listview = self.query_one(ListView)
        if listview.has_focus:
            list_item = listview.highlighted_child
            server_label = list_item.children[0]
            srv = server_label._server
            self._stop_servers(srv, kill=True)
        else:
            servers = [s["name"] for s in self.servers]
            self._stop_servers(*servers, kill=True)

    @work
    async def _start_servers(self, *servers: str):
        async def maybe_start(start):
            if start:
                results = await asyncio.wait(
                    [
                        asyncio.create_task(start_server(self._hostname, s))
                        for s in servers
                    ]
                )
                failed = [
                    s for s, r in zip(servers, results) if isinstance(r, Exception)
                ]
                started = [s for s, r in zip(servers, results) if r]
                if failed:
                    message = "\n - ".join(["Failed to start:", *failed])
                    self.app.notify(message)
                if started:
                    self.app.notify(f"Done starting {len(servers)} servers!")
                else:
                    self.app.notify("Nothing to start!")

        server_lines = "\n - ".join(servers)
        question = f"Do you really want to start these servers?\n - {server_lines}"
        self.app.push_screen(ConfirmModal(question), maybe_start)

    @work
    async def _stop_servers(self, *servers: str, kill=False):
        verb = "kill" if kill else "stop"

        async def maybe_stop(start):
            if start:
                results = await asyncio.wait(
                    [
                        asyncio.create_task(stop_server(self._hostname, s, force=kill))
                        for s in servers
                    ]
                )
                failed = [
                    s for s, r in zip(servers, results) if isinstance(r, RuntimeError)
                ]
                if failed:
                    message = "\n - ".join([f"Failed to {verb}:", *failed])
                    self.app.notify(message)
                else:
                    self.app.notify(f"Done {verb}ing servers!")

        server_lines = "\n - ".join(servers)
        question = f"Do you really want to {verb} these servers?\n - {server_lines}"
        self.app.push_screen(ConfirmModal(question), maybe_stop)


class TangoServerLevels(TangoEntryWidget):
    DEFAULT_CSS = """
    TangoServerLevels {
        height: 1fr;
        overflow-y: auto;
    }
    """

    async def entry_changed(self, entry: TangoHost):
        listener = get_listener()
        servers_attr = f"{entry.get_starter()}/servers".lower()
        async for _, event in listener.listen([servers_attr]):
            if isinstance(event, tango.DevFailed):
                self.error = event
                continue
            servers = sorted(
                (entry._parse_server(line) for line in event.value),
                key=lambda srv: srv["level"] * srv["mode"],
            )
            by_level = groupby(servers, lambda srv: srv["level"] * srv["mode"])
            for level, level_servers in by_level:
                level_id = f"level-{level}"
                try:
                    level_w = self.get_widget_by_id(level_id, expect_type=StarterLevel)
                except NoMatches:
                    level_w = StarterLevel(entry.name, level, id=level_id)
                    await self.mount(level_w)
                level_w.servers = sorted(level_servers, key=lambda s: s["name"].lower())

    def on_list_view_highlighted(self, message):
        if message.item:
            self.scroll_to_widget(message.item)

    async def on_list_view_selected(self, message):
        message.item.children[0]._server


class TangoHostBanner(TangoBanner[TangoHost]):
    def compose(self):
        with Horizontal():
            yield Static(id="name")
            yield DeviceState(id="state")

    async def entry_changed(self, entry: TangoHost):
        starter_entry = await get_device_entry(f"tango/admin/{entry.name}")
        self.query_one(DeviceState).set_entry(starter_entry)
        usage = await entry.get_usage()
        if usage:
            title = f"Starter: [bold]{entry.name}[/] ({usage})"
        else:
            title = f"Starter: [bold]{entry.name}"
            self.get_widget_by_id("name", expect_type=Static).update(title)
