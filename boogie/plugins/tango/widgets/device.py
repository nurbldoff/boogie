from __future__ import annotations
from typing import Generic, ClassVar

from dateutil.parser import parse
from textual import on, work
from textual.containers import Horizontal, Vertical
from textual.widgets import (
    Static,
    Label,
    Collapsible,
    Input,
    Button,
    TabbedContent,
    TabPane,
)
import tango

from boogie.widgets.misc import InfoTable
from boogie.widgets.modal import ConfirmModal, GetValueModal, GetTextModal
from boogie.util import all_subclasses
from boogie.entry import EntryType
from .details import TangoDetails, TangoEntryWidget, TangoBanner
from .error import TangoError
from ..listener import get_listener
from ..device import TangoDevice


class TangoDeviceDetailsBase(TangoDetails, Generic[EntryType]):
    DEFAULT_CSS = """
    TangoDeviceDetailsBase {
        #info {
            height: auto;
            overflow-y: auto;
            hatch: right $background-lighten-1;
            &.error .actions {
                display: none;
            }
        }
        #custom {
            height: 1fr;
        }
    }
    """

    ACTIONS = [
        (
            "init",
            "Init",
            "warning",
            "Run the Init() command on the device",
        ),
        # ("Restart", "error", "restart", "Restart the device via the DServer"),
        # ("delete", "Delete", "error", "Delete this device from the database",),
    ]

    BINDINGS = [
        *TangoDetails.BINDINGS,
        ("delete", "delete", "Delete"),
        ("m", "monitor", "Monitor"),
    ]

    def compose(self):
        with TabbedContent():
            with TabPane("Info", id="info"):
                yield DeviceInfo()
                yield DeviceStatus()

                with Horizontal(id="actions", classes="actions"):
                    for method, action, variant, tooltip in self.ACTIONS:
                        button = Button(
                            action, variant=variant, classes="small action", id=method
                        )
                        button.tooltip = tooltip
                        yield button

            with TabPane("Custom", id="custom"):
                pass
        yield TangoError()

    async def entry_changed(self, entry):
        await self.query(TangoDeviceClassCustomWidget).remove()

        for widget in self.query(TangoEntryWidget):
            widget.set_entry(entry, reload=True)

        try:
            # Check for "custom" widgets for the corresponding device class
            dev_cls = await entry.get_class()
            custom_widget_cls = TangoDeviceClassCustomWidget.get_device_widget_class(
                dev_cls
            )
            self.log("Found custom device widget", dev=dev_cls, cls=custom_widget_cls)
            await self.query(TangoDeviceClassCustomWidget).remove()
            if custom_widget_cls:
                self.query_one("Tabs").styles.height = None
                # TODO set tab title according to widget. Not currently possible.
                if await custom_widget_cls.pre_check(entry):
                    custom_widget = custom_widget_cls()
                    self.query_one("TabPane#custom", TabPane).mount(custom_widget)
                    custom_widget.set_entry(entry)
            else:
                self.query_one("Tabs").styles.height = 0
        except tango.DevFailed:
            pass

        self._check_state(entry)

    @work(exclusive=True)
    async def _check_state(self, entry):
        device = await entry.get_device()
        state_attr = f"{device}/state"
        listener = get_listener()
        async for attr, data in listener.listen([state_attr]):
            if isinstance(data, tango.DevFailed) and self._error != data:
                self._error = data
            elif self._error:
                self._error = None

    async def action_delete(self):
        name = await self.entry.get_device()

        async def maybe_delete(doit):
            if doit:
                await self.app.run_entry_method(self.entry, self.entry.delete)
                await self.screen.reload()
                self.app.notify(f"Deleted device '{name}'")

        message = f"Really delete device {name}?"
        self.app.push_screen(ConfirmModal(message), maybe_delete)

    def action_monitor(self):
        # self.post_message(AddToMonitor(self.entry))
        self.app.add_to_monitor(self.entry)

    async def on_button_pressed(self, message):
        if message.button.id == "alias":
            await self.edit_alias()
        elif message.button.id == "description":
            await self.edit_description()

    async def edit_alias(self):
        async def maybe_update(new_alias):
            if new_alias is not None:
                if new_alias == "":
                    await self.app.run_entry_method(self.entry, self.entry.delete_alias)
                    self.app.notify(f"Removed alias for {self.entry.name}")
                else:
                    await self.app.run_entry_method(
                        self.entry, self.entry.put_alias, value=new_alias
                    )
                    self.app.notify(f"Changed alias for {self.entry.name}")
                self.reload()

        message = f"Edit alias for {self.entry.name}"
        alias = await self.entry.get_alias()
        self.app.push_screen(
            GetValueModal(message, placeholder="Alias", initial=alias), maybe_update
        )

    async def edit_description(self):
        async def maybe_update(new_desc):
            if new_desc is not None:
                await self.app.run_entry_method(
                    self.entry, self.entry.put_description, value=new_desc
                )
                self.app.notify(
                    f"Changed description for {self.entry.name}."
                    + "\nNote: only updates on device start."
                )
                self.reload()

        message = f"Edit description for {self.entry.name}"
        description = await self.entry.get_description()
        self.app.push_screen(GetTextModal(message, initial=description), maybe_update)


class TangoDeviceDetails(TangoDeviceDetailsBase[TangoDevice]):
    pass


class DeviceInfoTable(InfoTable):
    FIELDS = [
        ("Name", "name"),
        ("Alias", "alias", str),
        ("Description", "description", str),
        ("Class", "class"),
        ("Server", "server"),
        ("DServer", "dserver"),
    ]

    @on(Input.Submitted)
    def changed(self, message):
        name = message.control.id
        alias = message.control.value
        print(f"put_alias {name}: {alias}")
        self.post_message(self.Changed(name, alias))


class DeviceInfo(TangoEntryWidget):
    DEFAULT_CSS = """
    DeviceInfo {
        width: 1fr;
        height: auto;
        display: none;
    }
    """

    def compose(self):
        self.classes = "section"
        self.border_title = "Information"
        yield DeviceInfoTable()

    async def entry_changed(self, entry):
        await self._update_info(entry)

    def _get_device_link(self, info):
        path = f"Device:{info.name.replace('/', ':')}"
        return f"[@click=app.go_to('{path}')]{info.name}[/]"

    def _get_class_link(self, info):
        path = f"Class:{info.class_name}"
        return f"[@click=app.go_to('{path}')]{info.class_name}[/]"

    def _get_server_link(self, info):
        path = f"Server:{info.ds_full_name.replace('/', ':')}"
        return f"[@click=app.go_to('{path}')]{info.ds_full_name}[/]"

    def _get_dserver_link(self, info):
        if info.class_name == "DServer":
            return None
        path = f"Device:dserver:{info.ds_full_name.lower().replace('/', ':')}"
        return f"[@click=app.go_to('{path}')]dserver/{info.ds_full_name}[/]"

    def _format_timestamp(self, ts):
        if ts:
            dt = parse(ts)
            return dt.isoformat(sep=" ")

    async def _update_info(self, entry):
        config = {}
        try:
            info = await entry.get_db_info()
        except tango.DevFailed as e:
            self._error = e
            return
        config["name"] = self._get_device_link(info)
        config["class"] = self._get_class_link(info)
        config["server"] = self._get_server_link(info)
        dserver = self._get_dserver_link(info)
        if dserver:
            config["dserver"] = dserver

        alias = await entry.get_alias()
        config["alias"] = alias

        try:
            description = await entry.get_description()
            config["description"] = description
        except tango.DevFailed:
            # TODO get description property
            pass
        await self.query_one(DeviceInfoTable).set_config(config)
        self.display = True

    async def action_edit_alias(self, alias: str) -> None:
        print("***** hej *****", alias)

    async def on_info_table_changed(self, message):
        if message.name == "alias":
            device = await self.entry.get_device()
            new_alias = message.value
            try:
                if new_alias:
                    await self.app.run_entry_method(
                        self.entry, self.entry.put_alias, alias=new_alias
                    )

                    self.notify(
                        f"Changed alias of device [bold]{device}[/] to [bold]{message.value}[/]"
                    )
                else:
                    await self.app.run_entry_method(self.entry, self.entry.delete_alias)
                    self.notify(f"Removed alias from device [bold]{device}[/]")
            except tango.DevFailed:
                # TODO clear the value?
                pass
            else:
                await self.app.screen.reload()


class DeviceStatus(TangoEntryWidget):
    DEFAULT_CSS = """
    DeviceStatus {
        overflow-y: auto;
        display: none;
        height: auto;
        max-height: 1fr;
    }
    """

    def compose(self):
        self.classes = "section"
        self.border_title = "Status"
        yield Static(markup=False)

    def on_mount(self):
        self.border_title = "Status"

    async def entry_changed(self, entry):
        device = await entry.get_device()
        attr = f"{device}/status"
        prev_status = None
        static = self.query_one(Static)
        listener = get_listener()
        async for _, data in listener.listen([attr]):
            match data:
                case tango.DevFailed():
                    self.display = False
                    prev_status = None
                case tango.DeviceAttribute():
                    status = str(data.value).strip()
                    if status != prev_status:
                        static.update(status)
                        self.display = True
                    prev_status = status


class DeviceState(Static, TangoEntryWidget):
    DEFAULT_CSS = """
    DeviceState {
        height: 3;
        min-width: 13;
        LoadingIndicator.-textual-loading-indicator {
            background: $background;
        }
    }
    """

    async def entry_changed(self, entry):
        print("DeviceState update")
        last_value = None
        device = await entry.get_device()
        state_attr = f"{device}/state"
        status_attr = f"{device}/status"
        listener = get_listener()
        self.loading = True
        async for attr, data in listener.listen([state_attr, status_attr]):
            self.loading = False
            name = attr.rsplit("/")[-1]
            match name, data:
                case "state", tango.DevFailed():
                    if data.args[0].reason == "API_AttrNotFound":
                        self.update("???")
                    else:
                        state = "N/A"
                        self.update(state)
                        self.classes = "tango-state state-NA"
                    last_value = None
                case "state", tango.DeviceAttribute():
                    if data.value != last_value:
                        state = str(data.value)
                        self.update(state)
                        self.classes = f"tango-state state-{state}"
                        last_value = data.value
                case "status", tango.DevFailed():
                    self.tooltip = data.args[-1].desc
                case "status", tango.DeviceAttribute():
                    self.tooltip = data.value


class TangoDeviceBanner(TangoBanner[TangoDevice]):
    def compose(self):
        with Horizontal():
            yield Static(id="name")
            yield DeviceState(id="state")

    async def entry_changed(self, entry):
        self.query_one("#state").set_entry(entry)
        title = await entry.get_title()
        self.query_one("#name").update(f"{title}")


class SubDevices(TangoEntryWidget, can_focus=False):
    DEFAULT_CSS = """
    SubDevices Vertical {
        height: auto;
    }
    """

    def compose(self):
        c = Collapsible(title="Subdevices", collapsed=False)
        c.tooltip = "Other devices that this device communicates with."
        with c:
            yield Vertical()

    async def entry_changed(self, entry):
        container = self.query_one(Vertical)
        container.query(Label).remove()
        subdevices = await entry.get_subdevices()
        for subdev in subdevices:
            path = f"Device:{subdev.replace('/', ':')}"
            container.mount(Label(f"[@click=app.go_to('{path}')]{subdev}[/]"))
        # container.display = True


class TangoDeviceClassCustomWidget(TangoEntryWidget):
    """
    Base class for custom device widgets. If one is implemented for
    the current entry class, it will be found and displayed in the
    device details widget.
    """

    DEFAULT_CSS = """
    TangoDeviceClassCustomWidget {
        height: auto;
    }
    """

    TANGO_DEVICE_CLASS: ClassVar[str]  # Name of the device class

    @classmethod
    async def pre_check(cls, entry: TangoDevice) -> bool:
        """
        Return whether the widget should be mounted or not.
        Default behavior is to only display if the device is running.
        It could be used to do further checks on the device.
        """
        try:
            await entry.ping()
        except tango.DevFailed:
            return False
        return True

    @classmethod
    def get_device_widget_class(
        cls, device_class: str
    ) -> type[TangoDeviceClassCustomWidget] | None:
        """
        Return a suitable widget class for the given Tango device class.
        The prerequisite is that the widget has been imported so that it's
        in the class hierarchy.
        """
        subclasses = all_subclasses(cls)
        matches = []
        for subclass in subclasses:
            if getattr(subclass, "TANGO_DEVICE_CLASS", None) == device_class:
                matches.append(subclass)
        if matches:
            return matches[0]  # TODO or -1?
        return None


# if __name__ == "__main__":

#     import sys

#     from textual.app import App
#     from textual.widgets import Footer

#     from boogie.plugins.tango.entry import get_server_device_entry

#     class AttributeApp(App):

#         CSS_PATH = ["../../../app2.css"]

#         def compose(self):
#             """Create child widgets for the app."""
#             yield DeviceState()
#             yield Footer()

#         async def on_ready(self):
#             device = sys.argv[1]
#             entry = await get_server_device_entry(device)
#             self.query_one(DeviceState).set_entry(entry)

#     app = AttributeApp()

#     app.run()
