"""
A stand-alone "trend" application.
Also plugged in as a subcommand.
"""

import asyncio
from collections import deque
from datetime import datetime, timedelta
from time import time

from rich.text import Text
import tango
from textual import work
from textual.app import App, ComposeResult
from textual.widgets import Static, Footer
from textual.reactive import reactive

from boogie.plot import time_plot
from boogie.plugins.tango.utils import get_attribute_proxy
from boogie.plugins.tango.device import TangoDeviceAttribute
from boogie.plugins.tango.listener import get_listener


class AttributeTrend(Static):
    """
    Plot a scalar attribute value over time
    """

    DEFAULT_CSS = """
    AttributeTrend {
        padding: 0;
        margin: 0;
        height: 100%;

    }
    """

    attributes: reactive[list[TangoDeviceAttribute]] = reactive([], init=False)
    poll_period: reactive[float] = reactive(3.0)
    zoom: reactive[int] = reactive(-1)
    buffer_size: reactive[int] = reactive(10000)

    COLORS = ["red", "green", "blue", "yellow", "grey", "purple"]

    def on_mount(self):
        self._data = {}

    async def watch_attributes(self, _, attributes):
        self._data = {
            name: (deque(maxlen=self.buffer_size), deque(maxlen=self.buffer_size))
            for name in attributes
        }
        self.listen()
        self._run()

    def watch_poll_period(self, _, period):
        self.listen()
        self.update()

    def watch_zoom(self, _, zoom):
        self._run()

    @work(exclusive=True, group="listen")
    async def listen(self):
        if not self.attributes:
            return
        listener = get_listener(poll_period=self.poll_period)
        async for attr, data in listener.listen(self.attributes):
            if isinstance(data, (tango.DevFailed, tango.AttributeInfoEx)):
                continue
            # name = attr.split("/", 3)[-1]
            ts, ys = self._data[attr]
            ys.append(data.value)
            ts.append(data.time.todatetime().astimezone(tz=None))

    @work(exclusive=True, group="run")
    async def _run(self):
        self.draw()
        t = time()
        await asyncio.sleep(t - int(t))
        timestep = max(0.25, 2**self.zoom)
        while True:
            self.draw()
            await asyncio.sleep(timestep)

    def draw(self):
        scale = 2**self.zoom
        now = datetime.fromtimestamp(int(time() / scale) * scale).astimezone(tz=None)
        p = time_plot(
            data=[
                (ts, ys, attr, color)
                for color, (attr, (ts, ys)) in zip(self.COLORS, self._data.items())
            ],
            x_limits=[
                now - timedelta(seconds=2 * self.size.width * scale - 1),
                now + timedelta(seconds=1),
            ],
            height=self.size.height,
            width=self.size.width,
        )
        self.update(Text.from_ansi(p))


class ScatterApp(App[None]):
    CSS = """
    Screen {
        padding: 0;
        margin: 0;
        background: $background;
        &:inline {
            border: none;
            height: 50vh;
        }
    }
    """

    BINDINGS = [
        ("plus", "zoom_in", "Zoom in"),
        ("minus", "zoom_out", "Zoom out"),
    ]

    def __init__(self, args):
        super().__init__()
        self._attributes = args.attribute
        self._poll_period = args.poll_period

    def compose(self) -> ComposeResult:
        yield AttributeTrend()
        yield Footer()

    async def on_mount(self) -> None:
        trend = self.query_one(AttributeTrend)
        trend.poll_period = self._poll_period
        bad_attributes = []
        good_attributes = []
        for attr in self._attributes:
            try:
                await get_attribute_proxy(*attr.rsplit("/", 1))
            except tango.DevFailed:
                bad_attributes.append(attr)
            else:
                good_attributes.append(attr)
        if bad_attributes:
            self.log("Bad attributes given", attributes=bad_attributes)
            # TODO report this to user somehow

        trend.attributes = good_attributes

    def action_zoom_in(self):
        trend = self.query_one(AttributeTrend)
        trend.zoom = trend.zoom - 1

    def action_zoom_out(self):
        trend = self.query_one(AttributeTrend)
        trend.zoom = trend.zoom + 1


def get_app(parser):
    "For plug-in usage"
    parser.description = "Plot live data from attributes in the terminal."
    parser.add_argument("attribute", nargs="+")
    parser.add_argument(
        "--fullscreen",
        "-f",
        dest="inline",
        action="store_false",
        help="Display using the entire terminal",
    )
    parser.add_argument("--poll-period", "-p", type=float, default=3.0,
                        help="How often to read attributes that don't push events")

    return ScatterApp
