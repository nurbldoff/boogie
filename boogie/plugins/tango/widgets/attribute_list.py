import tango
from rich.text import Text
from textual.binding import Binding

from .details_list import TangoDetailsList
from ..device import TangoDeviceAttributes


WRITABLE = {
    tango.AttrWriteType.READ: "R",
    tango.AttrWriteType.WRITE: "W",
    tango.AttrWriteType.READ_WRITE: "R/W",
    tango.AttrWriteType.READ_WITH_WRITE: "R+W",
}


class TangoDeviceAttributeListDetails(TangoDetailsList[TangoDeviceAttributes]):
    COLUMNS = "Attribute", "Label", "Type", "Format", "R/W", "Description"

    BINDINGS = TangoDetailsList.BINDINGS + [
        ("m", "monitor", "Monitor"),
        Binding("M", "monitor2", "Monitor2", show=False),
    ]

    async def get_row(self, entry):
        info = entry.info
        # TODO make the columns more compact, e.g. shorten format, writable...
        return (
            Text(info.name, style="bold"),
            info.label if info.label != info.name else "",
            str(tango.CmdArgType.values[info.data_type])[3:],
            str(tango.AttrDataFormat.values[info.data_format]),
            WRITABLE[tango.AttrWriteType.values[info.writable]],
            "" if info.description == "No description" else info.description,
        ), {}

    def action_monitor(self):
        entry = self.highlighted_entry
        self.app.add_to_monitor(entry)

    def action_monitor2(self):
        entry = self.highlighted_entry
        self.app.add_to_monitor(entry, show=True)
