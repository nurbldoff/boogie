from rich.text import Text
from textual.widgets import DataTable

from boogie.messages import NavigateTo

from .details_list import TangoDetailsList
from ..device import TangoDeviceSubdevices


class TangoSubdeviceListDetails(TangoDetailsList[TangoDeviceSubdevices]):
    """
    This is the list of "subdevices" to the device. This means other devices
    that the device has communication with. Note that under some circumstances
    subdevices may not appear in this list, e.g. when threading is involved.
    """

    COLUMNS = ("Name",)

    async def get_row(self, entry):
        return ((Text(entry.name, style="bold"),), {})

    async def on_data_table_cell_selected(
        self, message: DataTable.CellSelected
    ) -> None:
        message.prevent_default()
        entries = self.get_filtered_children(self.entries, self.pattern)
        entry = entries[message.coordinate.row]
        parts = entry.name.rsplit("/", 4)
        path = ["device", *parts[-3:]]
        device_entry = await self.app.get_entry(path)
        self.post_message(NavigateTo(device_entry))
