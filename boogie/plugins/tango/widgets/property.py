from datetime import datetime
import difflib
import os
import tempfile
import shlex
import subprocess
from typing import Any

from rich.markup import escape
import tango
from textual import on
from textual.containers import Horizontal, Vertical, Grid
from textual.events import Key
from textual.screen import ModalScreen
from textual.widgets import (
    Button,
    DataTable,
    Select,
    Input,
    TextArea,
    TabbedContent,
    TabPane,
)

from boogie.widgets.entry import EntryWidget
from boogie.messages import Reload, NavigateTo
from ..device import TangoDeviceProperty, TangoDeviceProperties
from .details import TangoDetails

# from ..utils import edit_text, suspend_app


class TangoDevicePropertyDetails(TangoDetails[TangoDeviceProperty], can_focus=False):
    DEFAULT_CSS = """
    TangoDevicePropertyDetails {
        height: 1fr;
        TabbedContent {
            height: 1fr;
        }
        TabPane {
            height: 1fr;
        }
    }
    """

    def compose(self):
        with TabbedContent():
            with TabPane("Value", id="value"):
                yield TangoPropertyValue()
            with TabPane("History", id="history"):
                yield TangoPropertyHistory()

    def on_mount(self) -> None:
        self.query_one("Tabs").can_focus = False
        self.classes = "header-border"

    async def entry_changed(self, entry: TangoDeviceProperty) -> None:
        self.border_title = str(entry)
        with self.app.batch_update():
            self.query_one(TangoPropertyValue).set_entry(entry, reload=True)
            self.query_one(TangoPropertyHistory).set_entry(entry, reload=True)
            if len(self.subpath) > 1:
                tab_id = self.subpath[1].name
                tabbed = self.query_one(TabbedContent)
                tabbed.active = tab_id

    @on(TabbedContent.TabActivated)
    async def on_tab_activated(self, message: TabbedContent.TabActivated) -> None:
        message.stop()  # Don't leak these up


class EditPropertyModal(ModalScreen[tuple[str, list[str]]]):
    DEFAULT_CSS = """
    EditPropertyModal {
        align: center middle;
        .frame {
            width: 75%;
            height: 75%;
            border: panel $secondary;
            layout: vertical;
            #value {
                border: solid $primary;
                height: 1fr;
                width: 1fr;
                margin: 0 1;
            }
            Select {
                display: none;
            }
            Input {
                width: 1fr;
                height: auto
            }
            TextArea {
                max-height: 1fr;
                height: 100%;
            }
            Horizontal {
                padding: 0 1;
                height: auto;
            }
            Button {
                width: 1fr;
            }
        }
    }
    """

    def __init__(
        self, entry: TangoDeviceProperties, name=None, value=None, *args, **kwargs
    ):
        super().__init__(*args, **kwargs)
        self.entry = entry
        self._name = name
        self._value = value
        self.defaults: dict[str, Any] = {}

    def compose(self):
        with Grid(classes="frame") as v:
            if self._name:
                v.border_title = "Editing property"
            else:
                v.border_title = f"Creating new property for device [b]{self._name}"
            yield Select([])
            yield Input(placeholder="Name")
            with Vertical(id="value") as v2:
                v2.border_title = "Value"
                yield TextArea(show_line_numbers=True, soft_wrap=False)
            with Horizontal():
                yield Button("Cancel", variant="error", id="no")
                yield Button("Proceed", variant="primary", id="yes")

    async def on_mount(self):
        # Ask the device what properties it recognizes, if it's running
        # In that case, populate the select widget.
        select = self.query_one(Select)
        input_ = self.query_one(Input)
        if self._name:
            select.visible = False
            input_.visible = True
            # Editing an existing property
            input_.value = self._name
            text_area = self.query_one(TextArea)
            text_area.text = self._value
            text_area.focus()
        else:
            # We're creating a new property here
            try:
                descriptions, defaults = await self.entry.get_info()
                self.defaults = defaults
            except tango.DevFailed:
                # Device isn't running, so no info is available
                input_.visible = True
                select.disabled = True
                select.prompt = "Device not running"
                return
            options = [
                (f"[b]{name}:[/b] [#aaaaaa]{escape(description)}", name)
                for name, description in descriptions.items()
            ]
            select.set_options(options)
            select.disabled = False
            select.display = True

    def on_select_changed(self, message):
        if message.value != Select.BLANK:
            # User has selected one of the device defined properties
            self.query_one(Input).disabled = True
            name = message.value
            if name in self.defaults:
                # Preset the default property value
                self.query_one(TextArea).text = self.defaults[name]
        else:
            self.query_one(Input).disabled = False

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "yes":
            input_ = self.query_one(Input)
            name: str
            if input_.disabled:
                name = str(self.query_one(Select).value)
            else:
                name = input_.value
            value = self.query_one(TextArea).text.splitlines()
            self.dismiss((name, value))
        else:
            self.dismiss()

    def on_key(self, event: Key) -> None:
        if event.key == "escape":
            event.prevent_default()
            self.dismiss()


class TangoPropertyValue(EntryWidget, can_focus=False):
    DEFAULT_CSS = """
    TangoPropertyValue {
        background: $background;
        hatch: right $background-lighten-1;
        height: 1fr;
        Static {
            background: $primary-darken-1;
        }
        TextArea {
            max-height: 100%;
            height: 1fr;
        }
        &.editing {
            border: solid $secondary;
        }
    }
    """

    BINDINGS = [
        ("e", "edit", "Edit"),
        ("E", "open_in_editor", "Open in $EDITOR"),
        ("c", "copy", "Copy"),
    ]

    def compose(self):
        yield TextArea(
            show_line_numbers=True, read_only=True, soft_wrap=False, classes="editing"
        )
        self.border_title = "Editing"
        self._value = ""

    async def entry_changed(self, entry: TangoDeviceProperty) -> None:
        raw_value = await entry.get_value()
        self._value = self._value = "\n".join(raw_value)

        self.query_one(TextArea).load_text(self._value)

    def action_edit(self) -> None:
        async def maybe_save(result) -> None:
            if result:
                name, value = result
                if name:
                    assert self.entry
                    if name != self.entry.name:
                        # Renamed
                        await self.entry.parent.put(name, value)
                        new_entry = await self.entry.parent.get_child(name)
                        await self.entry.delete()
                        self.post_message(NavigateTo(new_entry))
                        # TODO should also reload the property list, but how?
                        return
                    if value:
                        try:
                            assert self.entry
                            await self.app.run_entry_method(
                                self.entry, self.entry.set_value, value=value
                            )
                            self.notify(f"Saved property '{name}'")
                            self.set_entry(self.entry, reload=True)
                        except tango.DevFailed as e:
                            self.notify(f"Failed to save property: {e}")
                    else:
                        self.notify("Can't save empty property.")
            else:
                self.notify("Property not saved!")

        # assert isinstance(self.entry, TangoDeviceProperties)
        assert self.entry
        modal = EditPropertyModal(
            self.entry.parent, name=self.entry.name, value=self._value
        )
        self.app.push_screen(modal, maybe_save)

    def check_action(self, action: str, _) -> bool:
        if action == "open_in_editor":
            return bool(os.environ.get("EDITOR"))
        return True

    async def action_open_in_editor(self) -> None:
        """
        Open an external editor (determined by the EDITOR environment variable)
        with the property value, to allow more advanced editing operations.
        """
        editor_command = os.environ.get("EDITOR")
        # The action is disabled if the env var is not set
        assert editor_command is not None
        editor_args: list[str] = shlex.split(editor_command)

        with tempfile.NamedTemporaryFile(delete=False) as temp_file:
            temp_file_name = temp_file.name
            temp_file.write(self._value.encode("utf-8"))
            temp_file.flush()
        t0 = os.path.getmtime(temp_file_name)

        editor_args.append(temp_file_name)

        with self.app.suspend():
            try:
                subprocess.call(editor_args)
            except OSError:
                self.app.notify(
                    severity="error",
                    title="Can't run command",
                    message=f"The command [b]{editor_command}[/b] failed to run.",
                )

        t1 = os.path.getmtime(temp_file_name)
        if t1 == t0:
            self.notify("No changes made!")
        else:
            with open(temp_file_name, "r", encoding="utf-8") as temp_file:
                text = temp_file.read()
            value = text.splitlines()
            assert self.entry
            await self.app.run_entry_method(
                self.entry, self.entry.set_value, value=value
            )
            self.set_entry(self.entry, reload=True)
            self.notify("Property value updated!", severity="warning")

        os.remove(temp_file_name)

    def action_copy(self) -> None:
        area = self.query_one(TextArea)
        text = area.selected_text or area.text
        if text:
            self.app.copy_to_clipboard(text, "Copied to clipboard")


class TangoPropertyHistory(EntryWidget):
    DEFAULT_CSS = """
    TangoPropertyHistory {
        height: 1fr;
    }

    TangoPropertyHistory DataTable {
        height: 1fr;
    }
    """

    def compose(self):
        data_table = DataTable()
        data_table.add_columns("Timestamp", "#lines", "Value (first 10 lines)")
        data_table.cursor_type = "row"
        data_table.zebra_stripes = True
        yield data_table
        self.can_focus = False

    def focus(self, **kwargs):
        # Make sure to focus the table
        data_table = self.query_one(DataTable)
        data_table.focus()

    def parse_property_history_time(self, time: str) -> datetime | None:
        try:
            return datetime.strptime(time, "%d/%m/%Y %H:%M:%S")
        except Exception as e:
            print("Failed to parse %r: %r", time, e)
        return None

    async def entry_changed(self, entry: TangoDeviceProperty) -> None:
        with self.app.batch_update():
            data_table = self.query_one(DataTable)
            data_table.clear()
            history = await entry.get_history()
            n_items = len(history)
            for index, item in enumerate(reversed(history)):
                value = item.get_value()
                deleted = item.is_deleted()
                data_table.add_row(
                    self.parse_property_history_time(item.get_date()),
                    "DELETED" if deleted else len(value),
                    "\n".join(value[:10]) if value else "",
                    height=None,
                    key=str(n_items - index - 1),
                )

    @on(DataTable.RowSelected)
    def on_selected(self, message: DataTable.RowSelected) -> None:
        async def maybe_restore(restore):
            if restore:
                history = await self.entry.get_history()
                item = history[index]
                await self.app.run_entry_method(
                    self.entry, self.entry.set_value, value=item.get_value()
                )
                self.post_message(Reload())

        assert message.row_key.value
        index = int(message.row_key.value)
        assert self.entry
        self.app.push_screen(
            PropertyHistoryDiffModal(self.entry, index),
            maybe_restore,
        )


class PropertyHistoryDiffModal(ModalScreen):
    """
    Screen with a yes/no dialog.
    """

    DEFAULT_CSS = """
    PropertyHistoryDiffModal {
        Vertical {
            height: 100%;
            width: 100%;
            margin: 2 4;
            layout: grid;
            grid-size: 1 2;
            grid-rows: 1fr auto;
            border: solid white;
            background: $background;
            border-title-align: center;
            DataTable {
                height: auto;
            }
            Horizontal {
                height: auto;
                width: 1fr;
                Button {
                    width: 1fr;
                }
            }
        }
    }
    """

    def __init__(self, entry: TangoDeviceProperty, index: int, *args, **kwargs) -> None:
        self.entry = entry
        self.index = index
        super().__init__(*args, **kwargs)

    def compose(self):
        with Vertical() as v:
            v.border_title = f"Diff for property '{self.entry.name}'"
            dt = DataTable()
            dt.add_columns("Current", "Historical")
            dt.show_cursor = False
            dt.zebra_stripes = True
            yield dt
            with Horizontal():
                yield Button("Cancel", variant="error", id="no")
                b = Button("Restore", variant="primary", id="yes")
                b.tooltip = (
                    "This will overwrite the current property value with "
                    + " the historical value."
                )
                yield b

    async def on_mount(self) -> None:
        history = await self.entry.get_history()
        current_value = list(history[-1].get_value())
        historical_value = list(history[self.index].get_value())
        # Show a simple line based diff (experimental!)
        diff = difflib.ndiff(current_value, historical_value)
        current = []
        historical = []
        for line in diff:
            stripped_line = line[2:]
            if line.startswith("  "):
                # Line in both
                current.append(stripped_line)
                historical.append(stripped_line)
            elif line.startswith("+ "):
                # Line added
                current.append("[#505050]---[/#505050]")
                historical.append(f"[#00FF00]{stripped_line}[/#00FF00]")
            elif line.startswith("- "):
                # Line removed
                current.append(f"[#FF0000]{stripped_line}[/#FF0000]")
                historical.append("[#505050]---[/#505050]")
            elif line.startswith("? "):
                # Subline info
                pass
        dt = self.query_one(DataTable)
        dt.add_rows(zip(current, historical))

    def on_button_pressed(self, message: Button.Pressed) -> None:
        if message.button.id == "yes":
            self.dismiss(True)
        else:
            self.dismiss(False)

    def on_key(self, event: Key) -> None:
        if event.key == "escape":
            event.prevent_default()
            self.dismiss(False)


if __name__ == "__main__":
    import sys

    from textual.app import App

    from boogie.plugins.tango.getters import get_device_entry

    class PropertyApp(App):
        CSS_PATH = ["../../../app2.css"]

        def compose(self):
            """Create child widgets for the app."""
            yield TangoDevicePropertyDetails()

        async def on_ready(self):
            device, prop = sys.argv[1:]
            dev_entry = await get_device_entry(device)
            properties_entry = await dev_entry.get_child("properties")
            entry = await properties_entry.get_child(prop)
            self.query_one(TangoDevicePropertyDetails).set_entry(entry)

    app = PropertyApp()

    app.run()
