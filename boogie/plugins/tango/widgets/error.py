from rich.text import Text
from rich.table import Table

import tango

from boogie.widgets.entry import Error


def format_devfailed(error: tango.DevFailed) -> Table:
    "Render a DevFailed exception into something nice"
    table = Table(
        padding=(0, 0),
        show_header=False,
        show_edge=False,
        show_lines=False,
        collapse_padding=True,
        border_style="#552222",
    )
    table.add_column(max_width=12, justify="right", no_wrap=True, style="bold")
    table.add_column(overflow="fold")
    for i, err in enumerate(error.args):
        table.add_row(Text("Argument", style="bold"), f"{i} ({type(err).__name__})")
        table.add_row(Text("Desc", style="bold"), str(err.desc).strip())
        table.add_row(Text("Reason", style="bold"), str(err.reason).strip())
        table.add_row(Text("Severity", style="bold"), str(err.severity).strip())
        table.add_row(Text("Origin", style="bold"), str(err.origin).strip())
        table.add_section()
    return table


class TangoError(Error):
    def watch_error(self, old_error, error):
        if isinstance(error, tango.DevFailed):
            self.border_title = f"Tango error: {type(error).__name__}"
            table = format_devfailed(error)
            self.update(table)
        else:
            super().watch_error(old_error, error)
