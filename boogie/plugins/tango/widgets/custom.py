"""
Widgets sub-classing TangoDeviceClassCustomWidget will be found
when visiting a device matching the "TANGO_DEVICE_CLASS" class
variable, and added to the details.
"""

from rich.pretty import Pretty
import tango
from textual import on
from textual.widgets import Static, Button, DataTable, Label
from textual.containers import Horizontal
from textual.widgets.data_table import DuplicateKey

from boogie.messages import NavigateTo
from .device import TangoDeviceClassCustomWidget
from ..device import TangoDevice
from ..listener import get_listener
from ..getters import get_device_entry, get_attribute_entry
from ..utils import get_short_name, get_attr_prefix


class DatabaseWidget(TangoDeviceClassCustomWidget):
    TITLE = "Timing"

    TANGO_DEVICE_CLASS = "DataBase"

    CUSTOM_CSS = """
    DatabaseWidget {
        layout: grid;
        grid-size: 1 3;
        grid-rows: auto 1fr;
    }
    """

    def compose(self):
        yield Static("...", id="info", markup=False)
        table = DataTable()
        table.add_column("Command", key="index")
        table.add_column("Calls", key="calls")
        table.add_column("Min", key="minimum")
        table.add_column("Average", key="average")
        table.add_column("Max", key="maximum")
        table.cursor_type = "row"
        table.zebra_stripes = True
        yield table

        with Horizontal(classes="actions"):
            yield Button("ResetTimingValues", variant="primary")

    async def entry_changed(self, entry):
        proxy = await entry.get_proxy()
        info = await proxy.command_inout("DbInfo")
        info_widget = self.query_one("#info")
        info_widget.update("\n".join(info))
        proxy = await entry.get_proxy()
        index = await proxy.read_attribute("timing_index")
        table = self.query_one(DataTable)
        for name in index.value:
            table.add_row(name, "", "", key=name)
        listener = get_listener()
        async for attr, data in listener.listen(
            [
                f"{entry.name}/timing_calls",
                f"{entry.name}/timing_average",
                f"{entry.name}/timing_minimum",
                f"{entry.name}/timing_maximum",
            ]
        ):
            match(data):
                case tango.DevFailed():
                    self.error = data
                case tango.DeviceAttribute():
                    col = attr.rsplit("_", 1)[-1]
                    for row, value in zip(index.value, data.value):
                        if col == "calls":
                            value = int(value)
                        table.update_cell(row, col, value, update_width=True)

    @on(Button.Pressed)
    async def reset_timing_values(self):
        try:
            proxy = await self.entry.get_proxy()
            await proxy.command_inout("ResetTimingValues")
        except tango.DevFailed as e:
            self.app.notify(f"Failed: {e.args[-1].desc}")


class TangoTestWidget(TangoDeviceClassCustomWidget):
    TANGO_DEVICE_CLASS = "TangoTest"

    TITLE = "Test"

    def compose(self):
        yield Static()
        with Horizontal(classes="actions"):
            yield Button("SwitchStates", variant="primary", id="switch-states")

    async def entry_changed(self, entry):
        self.query_one(Static).update(
            f"I am a custom widget for TangoTest: '{entry.name}'!"
        )

    @on(Button.Pressed, selector="#switch-states")
    async def switch_states(self):
        try:
            proxy = await self.entry.get_proxy()
            await proxy.command_inout("SwitchStates")
        except tango.DevFailed as e:
            self.app.notify(f"Failed: {e.args[-1].desc}")


class PanicWidget(TangoDeviceClassCustomWidget):
    """
    Custom widget for PANIC compatible alarm devices.
    """

    DEFAULT_CSS = """
    PanicWidget {
        height: 1fr;
        Horizontal {
            height: auto;
        }
        DataTable {
            height: 1fr;
            width: 1fr;
        }
    }
    """

    TANGO_DEVICE_CLASS = "PyAlarm"

    TITLE = "Alarms"

    def compose(self):
        # self.classes = "section"
        with Horizontal():
            yield Label("Number of alarms: ")
            yield Static(id="nbr-alarms")
        table = DataTable()
        table.add_column("Tag")
        table.add_column("Severity")
        self._status_column = table.add_column("Status")
        self._timestamp_column = table.add_column("Timestamp")
        table.add_column("Description")
        table.cursor_type = "row"
        table.zebra_stripes = True
        yield table
        self.loading = True
        with Horizontal(classes="actions"):
            yield Button("Init", variant="primary", id="init")
            yield Button("ResetAll", variant="primary", id="reset-all")

    SEVERITY_STYLES = {
        "ALARM": "[red]",
        "WARNING": "[yellow]",
        "INFO": "[blue]",
        "DEBUG": "[green]",
    }

    async def entry_changed(self, entry: TangoDevice) -> None:
        try:
            proxy = await entry.get_proxy()
            nbr_alarms = await proxy.read_attribute("nbr_alarms")
            w = self.query_one("#nbr-alarms", expect_type=Static)
            w.update(str(nbr_alarms.value))

            props = await proxy.get_property(
                ["AlarmList", "AlarmDescriptions", "AlarmSeverities"]
            )
            alarm_list: list[str] = props.get("AlarmList", [])
            alarm_descs: list[str] = props.get("AlarmDescriptions", [])
            alarm_sevs: list[str] = props.get("AlarmSeverities", [])

            tags = [a.split(":")[0] for a in alarm_list]
            descs = dict(d.split(":", 1) for d in alarm_descs)
            sevs = dict(s.split(":", 1) for s in alarm_sevs)
            table = self.query_one(DataTable)
            for tag in tags:
                sev = sevs.get(tag, "ALARM")
                try:
                    table.add_row(
                        tag,
                        f"{self.SEVERITY_STYLES.get(sev, '')}{sev}",
                        "",
                        "",
                        descs.get(tag, ""),
                        key=tag,
                    )
                except DuplicateKey:
                    # TODO this should probably not be possible even to configure
                    pass

            device_name = await entry.get_device()
            remaining = {"activealarms", "failedalarms"}
            last: dict[str, set[str]] = {
                "activealarms": set(),
                "failedalarms": set(),
            }
            listener = get_listener()
            async for attr, data in listener.listen(
                [
                    f"{device_name}/ActiveAlarms",
                    f"{device_name}/FailedAlarms",
                ]
            ):
                if isinstance(data, tango.DevFailed):
                    self.error = data
                    self.loading = False
                    continue
                if isinstance(data, tango.AttributeInfoEx):
                    continue
                attrname = data.name.lower()
                rows = list(
                    map(
                        (
                            self.parse_active_alarm
                            if attrname == "activealarms"
                            else self.parse_failed_alarm
                        ),
                        data.value,
                    )
                )
                status = (
                    "[bold]ACTIVE" if attrname == "activealarms" else "[blue]FAILED"
                )
                new = set()
                old = last[attrname]
                with self.app.batch_update():
                    self.error = None
                    for tag, t in rows:
                        table.update_cell(tag, self._status_column, status)
                        table.update_cell(
                            tag, self._timestamp_column, t, update_width=True
                        )
                        old.discard(tag)
                        new.add(tag)
                    for tag in old:
                        table.update_cell(tag, self._status_column, "")
                        table.update_cell(
                            tag, self._timestamp_column, "", update_width=True
                        )
                last[attrname] = new
                table.sort(self._timestamp_column, reverse=True)
                if remaining:
                    remaining.discard(attrname)
                    if not remaining:
                        self.loading = False
        except Exception as e:
            self.set_error(e)

    @staticmethod
    def parse_active_alarm(s: str) -> tuple[str, str]:
        tag, rest = s.split(":", 1)
        timestamp, _ = rest.rsplit(":", 1)
        return tag, timestamp

    @staticmethod
    def parse_failed_alarm(s: str) -> tuple[str, str]:
        tag, timestamp = s.split(":", 1)
        return tag, timestamp

    @on(Button.Pressed, selector="#init")
    async def run_init(self):
        try:
            proxy = await self.entry.get_proxy()
            await proxy.command_inout("Init")
        except tango.DevFailed as e:
            self.app.notify(f"Failed: {e.args[-1].desc}")

    @on(Button.Pressed, selector="#reset-all")
    async def run_reset_all(self):
        try:
            proxy = await self.entry.get_proxy()
            await proxy.command_inout("ResetAll", "Greetings from Boogie!")
        except tango.DevFailed as e:
            self.app.notify(f"Failed: {e.args[-1].desc}")


class HdbConfigurationManagerWidget(TangoDeviceClassCustomWidget):
    TANGO_DEVICE_CLASS = "HdbConfigurationManager"

    DEFAULT_CSS = """
    HdbConfigurationManagerWidget {
        DataTable {
            height: 1fr;
        }
    }
    """

    def compose(self):
        table = DataTable()
        table.add_column("Archiver", key="archiver")
        table.add_column("State", key="state")
        table.add_column("Error", key="error")
        table.add_column("Total", key="total")
        table.add_column("NOK", key="nok")
        table.add_column("Pending", key="pending")
        table.cursor_type = "row"
        table.zebra_stripes = True
        yield table

        with Horizontal(classes="actions"):
            yield Button("ResetStatistics", variant="primary")

    async def entry_changed(self, entry):
        table = self.query_one(DataTable)
        listener = get_listener()
        async for attr, data in listener.listen(
            [
                f"{entry.name}/ArchiverStatus",
            ]
        ):
            if isinstance(data, tango.DevFailed):
                self.error = data
                continue
            if isinstance(data, tango.AttributeInfoEx):
                continue
            with self.app.batch_update():
                table.clear()
                for row in data.value:
                    archiver, rest = row.split("\t", 1)
                    if rest.startswith("Error="):
                        error = rest[6:]
                        state = "???"
                        nok = 0
                        total = 0
                        pending = ""
                    else:
                        try:
                            archiver, state, error, nok, pending = row.split("\t")
                            nok, total = nok.split("=")[1].split("/")
                            pending = int(pending.split("=")[1])
                        except ValueError:
                            error = rest
                            state = "???"
                            nok = total = 0
                            pending = ""
                    table.add_row(
                        archiver,
                        state,
                        error,
                        int(total),
                        int(nok),
                        pending,
                        key=archiver,
                    )

    @on(DataTable.RowSelected)
    async def select_row(self, message):
        entry = await get_device_entry(message.row_key.value)
        self.post_message(NavigateTo(entry))

    @on(Button.Pressed)
    async def reset_statistics(self):
        try:
            proxy = await self.entry.get_proxy()
            await proxy.command_inout("ResetStatistics")
        except tango.DevFailed as e:
            self.app.notify(f"Failed: {e.args[-1].desc}")


class HdbEventSubscriberWidget(TangoDeviceClassCustomWidget):
    TANGO_DEVICE_CLASS = "HdbEventSubscriber"

    DEFAULT_CSS = """
    HdbEventSubscriberWidget {
        DataTable {
            height: 1fr;
        }
    }
    """

    def compose(self):
        table = DataTable()
        table.add_column("Attribute", key="attribute")
        # table.add_column("NOK", key="nok")
        table.add_column("Status", key="status")
        table.add_column("Freq", key="freq")
        table.add_column("Pending", key="pending")
        table.add_column("Error", key="error")
        table.cursor_type = "row"
        table.zebra_stripes = True
        yield table

        with Horizontal(classes="actions"):
            yield Button("Stop", variant="error", id="stop")
            yield Button("Start", variant="primary", id="start")
            yield Button("Pause", variant="warning", id="pause")
            yield Button("Go to", id="go-to")

    async def entry_changed(self, entry):
        proxy = await entry.get_proxy()
        index = await proxy.read_attribute("AttributeList")
        table = self.query_one(DataTable)
        prefix = get_attr_prefix()
        for attr in index.value:
            name = attr.removeprefix(prefix)
            table.add_row(name, None, None, None, None, key=attr)

        # TODO some bugs here...
        statuses = {"started": set(), "stopped": set(), "paused": set(), "nok": set()}
        listener = get_listener()
        async for attr, data in listener.listen(
            [
                f"{entry.name}/AttributeNokList",
                f"{entry.name}/AttributeErrorList",
                f"{entry.name}/AttributeRecordFreqList",
                f"{entry.name}/AttributeStartedList",
                f"{entry.name}/AttributeStoppedList",
                f"{entry.name}/AttributePausedList",
            ]
        ):
            if isinstance(data, tango.DevFailed):
                self.error = data
                continue
            if isinstance(data, tango.AttributeInfoEx):
                continue
            col = attr.rsplit("/", 1)[-1][9:-4]
            prev_attrs = None
            if col in {"started", "stopped", "paused", "nok"}:
                prev_attrs = statuses[col] - set(data.value)
                for attr in prev_attrs:
                    table.update_cell(attr, "status", "")
                    statuses[col] = set(data.value)
            with self.app.batch_update():
                for i, value in enumerate(data.value):
                    if col == "recordfreq":
                        table.update_cell_at(
                            (i, 2), Pretty(float(value)), update_width=True
                        )
                    elif col == "pending":
                        table.update_cell_at(
                            (i, 3), Pretty(int(value)), update_width=True
                        )
                    elif col == "error":
                        # TODO can fail if there's a new line
                        table.update_cell_at((i, 4), value, update_width=True)
                    elif col in {"stopped", "paused", "nok"}:
                        table.update_cell(value, "status", col)

    @on(Button.Pressed, selector="#start")
    async def attribute_start(self):
        try:
            proxy = await self.entry.get_proxy()
            table = self.query_one(DataTable)
            key = table.coordinate_to_cell_key((table.cursor_row, 0)).row_key
            await proxy.command_inout("AttributeStart", key.value)
        except tango.DevFailed as e:
            self.app.notify(f"Failed: {e.args[-1].desc}")

    @on(Button.Pressed, selector="#stop")
    async def attribute_stop(self):
        try:
            proxy = await self.entry.get_proxy()
            table = self.query_one(DataTable)
            key = table.coordinate_to_cell_key((table.cursor_row, 0)).row_key
            await proxy.command_inout("AttributeStop", key.value)
        except tango.DevFailed as e:
            self.app.notify(f"Failed: {e.args[-1].desc}")

    @on(Button.Pressed, selector="#pause")
    async def attribute_pause(self):
        try:
            proxy = await self.entry.get_proxy()
            table = self.query_one(DataTable)
            key = table.coordinate_to_cell_key((table.cursor_row, 0)).row_key
            await proxy.command_inout("AttributePause", key.value)
        except tango.DevFailed as e:
            self.app.notify(f"Failed: {e.args[-1].desc}")

    @on(Button.Pressed, selector="#go-to")
    async def go_to(self, message):
        table = self.query_one(DataTable)
        full_attr, *_ = table.get_row_at(table.cursor_row)
        attr = await get_short_name(full_attr)
        try:
            entry = await get_attribute_entry(attr)
            self.post_message(NavigateTo(entry))
        except KeyError as e:
            self.app.notify(f"Could not go to attribute {attr}: {e}")
