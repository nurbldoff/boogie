import time
from typing import Generic

from rich.text import Text
import tango
from textual import work
from textual.widgets import DataTable

from boogie.messages import NavigateTo
from boogie.widgets.modal import ConfirmModal, GetValueModal
from boogie.widgets.entry import EntryType
from ..device import TangoDeviceProperty, TangoDeviceProperties
from ..classes import TangoDeviceClassProperties
from ..properties import TangoObjectProperties
from .details_list import TangoDetailsList
from .property import EditPropertyModal


INTERNAL_PROPERTY_DESCRIPTIONS = tango.utils.CaselessDict(
    {
        "description": "Description string for the device",
        "polled_attr": "Attribute polling config (name, period, ...)",
        "polled_cmd": "Command polling config (name, period, ...)",
        "logging_level": "Persistent logging level",
        "logging_target": "Persistent logging target",
        "__SubDevices": "Devices being contacted by this device",
        # ...?
    }
)


class TangoPropertyDetailsList(TangoDetailsList, Generic[EntryType]):
    BINDINGS = [
        ("n", "create", "New"),
        ("r", "rename", "Rename"),
        ("delete", "delete", "Delete"),
    ]

    COLUMNS = "Property", "Description"

    async def children_changed(
        self,
        entries: list[TangoDeviceProperty],
        pattern: str,
    ):
        await super().children_changed(entries, pattern)
        self._update_rows(entries)

    @work(exclusive=True)
    async def _update_rows(self, entries):
        "Asynchronously try to get some extra info about the properties"
        t0 = time.time()
        try:
            descriptions, defaults = await self.entry.get_info()
        except tango.DevFailed:
            # Device isn't running, no info available
            return
        table = self.query_one(DataTable)
        with self.app.batch_update():
            for entry in entries:
                desc = descriptions.get(entry.name, "")
                if desc:
                    table.update_cell(entry.name, "Description", desc, update_width=True)

    def get_description(self, name):
        if desc := INTERNAL_PROPERTY_DESCRIPTIONS.get(name):
            return desc
        return ""

    def get_style(self, entry: TangoDeviceProperty):
        if entry.name in INTERNAL_PROPERTY_DESCRIPTIONS:
            return ""
        if entry.deleted:
            return "bold red"
        return "bold"

    async def get_row(self, entry: TangoDeviceProperty):
        return (
            Text(entry.name, style=self.get_style(entry)),
            self.get_description(entry.name),
            # self.property_defaults.get(entry.name, ""),
        ), {"key": entry.name}

    async def action_rename(self) -> None:
        data_table = self.query_one(DataTable)
        entry = self.entries[data_table.cursor_row]
        assert entry

        async def maybe_rename(new_name: str | None) -> None:
            if new_name:
                await entry.rename(new_name)
                await self._load_children(self.entry)
                assert self.entry
                new_entry = await self.entry.get_child(new_name)
                self.post_message(NavigateTo(new_entry))
                self.notify(
                    f"Property '{entry.name}' renamed to '{new_name}'",
                    severity="warning",
                )

        question = f"Rename property '{str(entry)}'?"
        self.app.push_screen(GetValueModal(question, initial=str(entry)), maybe_rename)

    async def action_delete(self) -> None:
        data_table = self.query_one(DataTable)
        entry = self.entries[data_table.cursor_row]

        async def maybe_delete(delete: bool | None) -> None:
            if delete:
                assert isinstance(entry, TangoDeviceProperty)
                try:
                    await self.app.run_entry_method(entry, entry.delete)
                    # await self._load_children(self.entry)
                    assert self.entry
                    self.post_message(NavigateTo(self.entry))
                    # self.post_message(Reload())
                    self.notify(f"Property '{entry.name}' removed", severity="warning")
                    await self._load_children(self.entry)
                except tango.DevFailed as e:
                    self.notify(f"Error: {e.args[-1].desc}")

        question = f"Really want to delete property\n'{str(entry)}'?"
        self.app.push_screen(ConfirmModal(question), maybe_delete)

    async def action_create(self) -> None:
        async def maybe_create(result) -> None:
            if result:
                name, value = result
                if name:
                    if value:
                        try:
                            assert self.entry
                            await self.app.run_entry_method(
                                self.entry, self.entry.put, name=name, value=value
                            )
                            self.notify(f"Created new property '{name}'")
                            await self._load_children(self.entry)
                            new_entry = await self.entry.get_child(name)
                            self.post_message(NavigateTo(new_entry))
                        except tango.DevFailed as e:
                            self.notify(f"Failed to create property: {e}")
                    else:
                        self.notify("Can't create empty property.")
            else:
                self.notify("Property not created!")

        # assert isinstance(self.entry, TangoDeviceProperties)
        assert self.entry
        self.app.push_screen(EditPropertyModal(self.entry), maybe_create)


class TangoDevicePropertyDetailsList(TangoPropertyDetailsList[TangoDeviceProperties]):
    pass


class TangoDeviceClassPropertyList(
    TangoPropertyDetailsList[TangoDeviceClassProperties]
):
    pass


class TangoObjectPropertyList(TangoPropertyDetailsList[TangoObjectProperties]):
    pass
