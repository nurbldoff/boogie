import asyncio
from collections import deque
from typing import Literal

import tango
from textual.widgets import (
    DataTable,
    TabbedContent,
    TabPane,
    SelectionList,
    Input,
    Button,
    Label,
)
from textual.widgets.selection_list import Selection
from textual.screen import ModalScreen
from textual.containers import Grid, Vertical

from .details import TangoDetails, TangoEntryWidget
from ..device import (
    TangoDevicePolling,
)
from boogie.widgets.entry import EntryWidget
from boogie.widgets.misc import LazyTabPane


# from textual_plotext import PlotextPlot
PlotextPlot = None


class DevicePolling(TangoDetails[TangoDevicePolling], can_focus=False):
    HELP = """
    Configure polling for a device. For more information
    about polling see the Tango documentation.
    """

    def compose(self):
        self.border_title = "Polling"
        self.classes = "header-border"
        with TabbedContent() as tc:
            tc.can_focus = False
            with LazyTabPane("Attribute", id="attribute"):
                yield DeviceAttributePolling()
            with LazyTabPane("Command", id="command"):
                yield DeviceCommandPolling()
        yield self.error_widget()

    # def on_mount(self):
    #     self.query_one("Tabs").can_focus = False

    async def entry_changed(self, entry):
        self.query_one("#attribute").set_entry(await entry.get_child("attributes"))
        self.query_one("#command").set_entry(await entry.get_child("commands"))


class DevicePollingBase(TangoEntryWidget, can_focus=False):
    DEFAULT_CSS = """
    DevicePollingBase {
        layout: grid;
        grid-size: 2 1;
        grid-columns: 1fr auto;
        Vertical {
            hatch: right $background-lighten-2;
        }
        Button {
            min-width: 10;
        }
    }
    """

    COLUMNS = "Name", r"Period \[ms]"

    POLLING_TYPE: Literal["attribute", "command"]

    BINDINGS = [
        ("delete", "stop_polling", "Stop"),
        ("a", "add_polling", "Add"),
    ]

    def compose(self):
        self.border_title = f"{self.POLLING_TYPE.capitalize()}"
        data_table = DataTable()
        data_table.add_columns(*self.COLUMNS)
        data_table.cursor_type = "row"
        data_table.zebra_stripes = True
        yield data_table
        with Vertical():
            yield Button("Add", id="add", variant="primary")
            yield Button("Stop", id="stop", variant="error")

    async def entry_changed(self, entry):
        data_table = self.query_one(DataTable, expect_type=DataTable)
        data_table.clear()
        try:
            polled = await entry.get_status()
        except tango.DevFailed as e:
            self.post_message(self.Error(e))
            return
        for item in sorted(polled, key=lambda item: item["name"]):
            name = item["name"]
            period = item["period"]
            data_table.add_row(name, period, key=name)

    def on_data_table_row_selected(self, message: DataTable.RowSelected) -> None:
        print(message)

    async def action_stop_polling(self):
        data_table = self.query_one(DataTable)
        name, _ = data_table.get_row_at(data_table.cursor_row)
        try:
            await self.app.run_entry_method(self.entry, self.entry.stop_poll, name=name)
        except tango.DevFailed:
            self.app.notify("Failed to stop polling", severity="error")
        self.reload()

    async def action_add_polling(self):
        async def add_polling(value):
            if value:
                items, period = value
                reload = False
                for item in items:
                    if old_period := lookup.get(item):
                        # No point in changing polling period if it's the same
                        if period == old_period:
                            continue
                    # TODO how to store this in history?
                    try:
                        poll_method = getattr(proxy, f"poll_{self.POLLING_TYPE}")
                        poll_method(item, period)
                    except tango.DevFailed as e:
                        # TODO notify about this
                        pass
                    reload = True
                if reload:
                    self.reload()
                if items:
                    self.app.notify(
                        f"Started polling {len(items)} {self.POLLING_TYPE}s at {period} ms."
                    )

        proxy = await self.entry.parent.get_proxy()
        items = await self.entry.get_pollable()
        polled = await self.entry.get_status()
        lookup = {item["name"]: item["period"] for item in polled}
        items = [(name, lookup.get(name)) for name in items]
        self.app.push_screen(
            AddPollingModal(f"Add {self.POLLING_TYPE} polling", items), add_polling
        )

    async def on_button_pressed(self, event: Button.Pressed) -> None:
        await self.run_action(f"{event.button.id}_polling()")


class DeviceAttributePolling(DevicePollingBase):
    POLLING_TYPE = "attribute"


class DeviceCommandPolling(DevicePollingBase):
    POLLING_TYPE = "command"


class AddPollingModal(ModalScreen[tuple[list[str], int]]):
    DEFAULT_CSS = """
    AddPollingModal {
        align: center middle;
        .frame {
            width: 75%;
            height: 75%;
            border: panel $secondary;
            layout: grid;
            grid-size: 2 4;
            grid-rows: auto 1fr auto auto;
            Label, Input, SelectionList {
                column-span: 2;
            }
            Button {
                width: 1fr;
            }
        }
    }
    """

    def __init__(self, title: str, items: list[str], *args, **kwargs):
        self._title = title
        self._items = items
        super().__init__(*args, **kwargs)

    def compose(self):
        with Grid(classes="frame") as g:
            g.border_title = self._title
            yield Label("Select which to be added/changed")
            items = [
                Selection(item[0], item[0])
                if not item[1]
                else Selection(f"[b]{item[0]} [{item[1]}]", item[0])
                for item in self._items
            ]
            yield SelectionList[str](*items)
            yield Input("3000", type="number")
            yield Button("Add", id="add", variant="primary")
            yield Button("Cancel", id="cancel", variant="error")

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "add":
            values = self.query_one(SelectionList).selected
            period = int(self.query_one(Input).value)
            self.dismiss((values, period))
        else:
            self.dismiss()


class _DevicePolling(TangoDetails):
    DEFAULT_CSS = """
    DevicePolling {
    }
    DevicePolling #plot {
        height: 1fr;
    }
    """

    def compose(self):
        self.border_title = "Polling"
        # yield PlotextPlot()
        yield DevicePollingSubDetails()

    async def get_poll_deltas(self, entry, attribute, last_time=0, history_size=10):
        "Return any new polling deltas received"
        hist = await entry.get_attribute_history(attribute, history_size)
        times = [h.time.totime() for h in hist if h.time.totime() >= last_time]
        if not times:
            return [], last_time
        deltas = [round((t2 - t1) * 1000) for t1, t2 in zip(times, times[1:])]
        return deltas, max(times)
        # return [1000 + gauss(0, 10) for _ in range(3)], max(times)

    async def entry_changed(self, entry):
        self.query_one(DevicePollingSubDetails).set_entry(entry)
        p = self.query_one(PlotextPlot)
        period_deltas = {}
        p.plt.title("Polling deltas [ms]")
        p.plt.yticks([])
        p.tooltip = (
            "This histogram shows the distribution of polling deltas, "
            + " i.e. the time between consecutive pollings. Ideally,"
            + " the deltas should be very close to their respective"
            + " polling periods. If that is not the case, it indicates"
            + " slow operations, or overload on polling threads."
        )
        t0s = {}
        while True:
            try:
                polling_status = await entry.get_polling_status()
                period_attrs = polling_status.get("attribute", {})
                for period, attrs in period_attrs.items():
                    name = attrs[0]["name"]
                    deltas, t0s[name] = await self.get_poll_deltas(
                        entry,
                        name,
                        t0s.get(name, 0),
                        history_size=attrs[0]["buffer_depth"],
                    )
                    period_deltas.setdefault(period, deque(maxlen=1000)).extend(deltas)
                    # for attr in attrs:
                    #     period_deltas.setdefault(period, []).append(attr["reading_time"])
                p.plt.clear_data()
                for period, deltas in period_deltas.items():
                    if deltas:
                        bins = 20  # min(len(deltas), self.size.width - 2)
                        p.plt.hist(deltas, bins, label=f"{period}")
                p.refresh()
                self._error = None
            except tango.DevFailed as e:
                self._error = e
                await asyncio.sleep(10)
                polling_status = None

            await asyncio.sleep(1)


class DevicePollingSubDetails(EntryWidget):
    DEFAULT_CSS = """
    DevicePollingSubDetails {
        height: 1fr;
    }
    DevicePollingSubDetails TabbedContent {
        height: 1fr;
    }

    DevicePollingSubDetails ContentSwitcher {
        height: 1fr;
    }

    DevicePollingSubDetails TabPane {
        height: 1fr;
        width: 1fr;
        overflow: auto;
    }
    """

    def compose(self):
        with TabbedContent():
            with TabPane("Attributes"):
                yield DeviceAttributePolling()
            with TabPane("Commands"):
                yield DeviceCommandPolling()

    # @on(TabbedContent.TabActivated)
    # async def on_tab_activated(self, message):
    #     entry = self.subpath[0]
    #     child = await entry.get_child(message.tab_id)
    #     self.post_message(NavigateTo(child))

    async def entry_changed(self, entry):
        for w in self.query(EntryWidget):
            w.subpath = self.subpath
