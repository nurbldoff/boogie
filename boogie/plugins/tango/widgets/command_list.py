from textwrap import dedent

from rich.text import Text

from .details_list import TangoDetailsList
from ..device import TangoDeviceCommands


def strip_multiline(s):
    return dedent("\n".join(line.rstrip() for line in s.splitlines()))


class TangoDeviceCommandListDetails(TangoDetailsList[TangoDeviceCommands]):
    COLUMNS = ["Command", "In type", "Out type", "In desc", "Out desc"]

    BINDINGS = TangoDetailsList.BINDINGS + [
        ("m", "monitor", "Monitor"),
    ]

    async def get_row(self, entry):
        info = entry.info
        in_type_desc = str(info.in_type_desc)
        out_type_desc = str(info.out_type_desc)
        height = max(in_type_desc.count("\n") + 1, out_type_desc.count("\n") + 1, 1)
        return (
            Text(info.cmd_name, style="bold"),
            info.in_type.name[3:],
            info.out_type.name[3:],
            (strip_multiline(in_type_desc) if in_type_desc != "Uninitialised" else ""),
            (
                strip_multiline(out_type_desc)
                if out_type_desc != "Uninitialised"
                else ""
            ),
        ), dict(height=height)

    async def action_monitor(self):
        entry = self.highlighted_entry
        await self.app.add_to_monitor(entry)
