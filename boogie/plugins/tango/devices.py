from dataclasses import dataclass

# from boogie import EntryTree
from boogie.cache import cached
from .utils import get_db
from .entry import TangoEntry
from .device import TangoDevice


@dataclass(frozen=True, repr=False, eq=False)
class TangoDevices(TangoEntry):
    parent: None = None
    name: str = "Device"

    @cached
    async def _get_domains(self):
        db = await get_db()
        return [d.lower() for d in await db.get_device_domain("*")]

    async def get_children(self):
        domains = await self._get_domains()
        return [TangoDeviceDomain(self, domain) for domain in domains]

    async def get_child(self, domain):
        domains = await self._get_domains()
        if domain.lower() in domains:
            return TangoDeviceDomain(self, domain.lower())
        raise KeyError(f"No device domain {domain}")


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceDomain(TangoEntry):
    parent: TangoDevices

    @cached
    async def _get_families(self):
        db = await get_db()
        families = await db.get_device_family(f"{self.name}/*")
        return [f.lower() for f in families]

    async def get_children(self):
        families = await self._get_families()
        return [TangoDeviceFamily(self, family) for family in families]

    async def get_child(self, family):
        families = await self._get_families()
        if family.lower() in families:
            return TangoDeviceFamily(self, family.lower())
        raise KeyError(f"No device family {family} of {self.name}/")


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceFamily(TangoEntry):
    parent: TangoDeviceDomain

    @property
    def domain(self):
        return self.parent.name

    @cached
    async def _get_members(self):
        db = await get_db()
        prefix = f"{self.domain}/{self.name}/"
        members = await db.get_device_member(prefix + "*")
        return [m.lower() for m in members]

    async def get_children(self):
        members = await self._get_members()
        return [TangoDeviceMember(self, member) for member in members]

    async def get_child(self, member):
        members = await self._get_members()
        if member.lower() in members:
            return TangoDeviceMember(self, member.lower())
        raise KeyError(f"No device member {member} of {self.domain}/{self.name}/")


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceMember(TangoDevice):
    parent: TangoDeviceFamily  # type: ignore  # TODO fixme

    @cached
    async def get_device(self):
        return "/".join(e.name for e in self.path[-3:])
