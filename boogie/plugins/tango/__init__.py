import tango

from . import widgets  # noqa  Important for widgets to be registered

from .servers import TangoServers
from .classes import TangoClasses
from .devices import TangoDevices
from .aliases import TangoDeviceAliases
from .properties import TangoObjects
from .hosts import TangoHosts

from .search import search_devices, search_aliases, search_servers, search_properties
from .commands import TangoSearch


INDEX = 0  # Used for ordering the tabs in the UI


# The title is used in the app header, but also to namespace plugins
# in the history. All plugins with the same title share the same history.
# So, here we return the current control system URL
async def get_title() -> str:
    # db = await get_db()
    # return f"{await db.get_db_host()}:{await db.get_db_port()}"
    return tango.ApiUtil.get_env_var("TANGO_HOST")


# These will each become a tab in the main UI
BROWSERS: list[dict] = [
    {
        "root": TangoServers,
        "search": search_servers,
        "get_title": get_title,
    },
    {
        "root": TangoDevices,
        "search": search_devices,
        "get_title": get_title,
    },
    {
        "root": TangoClasses,
        "get_title": get_title,
    },
    {
        "root": TangoDeviceAliases,
        "search": search_aliases,
        "get_title": get_title,
    },
    {
        "root": TangoObjects,
        "search": search_properties,
        "get_title": get_title,
    },
    {
        "root": TangoHosts,
        "get_title": get_title,
    },
]


EXTRA_CSS = [
    "state.tcss",
    "device.tcss",
]


COMMAND_PROVIDERS = {TangoSearch}


SEARCH_PROVIDERS = {
    "Server": search_servers,
    "Device": search_devices,
    "Alias": search_aliases,
    "Property": search_properties,
}


SCRIPTS = {"trend": widgets.trend}
