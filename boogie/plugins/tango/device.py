from __future__ import annotations
from dataclasses import field
import asyncio
import time

from dataclasses import dataclass
import tango
from tango.asyncio import DeviceProxy
from typing import Any, Sequence

from boogie.cache import cached
from boogie.command import command
from .entry import TangoEntry
from .utils import get_db, get_device_proxy, get_full_name
from .logging import device_logs
from .listener import get_listener


# TODO are these correct?
SCALAR_TYPES_FROM_ARRAYS = {
    tango.CmdArgType.DevVarCharArray: tango.CmdArgType.DevUChar,
    tango.CmdArgType.DevVarShortArray: tango.CmdArgType.DevShort,
    tango.CmdArgType.DevVarLongArray: tango.CmdArgType.DevLong,
    tango.CmdArgType.DevVarFloatArray: tango.CmdArgType.DevFloat,
    tango.CmdArgType.DevVarDoubleArray: tango.CmdArgType.DevDouble,
    tango.CmdArgType.DevVarUShortArray: tango.CmdArgType.DevUShort,
    tango.CmdArgType.DevVarULongArray: tango.CmdArgType.DevULong,
    tango.CmdArgType.DevVarStringArray: tango.CmdArgType.DevString,
    # TODO No idea what to do about these types
    # tango.CmdArgType.DevVarLongStringArray: tango.CmdArgType.DevLong,
    # tango.CmdArgType.DevVarDoubleStringArray: tango.CmdArgType.DevDouble,
    tango.CmdArgType.DevVarBooleanArray: tango.CmdArgType.DevBoolean,
    tango.CmdArgType.DevVarLong64Array: tango.CmdArgType.DevLong64,
    tango.CmdArgType.DevVarULong64Array: tango.CmdArgType.DevULong64,
    tango.CmdArgType.DevVarStateArray: tango.CmdArgType.DevState,
}


@dataclass(frozen=True, repr=False, eq=False)
class TangoDevice(TangoEntry):
    # Note: not setting the parent type here, as it's used in several
    # different "trees" and overriding the type doesn't work well.
    # Anyway, in this case there's not much useful info on the parent
    # so I don't think we need to use it.

    async def get_device(self) -> str:
        return self.name

    async def get_info(self) -> tango.DeviceInfo:
        db = await get_db()
        device = await self.get_device()
        return await db.get_device_info(device)

    async def get_import_info(self) -> None:
        db = await get_db()
        device = await self.get_device()
        _, info = await db.command_inout("DbImportDevice", device)
        return info

    async def get_dserver(self) -> str:
        info = await self.get_info()
        return f"dserver/{info.ds_full_name}"

    async def get_proxy(self) -> DeviceProxy:
        return await get_device_proxy(await self.get_device())

    async def get_db_info(self) -> tango.DbDevFullInfo:
        return await self.get_info()

    async def get_class(self) -> str:
        info = await self.get_db_info()
        return info.class_name

    async def get_alias(self) -> str | None:
        db = await get_db()
        try:
            device = await self.get_device()
            return await db.get_alias_from_device(device)
        except tango.DevFailed:
            return None

    async def put_alias(self, value: str) -> None:
        db = await get_db()
        device = await self.get_device()
        await db.put_device_alias(device, value)

    async def delete_alias(self) -> None:
        db = await get_db()
        try:
            alias = await self.get_alias()
            if alias is not None:
                return await db.delete_device_alias(alias)
        except tango.DevFailed:
            return None

    async def get_description(self) -> str:
        proxy = await self.get_proxy()
        # A hack; seems proxy.description() doesn't release the GIL (or something)
        # causing the call to block even if it's running in a thread pool, and taking
        # potentially several seconds if the device isn't running.
        # Adding a ping here to "fail early" instead.
        await proxy.ping()
        loop = asyncio.get_event_loop()
        return await loop.run_in_executor(None, proxy.description)

    async def put_description(self, value: str) -> None:
        db = await get_db()
        device = await self.get_device()
        await db.put_device_property(device, {"description": [value]})

    async def get_title(self) -> str:
        device = await self.get_device()
        domain, family, member = device.split("/")
        alias = await self.get_alias()
        main_title = (
            f"[b]{domain}[gray50]/[/gray50]{family}[gray50]/[/gray50]{member}[/b]"
        )
        if alias:
            return f"{main_title} ({alias})"
        return main_title

    @cached
    async def get_subdevices(self) -> set[str]:
        dserver = await self.get_dserver()
        dserver_proxy = await get_device_proxy(dserver)
        result = await dserver_proxy.command_inout("QuerySubDevice")
        subdevs = set()
        n_siblings = len(await dserver_proxy.command_inout("QueryDevice"))

        lowername = self.name.lower()
        is_dserver = lowername.startswith("dserver/")
        for item in result:
            if " " in item:
                dev, subdev = item.split(" ")
                if dev.lower() == lowername or is_dserver:
                    subdevs.add(subdev)
            elif is_dserver or n_siblings == 1:
                # No space means Tango does not know which device contains the subdevice
                # But if the server only contains one device we can assume who it belongs
                # to anyway.
                subdevs.add(item)
        return subdevs

    # TODO kind of pointless to wrap all these things...

    async def ping(self) -> int:  # Microseconds
        proxy = await self.get_proxy()
        return await proxy.ping()

    async def init(self) -> None:
        proxy = await self.get_proxy()
        await proxy.command_inout("Init")

    async def restart(self) -> None:
        dserver = await self.get_dserver()
        proxy = await get_device_proxy(dserver)
        await proxy.command_inout("DevRestart", await self.get_device())

    async def lock(self) -> None:
        proxy = await self.get_proxy()
        proxy.lock()

    async def unlock(self, force=False) -> None:
        proxy = await self.get_proxy()
        proxy.unlock(force)

    async def get_lock_info(self) -> tango.LockerInfo:
        proxy = await self.get_proxy()
        info = tango.LockerInfo()
        proxy.get_locker(info)
        return info

    @command(
        confirm="Really delete device {entry.name}?",
        notify="Done deleting {entry.name}!",
        reload=True,
    )
    async def delete(self) -> None:
        "Remove the device from the database"
        db = await get_db()
        db.delete_device(await self.get_device())

    # TODO
    # - rename (also update any alias!)

    def refresh(self, *args, **kwargs) -> None:
        pass

    async def get_children(self) -> list[TangoEntry]:
        return [
            TangoDeviceAttributes(self),
            TangoDeviceCommands(self),
            TangoDeviceProperties(self),
            TangoDeviceAttributeProperties(self),
            TangoDevicePolling(self),
            TangoDeviceLogging(self),
            TangoDeviceSubdevices(self),
        ]

    async def get_child(self, name) -> TangoEntry:
        lowername = name.lower()
        if lowername == "attributes":
            return TangoDeviceAttributes(self)
        if lowername == "properties":
            return TangoDeviceProperties(self)
        if lowername == "attr props":
            return TangoDeviceAttributeProperties(self)
        if lowername == "commands":
            return TangoDeviceCommands(self)
        if lowername == "polling":
            return TangoDevicePolling(self)
        if lowername == "logging":
            return TangoDeviceLogging(self)
        if lowername == "subdevices":
            return TangoDeviceSubdevices(self)
        raise KeyError

    @command(
        name="Some test",
        confirm="Really delete device {entry.name}?",
        notify="Done!",
        reload=True,
    )
    async def test(self):
        "This is a test device command"
        print("Testing commands")


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceAttributes(TangoEntry):
    parent: TangoDevice
    name: str = "Attributes"

    leaf_node = True

    @property
    def device(self) -> str:
        return str(self.parent)

    async def get_children(self):
        proxy = await get_device_proxy(await self.parent.get_device())
        loop = asyncio.get_event_loop()

        # It seems attribute_list_query_ex does not release the GIL, because it
        # locks up everything, and if the device isn't running it can take seconds.
        # The ping however is async, and if it fails it will prevent
        # the slow query. Remove it whenever PyTango's async support improves!
        await proxy.ping()
        attributes = await loop.run_in_executor(None, proxy.attribute_list_query_ex)
        return [TangoDeviceAttribute(self, attr.name, info=attr) for attr in attributes]

    async def get_child(self, name):
        try:
            proxy = await get_device_proxy(await self.parent.get_device())
            info = await proxy.get_attribute_config(name.lower())
            return TangoDeviceAttribute(self, name, info=info)
        except tango.DevFailed:
            raise KeyError(f"Could not get info about attribute {name}")
        except KeyError:
            # TODO when does this happen?
            raise KeyError(f"No attribute found called {name}")


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceAttribute(TangoEntry):
    parent: TangoDeviceAttributes
    name: str
    info: tango.AttributeInfoEx = field(compare=False)  # Ignore when checking eq

    async def get_config(self) -> tango.AttributeInfoEx:
        proxy = await self.get_proxy()
        (config,) = await proxy.get_attribute_config_ex(self.name)
        return config

    async def set_config(self, config: tango.AttributeInfoEx) -> None:
        proxy = await self.get_proxy()
        loop = asyncio.get_event_loop()
        await loop.run_in_executor(None, proxy.set_attribute_config, config)

    async def get_children(self):
        return [
            TangoEntry(self, "value"),
            TangoEntry(self, "info"),
            TangoEntry(self, "config"),
            TangoEntry(self, "alarms"),
            TangoEntry(self, "events"),
            # TangoDbEntry(self, "properties"),
        ]

    async def get_child(self, name):
        lowername = name.lower()
        if lowername == "info":
            return TangoEntry(self, "info")
        elif lowername == "config":
            return TangoEntry(self, "config")
        elif lowername == "value":
            return TangoEntry(self, "value")
        elif lowername == "polling":
            return TangoEntry(self, "polling")
        elif lowername == "events":
            return TangoEntry(self, "events")
        elif lowername == "alarms":
            return TangoEntry(self, "alarms")
        elif lowername == "events":
            return TangoEntry(self, "events")
        # elif lowername == "properties":
        #     return TangoDbEntry(self, "properties")
        raise NotImplementedError()

    # def __str__(self):
    #     return self.name

    # def __hash__(self):
    #     return hash(self.name)

    async def get_device(self) -> str:
        return await self.parent.parent.get_device()

    async def get_proxy(self) -> DeviceProxy:
        return await get_device_proxy(await self.get_device())

    async def read(self) -> tango.DeviceAttribute:
        proxy = await self.get_proxy()
        return await proxy.read_attribute(self.name)

    async def write(self, value) -> None:
        proxy = await self.get_proxy()
        await proxy.write_attribute(self.name, value)

    async def poll_period(self) -> int:
        proxy = await self.get_proxy()
        loop = asyncio.get_event_loop()
        return await loop.run_in_executor(
            None, proxy.get_attribute_poll_period, self.name
        )

    async def polling_status(self) -> list[str]:
        proxy = await self.get_proxy()
        statuses = proxy.polling_status() or []
        device = (await self.get_device()).lower()
        for status in statuses:
            lines = status.splitlines()
            name = lines[0].split("=")[-1].strip().lower()
            if name.lower() == device:
                return lines
        return []

    async def get_full_name(self) -> str:
        name = await self.get_name()
        return get_full_name(name)

    async def get_name(self) -> str:
        device = await self.get_device()
        return f"{device}/{self.name}"

    def parse_string(self, value: str) -> object:
        try:
            numpy_type = tango.utils.FROM_TANGO_TO_NUMPY_TYPE[self.info.data_type]
            return numpy_type(value)
        except ValueError as e:
            raise ValueError(f"Error parsing value: {e}")
        except KeyError as e:
            if self.info.data_type == tango.DevEnum:
                # Handle enums as plain int for now
                return int(value)
            raise ValueError(f"Unknown type {str(e)}!?")

    async def listen(self):
        listener = get_listener()
        full_name = await self.get_full_name()
        return listener.listen([full_name])


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceAttributeInfo(TangoEntry):
    name: str = "Info"
    parent: TangoDeviceAttribute


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceAttributeValue(TangoEntry):
    name: str = "Value"
    parent: TangoDeviceAttribute


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceCommands(TangoEntry):
    name: str = "Commands"
    parent: TangoDevice

    leaf_node = True

    async def get_device(self):
        return await self.parent.get_device()

    async def get_proxy(self):
        return await get_device_proxy(await self.get_device())

    async def get_children(self):
        proxy = await self.get_proxy()
        loop = asyncio.get_event_loop()
        # It seems command_list_query does not release the GIL or something
        # because it locks up everything, and if the device isn't running it can
        # take seconds. The ping however is async, and if it fails it will prevent
        # the slow query. Hack!
        await proxy.ping()
        commands = await loop.run_in_executor(None, proxy.command_list_query)
        return [TangoDeviceCommand(self, cmd.cmd_name, cmd) for cmd in commands]

    async def get_child(self, cmd_name):
        print("get child", cmd_name)
        proxy = await self.get_proxy()
        try:
            cmd_info = proxy.command_query(cmd_name)
            return TangoDeviceCommand(self, cmd_name, cmd_info)
        except tango.DevFailed:
            raise KeyError


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceCommand(TangoEntry):
    parent: TangoDeviceCommands
    info: tango.CommandInfo

    async def get_device(self):
        return await self.parent.parent.get_device()

    async def get_children(self):
        return [
            TangoEntry(self, "run"),
            TangoEntry(self, "polling"),
        ]

    async def get_child(self, name):
        lowername = name.lower()
        if lowername == "run":
            return TangoEntry(self, "run")
        elif lowername == "polling":
            return TangoEntry(self, "polling")
        raise NotImplementedError()

    @property
    def takes_arguments(self):
        return self.info.in_type != tango.CmdArgType.DevVoid

    @property
    def returns_values(self):
        return self.info.out_type != tango.CmdArgType.DevVoid

    async def get_proxy(self):
        return await get_device_proxy(await self.get_device())

    async def run(self, args=None):
        proxy = await self.get_proxy()
        return await proxy.command_inout(self.name, args)

    def validate_argument(self, value: str):
        if self.info.in_type in SCALAR_TYPES_FROM_ARRAYS:
            scalar_type = SCALAR_TYPES_FROM_ARRAYS[self.info.in_type]
            numpy_type = tango.utils.FROM_TANGO_TO_NUMPY_TYPE[scalar_type]
        else:
            numpy_type = tango.utils.FROM_TANGO_TO_NUMPY_TYPE[self.info.in_type]
        try:
            numpy_type(value)
        except ValueError as e:
            raise ValueError(f"Error parsing value: {e}")

    def convert_arguments(self, value: str | list[str]):
        """
        Convert a string or list of strings into the expected argument type.
        Raises ValueError if this can't be done.
        """
        if self.info.in_type in SCALAR_TYPES_FROM_ARRAYS:
            if isinstance(value, str):
                raise ValueError("Expected list of strings for array type")
            scalar_type = SCALAR_TYPES_FROM_ARRAYS[self.info.in_type]
            numpy_type = tango.utils.FROM_TANGO_TO_NUMPY_TYPE[scalar_type]
            values = []
            for i, line in enumerate(value):
                try:
                    values.append(numpy_type(line))
                except ValueError as e:
                    raise ValueError(f"Error parsing argument line {i}: {e}")
            return values
        else:
            # TODO DevVarDoubleStringArray, DevVarLongStringArray
            numpy_type = tango.utils.FROM_TANGO_TO_NUMPY_TYPE[self.info.in_type]
            return numpy_type(value)


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceProperties(TangoEntry):
    parent: TangoDevice
    name: str = "Properties"

    leaf_node = True

    async def get_device(self) -> str:
        return await self.parent.get_device()

    async def get_dserver(self) -> str:
        return await self.parent.get_dserver()

    async def get_children(self):
        db = await get_db()
        device = await self.get_device()
        properties = await db.get_device_property_list(device, "*")
        return [TangoDeviceProperty(self, prop) for prop in properties]

    async def get_child(self, name: str):
        db = await get_db()
        device = await self.get_device()
        prop = await db.get_device_property(device, name)
        if prop[name]:
            return TangoDeviceProperty(self, name)
        raise KeyError(f"No property named {name} on device {device}")

    async def put(self, name: str, value: list[str]) -> None:
        db = await get_db()
        device = await self.get_device()
        await db.put_device_property(device, {name: value})

    async def get_info(self) -> tuple[dict[str, str], dict[str, str]]:
        t0 = time.time()
        dserver_proxy = await get_device_proxy(await self.parent.get_dserver())
        print("got proxy", time.time() - t0)
        device_info = await self.parent.get_db_info()
        print("got info", time.time() - t0)
        data = await dserver_proxy.command_inout(
            "QueryWizardDevProperty", device_info.class_name
        )
        print("got wizard", time.time() - t0)
        descriptions = {name: desc for name, desc in zip(data[::3], data[1::3])}
        defaults = {name: default for name, default in zip(data[::3], data[2::3])}
        return descriptions, defaults

    def __repr__(self):
        return f"Properties(device={self.parent.name})"


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceProperty(TangoEntry):
    parent: TangoDeviceProperties
    name: str
    deleted: bool = False

    async def get_device(self) -> str:
        return await self.parent.parent.get_device()

    async def get_value(self) -> list[str]:
        db = await get_db()
        device = await self.get_device()
        result = await db.get_device_property(device, self.name)
        return result[self.name]

    async def get_history(self) -> list[tango.DbHistory]:
        db = await get_db()
        device = await self.get_device()
        result = await db.get_device_property_history(device, self.name)
        return list(result)

    async def set_value(self, value: str | Sequence[str]) -> None:
        db = await get_db()
        device = await self.get_device()
        await db.put_device_property(device, {self.name: value})

    async def rename(self, new_name: str) -> None:
        # There's no real way to rename a property; we just have to remove
        # it, and add a new one. This means we don't keep the history.
        db = await get_db()
        device = await self.get_device()
        value = await db.get_device_property(device, self.name)
        await db.delete_device_property(device, self.name)
        await db.put_device_property(device, {new_name: value[self.name]})

    async def delete(self) -> None:
        db = await get_db()
        device = await self.get_device()
        await db.delete_device_property(device, self.name)


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceAttributeProperties(TangoEntry):
    parent: TangoDevice
    name: str = "Attr props"

    async def get_device(self) -> str:
        return await self.parent.get_device()

    async def get_dserver(self) -> str:
        return await self.parent.get_dserver()

    async def get_info(self) -> tango.DeviceInfo:
        db = await get_db()
        device = await self.get_device()
        return await db.get_device_info(device)

    async def get_db_info(self) -> tango.DbDevFullInfo:
        return await self.get_info()

    async def get_children(self):
        db = await get_db()
        device = await self.get_device()
        attrs = tango.StdStringVector()
        await db.get_device_attribute_list(device, attrs)
        return [TangoDeviceAttributePropertiesAttribute(self, attr) for attr in attrs]

    async def get_child(self, name: str):
        db = await get_db()
        device = await self.get_device()
        prop = await db.get_device_attribute_property(device, name.lower())
        if name.lower() in prop:
            return TangoDeviceAttributePropertiesAttribute(self, name)
        raise KeyError(f"No property named {name} on device {device}")


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceAttributePropertiesAttribute(TangoDeviceProperties):
    # Yeah, great class name... hope nobody sees it!
    parent: TangoDeviceAttributeProperties  # type: ignore

    async def get_device(self) -> str:
        return await self.parent.parent.get_device()

    async def get_children(self):
        db = await get_db()
        device = await self.get_device()
        attr = await db.get_device_attribute_property(device, self.name)
        return [
            TangoDeviceAttributeProperty(self, prop.lower()) for prop in attr[self.name]
        ]

    async def get_child(self, name: str):
        db = await get_db()
        device = await self.get_device()
        properties = await db.get_device_attribute_property(device, self.name)
        if name.lower() in {prop.lower() for prop in properties}:
            return TangoDeviceAttributeProperty(self, name)
        raise KeyError(f"No property named {name} on device {device}")


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceAttributeProperty(TangoDeviceProperty):
    parent: TangoDeviceAttributePropertiesAttribute

    async def get_device(self) -> str:
        return await self.parent.parent.get_device()

    async def get_attribute_name(self) -> str:
        return self.parent.name

    async def get_value(self):
        db = await get_db()
        device = await self.get_device()
        attr = await self.get_attribute_name()
        result = await db.get_device_attribute_property(device, attr)
        return result[attr][self.name]

    async def get_history(self):
        db = await get_db()
        device = await self.get_device()
        attr = await self.get_attribute_name()
        result = await db.get_device_attribute_property_history(device, attr, self.name)
        return list(result)

    async def set_value(self, value: str | Sequence[str]) -> None:
        db = await get_db()
        device = await self.get_device()
        await db.put_device_property(device, {self.name: value})

    async def rename(self, new_name: str) -> None:
        # There's no real way to rename a property; we just have to remove
        # it, and add a new one. This means we don't keep the history.
        db = await get_db()
        device = await self.get_device()
        value = await db.get_device_property(device, self.name)
        await db.delete_device_property(device, self.name)
        await db.put_device_property(device, {new_name: value[self.name]})

    async def delete(self) -> None:
        db = await get_db()
        device = await self.get_device()
        attr = await self.get_attribute_name()
        await db.delete_device_attribute_property(device, {attr: [self.name]})


@dataclass(frozen=True, repr=False, eq=False)
class TangoDevicePolling(TangoEntry):
    name: str = "Polling"
    parent: TangoDevice

    leaf_node = True

    async def get_children(self):
        return [
            TangoDevicePolledAttributes(self),
            TangoDevicePolledCommands(self),
        ]

    async def get_child(self, name):
        if name.lower() == "attributes":
            return TangoDevicePolledAttributes(self)
        if name.lower() == "commands":
            return TangoDevicePolledCommands(self)

    async def get_proxy(self) -> DeviceProxy:
        return await self.parent.get_proxy()

    async def get_polling_status(self) -> dict[str, list[dict[str, Any]]]:
        proxy = await self.get_proxy()
        status_strings = proxy.polling_status() or []
        statuses = (self._parse_polling_status(s) for s in status_strings)
        statuses_by_type: dict[str, list[dict[str, Any]]] = {}
        for status in statuses:
            if not status:
                continue
            (statuses_by_type.setdefault(status["type"], []).append(status))
        return statuses_by_type

    def _parse_polling_status(self, status: str) -> dict[str, Any]:
        """
        Poling stats are returned in a "human readable" format that
        we must parse.

        Example:

            Polled attribute name = boolean_scalar
            Polling period (mS) = 2000
            Polling ring buffer depth = 10
            Time needed for the last attribute reading (mS) = 0.006
            Data not updated since 62 mS
            Delta between last records (in mS) = 2000, 1999, 2571, 3000

        But if polling was just enabled, there might be no statistics yet:

            Polled attribute name = short_scalar_ro
            Polling period (mS) = 3000
            Polling ring buffer depth = 10
            No data recorded yet

        TODO might be other variants... :(
        """
        lines = str(status).splitlines()
        try:
            data = dict(
                type=lines[0].split(" ", 2)[1],
                name=lines[0].split("=")[-1].strip(),
                period=int(lines[1].split("=")[-1]),
                buffer_depth=int(lines[2].split("=")[-1]),
            )
            if len(lines) > 4:
                data.update(
                    dict(
                        reading_time=float(lines[3].split("=")[-1]),
                        last_update=int(lines[4].rsplit(" ", 2)[-2]),
                        deltas=(
                            [int(t) for t in lines[5].rsplit("=")[-1].split(",")]
                            if len(lines) > 5
                            else []
                        ),
                    )
                )
            return data
        except (ValueError, IndexError) as e:
            print(f"Could not parse polling status {status}: {e}")
            return {}

    async def get_attribute_history(
        self, name: str, history_size=10
    ) -> list[tango.DbHistory]:
        proxy = await self.get_proxy()
        return await asyncio.to_thread(proxy.attribute_history, name, history_size)


@dataclass(frozen=True, repr=False, eq=False)
class TangoDevicePolledAttributes(TangoEntry):
    parent: TangoDevicePolling
    name: str = "Attributes"

    async def get_status(self) -> list:
        status = await self.parent.get_polling_status()
        return status.get("attribute", [])

    async def get_pollable(self) -> list[str]:
        proxy = await self.parent.get_proxy()
        return proxy.get_attribute_list()

    async def stop_poll(self, name: str):
        proxy = await self.parent.get_proxy()
        proxy.stop_poll_attribute(name)

    async def poll(self, name: str, period: int):
        proxy = await self.parent.get_proxy()
        await proxy.poll_attribute(name, period)


@dataclass(frozen=True, repr=False, eq=False)
class TangoDevicePolledCommands(TangoEntry):
    parent: TangoDevicePolling
    name: str = "Commands"

    async def get_status(self) -> list:
        status = await self.parent.get_polling_status()
        return status.get("command", [])

    async def get_pollable(self) -> list[str]:
        proxy = await self.parent.get_proxy()
        commands = proxy.command_list_query()
        return [
            command.cmd_name
            for command in commands
            if command.in_type == tango.CmdArgType.DevVoid
        ]

    async def stop_poll(self, name: str):
        proxy = await self.parent.get_proxy()
        proxy.stop_poll_command(name)

    async def poll(self, name: str, period: int):
        proxy = await self.parent.get_proxy()
        await proxy.poll_command(name, period)


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceLogging(TangoEntry):
    name: str = "Logging"
    parent: TangoDevice

    leaf_node = True

    def get_proxy(self) -> DeviceProxy:
        return self.parent.get_proxy()

    async def logs(self):
        # Experimental!!
        name = await self.parent.get_device()
        return device_logs([name], 5)


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceSubdevices(TangoEntry):
    parent: TangoDevice
    name: str = "Subdevices"

    leaf_node = True

    async def get_children(self):
        subdevs = await self.parent.get_subdevices()
        return [TangoDevice(self, subdev) for subdev in subdevs]

    async def get_child(self, name):
        subdevs = await self.parent.get_subdevices()
        if name.lower() in {sd.lower() for sd in subdevs}:
            return TangoDevice(self, name)
        raise KeyError(f"Subdevice not found: {name}")
