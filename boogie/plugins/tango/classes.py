from dataclasses import dataclass

import tango

from boogie.cache import cached
from .utils import get_db
from .entry import TangoEntry
from .device import TangoDevice, TangoDeviceProperty


@dataclass(frozen=True, repr=False, eq=False)
class TangoClasses(TangoEntry):
    parent: None = None
    name: str = "Class"

    async def get_children(self):
        db = await get_db()
        classes = await db.get_class_list("*")
        return [TangoDeviceClass(self, clss) for clss in classes]

    async def get_child(self, clss: str):
        db = await get_db()
        classes = await db.get_class_list(clss)
        if classes:
            return TangoDeviceClass(self, name=classes[0])
        raise KeyError(f"No class named {clss}")


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceClass(TangoEntry):
    parent: TangoClasses

    @property
    def class_name(self) -> str:
        return self.name

    async def get_children(self):
        return [
            TangoDeviceClassDevices(self),
            TangoDeviceClassProperties(self),
        ]

    async def get_child(self, name: str):
        if name.lower() == "devices":
            return TangoDeviceClassDevices(self)
        if name.lower() == "properties":
            return TangoDeviceClassProperties(self)
        raise KeyError()


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceClassDevices(TangoEntry):
    parent: TangoDeviceClass
    name: str = "Devices"

    @property
    def class_name(self):
        return self.parent.name

    @cached
    async def get_devices(self):
        db = await get_db()
        _, devices = await db.command_inout(
            "DbMySQLSelect",
            f"SELECT name FROM device WHERE class = '{self.class_name}' ORDER BY name",
        )
        return devices

    async def get_children(self):
        devices = await self.get_devices()
        return [TangoDevice(self, device) for device in devices]

    async def get_child(self, device: str):
        devices = await self.get_devices()
        if device.lower() in {d.lower() for d in devices}:
            return TangoDevice(self, device)
        raise KeyError(f"Device {device} not found")


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceClassProperties(TangoEntry):
    parent: TangoDeviceClass
    name: str = "Properties"

    leaf_node = True

    @property
    def class_name(self) -> str:
        return self.parent.name

    async def get_children(self):
        db = await get_db()
        props = await db.get_class_property_list(self.class_name)
        return [TangoDeviceClassProperty(self, prop) for prop in props]

    async def get_child(self, name: str):
        db = await get_db()
        try:
            props = await db.get_class_property_list(self.class_name)
            if name.lower() in {p.lower() for p in props}:
                return TangoDeviceClassProperty(self, name)
            raise KeyError()
        except IndexError:
            raise KeyError()

    async def get_info(self) -> tuple[dict, dict]:
        return {}, {}

    async def put(self, name: str, value: list[str]) -> None:
        db = await get_db()
        await db.put_class_property(self.class_name, {name: value})


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceClassProperty(TangoDeviceProperty):
    parent: TangoDeviceClassProperties  # type: ignore

    @property
    def class_name(self) -> str:
        return self.parent.class_name

    async def get_value(self) -> list[str]:
        db = await get_db()
        result = await db.get_class_property(self.class_name, self.name)
        return result[self.name]

    async def get_history(self) -> list[tango.DbHistory]:
        db = await get_db()
        result = await db.get_class_property_history(self.class_name, self.name)
        return list(result)

    async def set_value(self, value):
        db = await get_db()
        await db.put_class_property(self.class_name, {self.name: value})

    async def rename(self, new_name: str) -> None:
        # There's no real way to rename a property; we just have to remove
        # it, and add a new one. This means we don't keep the history.
        db = await get_db()
        value = await db.get_class_property(self.class_name, self.name)
        await db.delete_class_property(self.class_name, self.name)
        await db.put_class_property(self.class_name, {new_name: value[self.name]})

    async def delete(self) -> None:
        db = await get_db()
        await db.delete_class_property(self.class_name, self.name)
