"""
This is an abstraction over a Tango control system, to turn it into a
convenient tree-like structure. It defines a hierarchy of "entries"
that can be traversed easily. Also each entry has various helper methods
and properties that wrap up some of the thornier parts of the Tango API,
and handles things like caching.
"""

from __future__ import annotations
from dataclasses import dataclass
import logging
from typing import Sequence

import tango

from .utils import get_db
from .entry import TangoEntry
from .device import TangoDeviceProperty


logger = logging.getLogger("textual")


@dataclass(frozen=True, repr=False, eq=False)
class TangoObjects(TangoEntry):
    parent: None = None
    name: str = "Property"

    async def get_children(self):
        db = await get_db()
        objs = await db.get_object_list("*")
        return [TangoObjectProperties(self, o) for o in objs]

    async def get_child(self, name):
        db = await get_db()
        try:
            (obj,) = await db.get_object_list(name)
            return TangoObjectProperties(self, name)
        except tango.DevFailed:
            raise KeyError(f"No object called {name} was found")


@dataclass(frozen=True, repr=False, eq=False)
class TangoObjectProperties(TangoEntry):
    leaf_node = True

    async def get_children(self):
        db = await get_db()
        objs = await db.get_object_property_list(self.name, "*")
        return [TangoObjectProperty(self, o) for o in objs]

    async def get_child(self, name):
        db = await get_db()
        try:
            (obj,) = await db.get_object_property_list(self.name, name)
            return TangoObjectProperty(self, name)
        except IndexError:
            raise KeyError(f"No properties for object {name} found")

    async def get_info(self):
        return {}, {}

    async def put(self, name: str, value: list[str]) -> None:
        db = await get_db()
        db.put_property(self.name, {name: value})


@dataclass(frozen=True, repr=False, eq=False)
class TangoObjectProperty(TangoDeviceProperty):
    @property
    def object_name(self):
        return self.parent.name

    async def get_value(self):
        db = await get_db()
        result = await db.get_property(self.object_name, self.name)
        print("get_value", result, self.name)
        return result[self.name]

    async def get_history(self):
        db = await get_db()
        result = await db.get_property_history(self.object_name, self.name)
        return list(result)

    async def set_value(self, value: str | Sequence[str]):
        db = await get_db()
        await db.put_property(self.object_name, {self.name: value})

    async def rename(self, new_name: str):
        # There's no real way to rename a property; we just have to remove
        # it, and add a new one. This means we don't keep the history.
        db = await get_db()
        value = await db.get_property(self.object_name, self.name)
        await db.delete_property(self.object_name, self.name)
        await db.put_property(self.object_name, {new_name: value[self.name]})

    async def delete(self):
        db = await get_db()
        await db.delete_property(self.object_name, self.name)
