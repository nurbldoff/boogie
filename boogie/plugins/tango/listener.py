import asyncio
from datetime import datetime
from functools import lru_cache
import logging
from time import time, perf_counter
from typing import Sequence

import tango

from boogie.cache import cached
from .utils import get_full_name, get_device_proxy


class AttributeTracker:
    """
    Tracks the value of a Tango attribute, either by event subscription
    or by periodic reading, depending on what it supports.

    Puts values on a queue as they become available.

    TODO: retry subscribing periodically
    """

    def __init__(
        self,
        name: str,
        queue: asyncio.Queue,
        read_period: float = 3,
        subscription_retry_period: float = 30,
        extract_as=tango.ExtractAs.Numpy,
        get_device_proxy=get_device_proxy,
        logger=None,
    ):
        self.full_name = name.lower()
        self.devicename, self.name = name.rsplit("/", 1)
        self.queue = queue
        self.read_period = read_period
        self.subscription_retry_period = subscription_retry_period
        self.extract_as = extract_as
        self.get_device_proxy = get_device_proxy

        self.proxy: tango.DeviceProxy = None
        self.config_subscription: int | None = None
        self.change_subscription: int | None = None
        self.periodic_subscription: int | None = None
        self.last_subscription_attempt: datetime | None = None
        logger = logger or logging.getLogger(__name__)
        self.logger = logger.getChild(f"AttrbuteTracker({self.full_name})")
        # self.logger.propagate = False

        self.read_task = None

    def start(self):
        asyncio.create_task(self._start())

    async def _start(self):
        self.logger.debug("Starting...")
        try:
            # TODO this can take quite some time, up to 20 seconds and
            # still succeed (though it will most likely not work?)
            self.proxy = await self.get_device_proxy(self.devicename)
        except tango.DevFailed as e:
            self.logger.error(f"Failed to create proxy: {e.args[0].desc}")
            # TODO Handle! This probably means the device is not even
            # defined, so it is unlikely to start working. But we
            # should perhaps retry periodically?
            # self.status = self.Status.broken
            self._publish(e)
        else:
            self.config_subscription = await self.proxy.subscribe_event(
                self.name,
                tango.EventType.ATTR_CONF_EVENT,
                self._handle_config_event,
                stateless=True,
            )
            await self._subscribe()
            if self.change_subscription is None and self.periodic_subscription is None:
                self.read_task = asyncio.create_task(self._read_loop())

    async def stop(self):
        await self._unsubscribe()
        if self.read_task:
            self.read_task.cancel()
            self.read_task = None
        self.logger.info("Done stopping")

    def _publish(self, value: tango.DeviceAttribute | tango.AttributeInfoEx | tango.DevFailed):
        self.queue.put_nowait((self.full_name, value))

    async def _handle_change_event(self, event: tango.EventData):
        # print(event)
        if event.err:
            self.logger.debug(f"Subscription error: {event.errors[0].desc}")
            # TODO maybe we should not immediately fall back to polling
            # on *all* errors...
            if event.errors[0].reason != "API_PollThreadOutOfSync":
                return
            self._publish(tango.DevFailed(*event.errors))
            if event.event == "change":
                try:
                    await self.proxy.unsubscribe_event(self.change_subscription)
                    self.change_subscription = None
                except KeyError as e:
                    self.logger.exception(
                        f"Error unsubscribing from change events: {e}"
                    )
            else:
                try:
                    await self.proxy.unsubscribe_event(self.periodic_subscription)
                    self.periodic_subscription = None
                except KeyError as e:
                    self.logger.exception(
                        f"Error unsubscribing from periodic events: {e}"
                    )

            # Note: subscribe_event can succeed, but the first, synchronous event
            # is an error. This can happen e.g. when the attribute times out.
            # We set the sub to ERROR to signal this.
            # TODO: does this necessarily mean that the subscription is bad?
            self.subscription = -1
        else:
            # TODO it's not possible to trust event.attr_name to have the same
            # prefix as the device. Weird!
            self._publish(event.attr_value)

    async def _handle_config_event(self, event: tango.AttrConfEventData):
        # print("config event", event.attr_conf)
        if not event.err:
            self._publish(event.attr_conf)

    async def _subscribe(self):
        self.logger.debug("Subscribing...")
        try:
            t0 = time()
            change_sub = await self.proxy.subscribe_event(
                self.name,
                tango.EventType.CHANGE_EVENT,
                self._handle_change_event,
                extract_as=self.extract_as,
                # TODO try stateless? Then we should not need explicit retry
            )
            # TODO periodic events
            if self.change_subscription is None:
                self.change_subscription = change_sub
                t1 = time()
                self.logger.debug(f"Subscribed in {t1 - t0:.2f} s")
            else:
                # This means we got an immediate error event, let's unsub
                # See _handle_change_event
                await self.proxy.unsubscribe_event(change_sub)
                self.change_subscription = None
        except tango.DevFailed as e:
            self.logger.debug(f"Error subscribing to change events: {e.args[0].desc}")
        try:
            t0 = time()
            periodic_sub = await self.proxy.subscribe_event(
                self.name,
                tango.EventType.PERIODIC_EVENT,
                self._handle_change_event,
                extract_as=self.extract_as,
            )
            if self.periodic_subscription is None:
                self.periodic_subscription = periodic_sub
                t1 = time()
                self.logger.debug(f"Subscribed in {t1 - t0:.2f} s")
            else:
                # This means we got an immediate error event, let's unsub
                # See _handle_change_event
                await self.proxy.unsubscribe_event(periodic_sub)
                self.periodic_subscription = None
        except tango.DevFailed as e:
            self.logger.debug(f"Error subscribing to periodic events: {e.args[0].desc}")

    async def _unsubscribe(self):
        self.logger.debug("Unsubscribing...")
        if self.change_subscription:
            try:
                await self.proxy.unsubscribe_event(self.change_subscription)
                self.change_subscription = None
            except (KeyError, RuntimeError) as e:
                self.logger.exception(f"Error unsubscribing: {e}")
                return
            self.logger.debug("Unsubscribed")
            self.last_subscription_attempt = datetime.now()
        else:
            self.logger.debug("Not subscribed, can't unsubscribe")
        if self.periodic_subscription:
            try:
                await self.proxy.unsubscribe_event(self.periodic_subscription)
                self.periodic_subscription = None
            except (KeyError, RuntimeError) as e:
                self.logger.exception(f"Error unsubscribing: {e}")
                return
            self.logger.debug("Unsubscribed")
            self.last_subscription_attempt = datetime.now()
        else:
            self.logger.debug("Not subscribed, can't unsubscribe")

    async def _read_loop(self):
        self.logger.info("Starting read loop...")
        while True:
            try:
                self.logger.debug("Reading...")
                t0 = perf_counter()
                result = await self.proxy.read_attribute(self.name)
                self.logger.debug("Read took %.6f s", perf_counter() - t0)
                self._publish(result)
            except tango.DevFailed as e:
                self.logger.debug(f"Failed to read: {e.args[-1].desc}")
                self._publish(e)
            await asyncio.sleep(self.read_period)


logger = logging.getLogger(__name__)


class Listener:
    """
    This thing handles getting realtime values from Tango attributes and
    distributing it to "listeners". It uses subscriptions if available
    or else falls back to client polling. The idea is to create one of these
    for an entire app, to prevent duplication of work, something like
    how Taurus TangoAttribute works. But here there's nothing preventing
    from creating several ones. There should just be no reason to do it.

    Probably the "listen" method is the most convenient to use, as it returns
    an async iterator of events for any number of attributes.

    Somewhat tested, but so far only at small scale.
    """

    def __init__(self, poll_period=3.0, tick_period=0.2) -> None:
        self.event_queue: asyncio.Queue = asyncio.Queue()
        # TODO maybe use weakref here, so that we can automatically clean up
        # listeners that no longer exist?
        self.listeners: dict[
            str, list[asyncio.Queue[tuple[str, tango.DeviceAttribute]]]
        ] = {}
        self.trackers: dict[str, AttributeTracker] = {}
        self._task = None
        self._latest_data: dict[str, tango.DeviceAttribute] = {}
        self.poll_period = poll_period
        self.tick_period = tick_period
        self.tick = asyncio.Event()

    def start(self):
        "Start working"
        self._task = asyncio.create_task(self._start())
        self._tick_task = asyncio.create_task(self._ticker())

    async def _ticker(self):
        """
        This taks maintains a steady "tick" which all listeners will synchronize with.
        That allows renders to be batched, which is good for performance when there
        are frequent updates.
        It does mean that we limit the time resolution of updates, but I think this
        is a good tradeoff.
        """
        while True:
            await asyncio.sleep(self.tick_period)
            self.tick.set()
            self.tick.clear()

    async def _start(self):
        try:
            while True:
                key, value = await self.event_queue.get()
                if listeners := self.listeners.get(key):
                    for listener in listeners:
                        listener.put_nowait((key, value))
                    self._latest_data[key] = value
                elif listeners is not None:
                    logger.debug(f"No listeners for {key}, cleaning up")
                    self.listeners.pop(key)
        except Exception:
            logger.exception("Unhandled error in start task")
            raise

    # async def stop(self):
    #     self._tick_task.cancel()
    #     for queues in self.listeners.values():
    #         for queue in queues:
    #             queue.put_nowait(None)
    #     await asyncio.gather(tracker._stop() for tracker in self.tracker.values())

    def add_attrs(self, attributes: Sequence[str]):
        if not self._task:
            self.start()
        for attr in attributes:
            if attr.lower() not in self.trackers:
                tracker = AttributeTracker(attr, self.event_queue)
                self.trackers[attr.lower()] = tracker
                tracker.start()

    async def remove_attr(self, attribute: str):
        tracker = self.trackers.pop(attribute, None)
        if tracker:
            await tracker.stop()
        self._latest_data.pop(attribute, None)

    def add_listeners(
        self,
        attrs: Sequence[str],
        queue: asyncio.Queue[tuple[str, tango.DeviceAttribute]],
        latest: bool = True,
    ):
        "Add attributes to be monitored, events will be put on the queue"
        for attr in attrs:
            self.listeners.setdefault(attr, []).append(queue)
            if latest and (latest_data := self._latest_data.get(attr)):
                # Send the latest events immediately
                queue.put_nowait((attr, latest_data))
        self.add_attrs(attrs)

    async def remove_listener(
        self, attr: str, queue: asyncio.Queue[tuple[str, tango.DeviceAttribute]]
    ):
        "Remove a listener queue"
        logger.debug("Remove listener %r", attr)
        try:
            queues = self.listeners.get(attr)
            if queues:
                try:
                    queues.remove(queue)
                    if not queues:
                        # No more listeners to this attr, clean up
                        self.listeners.pop(attr)
                        await self.remove_attr(attr)
                    else:
                        pass
                except ValueError:
                    pass
        except Exception:
            logger.exception("Failed to remove listener")

    async def listen(self, attributes: Sequence[str], throttle=True, cleanup_after=10):
        "Create an async iterator for events from some attributes"
        logger.debug("Start listening to %r", attributes)
        full_attributes = [get_full_name(attr) for attr in attributes]
        queue: asyncio.Queue[tuple[str, tango.DeviceAttribute]] = asyncio.Queue()
        self.add_listeners(full_attributes, queue)
        while True:
            try:
                if throttle:
                    # Just send the latest event per attribute and type
                    events = {}
                    while not queue.empty():
                        key, data = queue.get_nowait()
                        events[key, type(data)] = (key, data)
                    for event in events.values():
                        yield event
                else:
                    # Send all events
                    while not queue.empty():
                        yield await queue.get()
                await self.tick.wait()  # Await next tick
            except asyncio.CancelledError:
                # This means whoever created the listener is done with it
                break

        asyncio.create_task(self._cleanup_later(full_attributes, queue, cleanup_after))

    async def _cleanup_later(self, attributes, queue, delay):
        # TODO
        # This is an unsophisticated way to manage listeners exiting.
        # The idea is to keep listeners around for a few seconds, since
        # the most likely attributes to be used again are the same ones.
        # I think this is not the best place to do it though.
        #
        # TODO the queue is filling up while we wait, which is kind of
        # a memory leak, if limited...
        logger.debug("Waiting to cleanup listeners")
        try:
            await asyncio.sleep(delay)
        finally:
            await asyncio.gather(*[
                self.remove_listener(attr, queue)
                for attr in attributes
            ])

        logger.debug("Stopped listening to %r", attributes)


@lru_cache(10)
def get_listener(poll_period=3.0):
    return Listener(poll_period)


if __name__ == "__main__":
    import sys

    # logging.basicConfig(level=logging.DEBUG)

    queue: asyncio.Queue[tuple[str, tango.DeviceAttribute]] = asyncio.Queue()

    async def main():
        # listener = AttributeTracker(sys.argv[1], queue)
        # listener.start()
        # while True:
        #     data = await queue.get()
        #     print(data)

        listener = Listener()
        async for attr, data in listener.listen(sys.argv[1:]):
            print(attr, data)

    asyncio.run(main())
