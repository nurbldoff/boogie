import asyncio
import time

import tango

from .utils import get_device_proxy, get_db


MODE_UNCONTROLLED = 0
MODE_CONTROLLED = 1


async def start_server(hostname: str, server: str, level: int | None = None):
    """Start a server via the starter device"""
    print("start_server", hostname, server)
    short_hostname = hostname.split(".", 1)[0]
    starter = await get_device_proxy(f"tango/admin/{short_hostname}")
    print("starter", starter)
    db = await get_db()
    info = await db.get_server_info(server)
    if (
        info.host.split(".")[0] != short_hostname
        or level is not None
        and info.level != level
        or info.mode != MODE_CONTROLLED
    ):
        info.mode = MODE_CONTROLLED
        info.host = hostname
        if level is not None:
            info.level = level
        await db.put_server_info(info)
    try:
        await starter.DevStart(server)
    except tango.DevFailed as e:
        print(e)
        if e.args[0].reason == "ALREADY_RUNNING":
            return False
        raise e

    while True:
        servernames = await starter.DevGetRunningServers(False)
        if server in servernames:
            return True

        await asyncio.sleep(0.5)


async def stop_server(
    hostname: str, servername: str, force: bool = False, force_timeout: int = 10
):
    """Stop a server via the starter device"""
    starter = await get_device_proxy(f"tango/admin/{hostname}")

    try:
        if force:
            await starter.HardKillServer(servername)
        else:
            await starter.DevStop(servername)

    except tango.DevFailed as e:
        if e.args[0].reason == "SERVER_NOT_RUNNING":
            return
        raise e

    t0 = time.time()
    force_kill_count = 0
    while True:
        servernames = await starter.DevGetStopServers(False)
        if servername in servernames:
            break

        await asyncio.sleep(0.5)

        # Force stop
        delta = time.time() - t0
        if delta > force_timeout:
            try:
                await starter.HardKillServer(servername)
                await asyncio.sleep(0.5)

                servernames = await starter.DevGetStopServers(False)
                if servername in servernames:
                    break

            except tango.DevFailed as e:
                if e.args[0].reason == "SERVER_NOT_RUNNING":
                    return
                raise e

            # Sleep 1 sec before trying to kill again
            await asyncio.sleep(1)

            if force_kill_count > 5:
                raise RuntimeError(f"could not (even) force kill {servername}")
