from dataclasses import dataclass

from boogie.cache import cached
from .entry import TangoEntry
from .utils import get_db
from .device import TangoDevice


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceAliases(TangoEntry):
    parent: None = None
    name: str = "Alias"

    async def get_children(self):
        db = await get_db()
        server_query = """
        SELECT DISTINCT alias FROM device
        WHERE alias IS NOT NULL
        ORDER BY alias
        """
        db = await get_db()
        _, aliases = await db.command_inout("DbMySQLSelect", server_query)

        return (TangoDeviceAlias(self, alias) for alias in aliases)

    async def get_child(self, alias: str):
        db = await get_db()
        if await db.get_device_from_alias(alias):
            return TangoDeviceAlias(self, alias)
        raise KeyError()


@dataclass(frozen=True, repr=False, eq=False)
class TangoDeviceAlias(TangoDevice):
    parent: TangoDeviceAliases  # type: ignore  # TODO fixme

    @cached
    async def get_device(self):
        db = await get_db()
        return await db.get_device_from_alias(self.name)

    async def get_alias(self):
        return self.name
