"""
A plugin purely for testing purposes. Can also serve as a minimal example
of a plugin.

It can be used by creating a JSON file somewhere, and setting the env var
BOOGIE_TEST_PATH to its filesystem path. E.g:

    $ BOOGIE_TEST_DATA=/tmp/testdata.json boogie

A new tab called "Test" should appear at the top right.

The format for the JSON file is simple, an example can be found in
'tests/testdata.json'.
"""

import asyncio
from dataclasses import dataclass
import json
import os

from textual.widgets import Static

from boogie.entry import Entry
from boogie.cache import cached
from boogie.widgets.details import Details, DetailsList


INDEX = 10000000  # Make it appear last in the tab bar

DATA_PATH = os.environ.get("BOOGIE_TEST_DATA")


# === Entries ===


@dataclass(frozen=True, repr=False, eq=False)
class TestEntry(Entry):
    # Required standard fields
    parent: None = None
    name: str = "Test"

    # Custom field for passing data down to children
    data: str | None = None

    @cached
    async def get_data(self):
        if self.data:
            return json.loads(self.data)
        try:
            with open(DATA_PATH) as f:
                return json.load(f)
        except FileNotFoundError:
            print("Test data not found at", DATA_PATH)

    async def get_children(self):
        data = await self.get_data()
        if delay := data.get("delay"):
            await asyncio.sleep(delay)
        return [
            (
                TestLeafEntry(self, child["name"], data=json.dumps(child))
                if child.get("leaf")
                else TestEntry(self, child["name"], data=json.dumps(child))
            )
            for child in data.get("children", [])
        ]

    async def get_child(self, name):
        data = await self.get_data()
        if delay := data.get("delay"):
            await asyncio.sleep(delay)
        child = None
        for child in data.get("children", []):
            if child["name"].lower() == name.lower():
                break
        else:
            raise KeyError(f"Found no child called {name}")
        return (
            TestLeafEntry(self, child["name"], data=json.dumps(child))
            if child.get("leaf")
            else TestEntry(self, child["name"], data=json.dumps(child))
        )


class TestLeafEntry(TestEntry):
    leaf_node = True


# === Widgets ===


class TestDetailsList(DetailsList[TestLeafEntry]):
    COLUMNS = "Foo", "Bar", "Baz"

    async def get_row(self, entry):
        data = await entry.get_data()
        if "row" in data:
            row = data["row"]
            assert len(row) == 3, "Row must contain precisely 3 items"
            return row, {}
        return ("A", "B", "C"), {}


class TestDetails(Details[TestEntry]):
    def compose(self):
        yield Static()

    async def entry_changed(self, entry):
        self.query_one(Static).update(f"I am '{entry.name}'!")


# === Plugin config ===

if DATA_PATH:
    # If the env var is not set, the plugin will not appear

    async def get_title():
        return DATA_PATH

    BROWSERS = [
        {
            "root": TestEntry,
            "get_title": get_title,
        }
    ]
