# Documentation

This is the user documentation for Boogie, a terminal based browser
for Tango control systems.

Boogie is *under construction*. If you find it is not correct, feel free
to leave an issue at https://gitlab.com/nurbldoff/boogie/ This also
goes for any other problems you find with the application. But,
consider that it is not yet considered reliable for everyday use. This
will be the case until it reaches version 1.0.

## Global key bindings

Available key bindings are shown in the footer of the application.
However some are always available and therefore omitted for space reasons.

| Key(s)             | Description                                              |
|--------------------|----------------------------------------------------------|
| `F1`               | Browse this documentation                                |
| `F10`, `Ctrl+q`    | Quit the application                                     |
| `Tab`, `Shift+Tab` | Cycle focus between widgets, forwards and backwards.     |
| `Esc`              | Exit the current context, e.g. a dialog, filter or edit. |
| `1, 2, ...`        | Switch to the tab with corresponding number              |
| `Ctrl+left/right`  | Go back and forward in navigation history                |
| `F2`               | Display the navigation history                           |
| `F3`               | Display the bookmarks                                    |
| `F4`               | Display the Monitor                                      |
| `F5`               | Reload the displayed data                                |
| `+`, `-`, `=`      | Grow, shrink or reset the current panel                  |
| `/`                | Search within the current tab                            |
| `F11`              | "Maximize" current widget (if it makes sense)            |

You can always summon a complete binding table with **Ctrl+h** (press again to hide)

## Local key bindings

- Some widgets such as the tree view and listings, allow filtering
  their content for easier navigation. If this is supported This is
  done by pressing **%** (percent) which brings you to an
  input. Type some characters and then return. This will apply your
  filter to the widget. The same key can be used to edit the
  filter. **Esc** clears any current filter.

- Usually, when a widget contains its own tabs, they can be selected
  by pressing the initial letter of the tab name.

- Further context sensitive key bindings are shown in the footer.

## General layout

The major parts of the application are:

```
   | Foo |  Bar  Baz
  +--------+----------------------+
  |           Location            |
  +--------+----------------------+
  |        |                      |
  |        |                      |
  |  Tree  |       Details        |
  |        |                      |
  |        |                      |
  +--------+----------------------+
```

At the top there's a main tab bar, showing all the available
"trees". This depends on the currently active plugins.

Below that is the "location" bar, that tells you what is currently
being inspected.

The tree view on the left shows a hierarchical view of the
entries available for this tree.

Those parts are always displayed. The right side ("details") however
is dynamic, and may show whatever is needed to convey information
about the current location. It may or may not be interactive, and can
consist of several parts, depending on the needs.

At the very top of the application, the current control system is
displayed.  The footer at the bottom shows currently active key
bindings.

Communication errors etc are usually shown at the bottom of the
Details area, in red.

The current *focus* of the application is important as it determines
which part is currently receiving keyboard input. The area currently
in focus is highlighted by a thick white border on the left. The focus
may be moved around using keyboard shortcuts detailed below.

## Mouse control

All parts of the application also support interaction with the mouse,
e.g. clicking buttons, choosing items, dragging scrollbars.

Sometimes, more information about something can be obtained by
hovering the mouse over it, causing a "tooltip" to be shown.

Underlined text usually means it's a link, that will jump to some other
location when clicked. Remember that it's always possible to get back
to where you were, using the back button or the history.

It's normally not possible to select text from the application, as it
runs in the terminal's "application" mode. However some terminals
allow bypassing this mode, e.g. by pressing **Shift** and dragging the
mouse.

There is some experimental clipboard support built in to Boogie. In
some places (where it makes sense) you can press "C" to copy text. This
goes for property valyes, attribute read values etc. This may or may
not work depending on platform and installation details.


## Tree

The "tree" view on the left is the most basic navigation tool. When it
is focused, the cursor can be moved using the arrow keys. Left/right 
folds/unfolds nodes. Use **enter** to select the current tree node. 
Doing that will navigate to that particular node, displaying the current 
"path" at the top and any available information on the right.

What is displayed in the tree depends on which "tab" is selected at
the top. Any form of hierarchical information could be displayed, if
there is an appropriate plugin for it.


## History and bookmarks

(Still somewhat experimental features, so don't be too surprised
if your history or bookmarks suddenly disappear!)

All "important" interactions with the application - such as
navigation, database changes, etc - are logged in a sqlite
database. Probably the main way to use this is via the "back/forward"
actions (**Ctrl+Left**, **Ctrl+Right**). **Ctrl+Up** goes "up" to the
parent of the current entry.

The whole history can be accessed with the *history browser*, accessed
via **F2**. It shows a chronologically ordered list, and by default is
filtered to only display navigation. Clicking or pressing **Enter** on
a line in the list jumps to that location. Further bindings are
displayed at the bottom as usual.

The database also stores your *bookmarks*. You can add a bookmark to
the current location by pressing **Ctrl+B**. This can be useful for
storing frequently accessed locations, or noting interesting
things. The list of bookmarks is opened by pressing **F3**. It mostly
works like the history browser but also allows adding text notes to
bookmarks with **e**. Bookmarks can be deleted by pressing **Delete**.

With **Alt+b** you can create a bookmark and add a note immediately.

## Search

Each tab may implement a search function. For example the "Devices"
tab allowes searching for devices by name. The search feature is
accessed by pressing **/**, typing a search string and pressing
`Enter`. Selecting a result and pressing `Enter` navigates to the
result.

The search string may contain "*" as explicit wildcards, otherwise
anything *containing* the given string is considered a match.


## Monitor

It is often useful to show a "live" representation of e.g. an attribute,
to watch for changes. Boogie has a *Monitor* screen for this purpose.
You can get to the monitor screen with `F4`.

Where it makes sense (e.g. on an attribute) you can press `m` to add
things to the monitor screen. Those things will be monitored in the
background and shown on the Monitor screen.

On the *Monitor* screen, there are a few actions available.

* Arrow keys to select between the widgets
* Press `delete` to remove a widget from the monitor list
* `.` (period) to swap between different representations (simple,
  trend, ...). The widgets available depends on the kind of item.
  * `shift+up/down` moves the focused widget in the list
* `+/-` to change the size of the time window for trends and such.
* `a` adds a new "tab" for monitor widgets 
* `r` renames the current tab (mainly useful for save/load, see below)


### Save and load

(Experimental feature!)

You can save your monitors to disk by pressing `s`. This stores the
open tabs under their names, in a local database file. You can later
restore these tabs by pressing `o` and selecting from the list.

Note: since the tab name is used for identifying monitors, saving
will replace previous saves under the same names. They are not
overwritten, but currently only the latest "version" is accessible.
In the future it will likely become possible to restore old versions.


## External editor

For some editing tasks (currently, property values) it can be nice
to use your favorite text editor. If you set the standard `EDITOR` env
variable properly, pressing `E` on a property value should suspend
boogie and bring up your chosen editor. 

    EDITOR=nano boogie
 
Saving and closing the editor brings you back. Close without saving 
to abort the edit.
 
This feature works on Linux, support for other platforms is unknown.
