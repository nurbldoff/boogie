from functools import lru_cache
from typing import Tuple, Dict, Optional

from rich.style import Style
from rich.color import ColorSystem


ANSI_RESET_COLOR = "\033[0;39m"
ANSI_RESET_BACKGROUND = "\033[0;49m"

BRAILLE_COLS = 2
BRAILLE_ROWS = 4
BRAILLE_ZERO = 10240


class BrailleCanvas:
    """
    Create a canvas for drawing with unicode braille characters.

    The "canvas" here refers to a rectangular block of pixels that
    can be drawn on using unicode "braille" characters. This means
    that the size in pixels is twice the width and four times the
    height in characters, giving the impression of higher resolution.

    The width and height are given in characters and determine the
    size of the canvas as printed.

    Methods exist for drawing individual pixels, and lines, using
    colors. Pixel drawing methods take coordinates in pixels.
    Colors are given using "Rich" style color strings, e.g. standard
    color names #rrggbb, etc.

    A limitation is that each character can only have one color, so if
    there are different color pixels within a character, the last drawn
    pixel's color is used.

    It's also possible to draw ordinary text strings, at character
    coordinates.
    """

    CHAR_COLS = BRAILLE_COLS
    CHAR_ROWS = BRAILLE_ROWS

    def __init__(self, width: int = 80, height: int = 20, background=False) -> None:
        self.width = width
        self.height = height
        self.background = background
        self.reset = ANSI_RESET_COLOR + (ANSI_RESET_BACKGROUND if background else "")
        self.w = width * self.CHAR_COLS  # width in braille dots
        self.h = height * self.CHAR_ROWS  # height in braille dots
        self.pixels: Dict[Tuple[int, int], dict[int, str | None]] = {}
        self.letters: Dict[Tuple[int, int], Tuple[str, str | None]] = {}

    def draw_pixel(self, x: int, y: int, color: Optional[str] = None) -> None:
        r"""
        Put a single pixel on the canvas with optional color.

        >>> show = lambda c, delim: print(delim +  "|\n|".join(c.render()) + delim)
        >>> c = BrailleCanvas(3, 3)
        >>> c.draw_pixel(0, 0)
        >>> show(c, "|")
        |⠁⠀⠀|
        |⠀⠀⠀|
        |⠀⠀⠀|
        >>> c = BrailleCanvas(3, 3)
        >>> c.draw_pixel(1, 1)
        >>> show(c, "|")
        |⠐⠀⠀|
        |⠀⠀⠀|
        |⠀⠀⠀|
        >>> c = BrailleCanvas(3, 3)
        >>> c.draw_pixel(1, 4)
        >>> show(c, "|")
        |⠀⠀⠀|
        |⠈⠀⠀|
        |⠀⠀⠀|

        """
        x1, y1 = x // self.CHAR_COLS, y // self.CHAR_ROWS
        if x1 < 0 or x1 >= self.width or y1 < 0 or y1 >= self.height:
            return

        # The Braille Unicode block is ordered a bit strangely, where the
        # first fourth only deals with six dot patterns. So we treat bottom
        # row pixels separately
        x2, y2 = x % self.CHAR_COLS, y % self.CHAR_ROWS
        if y2 < 3:
            p = 2 ** (y2 + 3 * x2)
        elif x2 == 0:
            p = 64
        elif x2 == 1:
            p = 128
        if pixels := self.pixels.get((x1, y1)):
            pixels[p] = color
        else:
            self.pixels[x1, y1] = {p: color}

    def draw_line(
        self, p0: Tuple[int, int], p1: Tuple[int, int], color: Optional[str] = None
    ) -> None:
        r"""
        Draw a straight line from p0 to p1 using an (optional) color.

        >>> show = lambda c, delim: print(delim +  "|\n|".join(c.render()) + delim)
        >>> c = BrailleCanvas(3, 3)
        >>> c.draw_line((0, 0), (3*2-1, 3*4-1))
        >>> show(c, "|")
        |⢣⠀⠀|
        |⠀⢣⠀|
        |⠀⠀⢣|
        >>> show = lambda c, delim: print(delim +  "|\n|".join(c.render()) + delim)
        >>> c = BrailleCanvas(3, 3)
        >>> c.draw_line((0, 0), (0, 3*4-1))
        >>> show(c, "|")
        |⡇⠀⠀|
        |⡇⠀⠀|
        |⡇⠀⠀|
        >>> show = lambda c, delim: print(delim +  "|\n|".join(c.render()) + delim)
        >>> c = BrailleCanvas(3, 3)
        >>> c.draw_line((0, 3), (4, 4))
        >>> show(c, "|")
        |⣀⠀⠀|
        |⠀⠉⠁|
        |⠀⠀⠀|
        >>> show = lambda c, delim: print(delim +  "|\n|".join(c.render()) + delim)
        >>> c = BrailleCanvas(3, 3)
        >>> c.draw_line((3, 3), (3, 3))
        >>> show(c, "|")
        |⠀⢀⠀|
        |⠀⠀⠀|
        |⠀⠀⠀|
        """

        x, y = p0
        x0, y0 = p0
        x1, y1 = p1
        # Skip drawing if the whole line is obviously outside the canvas
        # TODO this can be smarter
        if ((x0 < 0 and x1 < 0)
            or (x0 >= self.w and x1 >= self.w)
            or (y0 < 0 and y1 < 0)
            or (y0 >= self.h and y1 >= self.h)):
            return

        dx = abs(x1 - x)
        sx = 1 if x < x1 else -1
        dy = -abs(y1 - y)
        sy = 1 if y < y1 else -1
        err = dx + dy

        self.draw_pixel(x, y, color)
        while x != x1 or y != y1:
            e2 = 2 * err
            if e2 >= dy:
                err += dy
                x += sx
            if e2 <= dx:
                err += dx
                y += sy
            self.draw_pixel(x, y, color)

    def draw_text(self, x: int, y: int, text: str, color: str | None = None) -> None:
        """
        Put a text string at the given position (in character coordinates),
        with optional color
        """
        for i, char in enumerate(text):
            px = x + i
            if 0 <= px < self.width:
                self.letters[px, y] = (char, color)

    @lru_cache(maxsize=1024)
    def _get_ansi_code(
        self, color: Optional[str], bgcolor: Optional[str] = None
    ) -> str | None:
        "Get ansi code for a color given by a Rich color string"
        if color is not None:
            style = Style(color=color, bgcolor=bgcolor)
            assert style.color is not None
            attrs = style._ansi or style._make_ansi_codes(ColorSystem.TRUECOLOR)
            return f"\x1b[{attrs}m"
        return None

    @lru_cache(maxsize=1024)
    def _build(
        self, positions: tuple[int], colors: tuple[str], ordered: bool = True
    ) -> tuple[str, str, str | None]:
        "Turn a pixel block representation into a unicode character"
        color = colors[-1]
        positions2 = (p for p, c in zip(positions, colors) if c == color)
        return chr(BRAILLE_ZERO + sum(positions2)), color, None

    def render(self) -> list[str]:
        """Build a list of printable strings out of the current canvas."""

        buf = []
        for y in range(self.height):
            current_color = None
            line = ""
            for x in range(self.width):
                pos = (x, y)
                letter = self.letters.get(pos)
                if letter is not None:
                    char, color = letter
                    code = self._get_ansi_code(color)

                    if code != current_color:
                        line += f"\033[1;3{code}m"
                        current_color = code
                    line += char
                else:
                    pixels = self.pixels.get(pos, -1)
                    if pixels == -1:
                        if self.background:
                            line += ANSI_RESET_BACKGROUND
                        line += chr(BRAILLE_ZERO)
                        continue
                    assert isinstance(pixels, dict)
                    positions = tuple(pixels.keys())
                    colors = tuple(pixels.values())
                    char, color, bgcolor = self._build(positions, colors)
                    code = self._get_ansi_code(color, bgcolor)
                    if code != current_color:
                        if self.background and bgcolor is None:
                            line += ANSI_RESET_BACKGROUND

                        if code is None:
                            # If color is not set, use terminal's default
                            line += self.reset
                        else:
                            line += code
                        current_color = code
                    line += char
            if current_color:
                # Let's not leave ascii colors turned on
                line += self.reset
            buf.append(line)
        return buf


BLOCK_ZERO = 0x2580


class HorizontalBlockCanvas(BrailleCanvas):
    "TODO this is pretty janky"

    CHAR_COLS = 2
    CHAR_ROWS = 1

    def draw_pixel(self, x: int, y: int, color: Optional[str] = None) -> None:
        r"""
        Put a single pixel on the canvas with optional color.

        >>> show = lambda c, delim: print(delim +  "|\n|".join(c.render()) + delim)
        >>> c = HorizontalBlockCanvas(3, 3)
        >>> c.draw_pixel(0, 0)
        >>> show(c, "|")
        |▌⠀⠀|
        |⠀⠀⠀|
        |⠀⠀⠀|
        >>> c = HorizontalBlockCanvas(3, 3)
        >>> c.draw_pixel(1, 0)
        >>> show(c, "|")
        |▐⠀⠀|
        |⠀⠀⠀|
        |⠀⠀⠀|
        >>> c = HorizontalBlockCanvas(3, 3)
        >>> c.draw_pixel(1, 2)
        >>> show(c, "|")
        |⠀⠀⠀|
        |⠀⠀⠀|
        |▐⠀⠀|
        """
        x1, y1 = x // self.CHAR_COLS, y // self.CHAR_ROWS
        if any([x1 < 0, x1 >= self.width, y1 < 0, y1 >= self.height]):
            return

        x2 = x % self.CHAR_COLS
        if x2 == 0:
            p = 12
        else:
            p = 16
        self.pixels.setdefault((x1, y1), {})[p] = color

    @lru_cache(maxsize=256)
    def _build(
        self, positions: tuple[int], colors: tuple[str], ordered: bool = True
    ) -> tuple[str, str, str | None]:
        "Turn a pixel block representation into a unicode character"
        color = colors[0]
        background = colors[-1]
        if len(positions) == 1:
            return chr(BLOCK_ZERO + positions[0]), color, None
        if background == color:
            return chr(0x2588), color, None
        # positions = [p for p, c in zip(positions, colors) if c == color]
        # position = positions[0]
        return chr(BLOCK_ZERO + 12), color, background


if __name__ == "__main__":
    import os
    from random import randint, choice
    from time import time

    size = os.get_terminal_size()
    canvas = BrailleCanvas(width=size.columns-2, height=size.lines-2)

    colors = [
        "rgb(255,0,0)",
        "blue",
        "green_yellow",
        "color(1)",
        "#908020",
        "rgb(180,0,90)",
    ]

    t0 = time()
    for i in range(10000):
        color = choice(colors)
        canvas.draw_line(
            (randint(0, canvas.w), randint(0, canvas.h)),
            (randint(0, canvas.w), randint(0, canvas.h)),
            color,
        )

    # print("|" + "\n|".join(canvas.render()))
    print("Took", time() - t0, "seconds")

    t0 = time()
    for i in range(10000):
        color = choice(colors)
        canvas.draw_line(
            (randint(0, canvas.w), randint(0, canvas.h)),
            (randint(0, canvas.w), randint(0, canvas.h)),
            color,
        )

    # print("|" + "\n|".join(canvas.render()))
    print("Took", time() - t0, "seconds")

    t0 = time()
    for i in range(10000):
        color = choice(colors)
        canvas.draw_line(
            (randint(0, canvas.w), randint(0, canvas.h)),
            (randint(0, canvas.w), randint(0, canvas.h)),
            color,
        )

    # print("|" + "\n|".join(canvas.render()))
    print("Took", time() - t0, "seconds")

    
    # canvas = HorizontalBlockCanvas(background=True)

    # colors = [
    #     "rgb(255,0,0)",
    #     "blue",
    #     "green_yellow",
    #     "color(1)",
    #     "#908020",
    #     "rgb(180,0,90)",
    # ]

    # x = 0
    # y = 0
    # for i in range(100):
    #     color = choice(colors)
    #     # canvas.draw_line(
    #     #     (randint(0, canvas.w), randint(0, canvas.h)),
    #     #     (randint(0, canvas.w), randint(0, canvas.h)),
    #     #     color,
    #     # )

    #     x1 = x
    #     canvas.draw_pixel(0, 0, "blue")
    #     canvas.draw_pixel(1, 0, "red")
    #     canvas.draw_line((5, 0), (5, 0), "green")
    #     canvas.draw_line((2, 0), (4, 0), "yellow")

    #     canvas.draw_line((5, 1), (10, 1), "green")

    #     # canvas.draw_line(
    #     #     (1, 0),
    #     #     (2, 0),
    #     #     colors[1],
    #     # )

    #     x = x1 + 1

    #     # canvas.draw_pixel(0, 0, colors[0])

    # print("|" + "\n|".join(canvas.render()))
