from argparse import ArgumentParser

from textual_serve.server import Server  # type: ignore


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-p", "--port", type=int, default=8000)
    args = parser.parse_args()

    server = Server("python -m boogie", title="Boogie", port=args.port)
    server.serve()
