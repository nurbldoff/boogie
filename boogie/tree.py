"""
Tree navigator widget
"""

import asyncio
from typing import Callable

from textual import work
from textual.message import Message
from textual.widgets import Tree
from textual.widgets.tree import TreeNode
from textual.binding import Binding
from textual.app import App, ComposeResult
from textual.geometry import Region
from rich.text import Text

from .entry import Entry, EntryType
from .widgets.entry import EntryWidget
from .util import get_plugins


class CustomTree(Tree[Entry]):
    """
    Tree widget for entries.
    """

    BINDINGS = [
        Binding("left", "cursor_back", "Cursor Back", show=False),
        Binding("right", "cursor_forward", "Cursor Forward", show=False),
    ]

    def __init__(self, entry: Entry, get_bookmark: Callable | None, *args, **kwargs):
        super().__init__(entry.name, entry, *args, **kwargs)
        self.get_bookmark = get_bookmark
        self.show_root = False
        self.auto_expand = False
        self.guide_depth = 2
        self._load_lock = asyncio.Lock()
        self._node_states: dict[Entry, bool] = {}  # Stores expanded nodes
        self.root.expand()

    async def find_node(self, root, path) -> TreeNode[Entry]:
        try:
            entry, *rest = path
        except ValueError:
            return root
        # Make sure the root node is expanded
        for node in root.children:
            if node.data.path_str().lower() == entry.path_str().lower():
                if entry.leaf_node:
                    return node
                if rest:
                    # A bit complicated; if the node is not expanded
                    # the node may not yet exist. In that case, we load the
                    # children manually (otherwise this would happen
                    # asynchronously, meaning hard to handle here)
                    with self.prevent(Tree.NodeExpanded):
                        node.expand()
                    try:
                        return await self.find_node(node, rest)
                    except ValueError:
                        await self._load_children(node, force=True)
                        try:
                            return await self.find_node(node, rest)
                        except ValueError:
                            pass
                return node
        raise ValueError("path could not be found")

    def go_to(self, node, scroll=True):
        # TODO can we avoid this if the node is already visible?
        if scroll:
            self.scroll_to_node(node)
        if node != self.cursor_node:
            with self.prevent(Tree.NodeSelected):
                # When moving programmatically, we don't want to send events as we're
                # likely already responding to an event.
                self.select_node(node)

    async def reload(self) -> None:
        """
        Reload the tree from scratch, and attempt to restore the
        state (opened nodes, cursor node)
        """
        node = self.cursor_node
        if node and node.data:
            path = node.data.path
        else:
            path = None
        with self.app.batch_update():
            states = self._node_states
            self._node_states = {}
            await self._load_children(self.root, force=True)
            for entry in list(states.keys()):
                try:
                    node = await self.find_node(self.root, entry.path[1:])
                    if node:
                        node.expand()
                except ValueError:
                    pass

        if path:
            # TODO find a better way to ensure that things are loaded so that
            # we can revisit the current node. Currently this happens after
            # the batch, potentially leading to flicker.
            async def finish_reload(path):
                try:
                    node = await self.find_node(self.root, path[1:])
                    self.call_after_refresh(self.go_to, node, scroll=False)
                except ValueError:
                    pass

            self.call_after_refresh(finish_reload, path)

    async def _load_children(self, node, force=False) -> None:
        "Called when a tree node is expanded, to populate its children"
        async with self._load_lock:  # Prevent simultaneous loading
            if node._children and not force:
                # Already populated. Reload to refresh the data.
                return
            node.remove_children()
            with self.app.batch_update():
                try:
                    for child in await node.data.get_children():
                        label = await child.get_label()
                        if self.get_bookmark:
                            bookmark = self.get_bookmark(child.path_str())
                        else:
                            bookmark = None
                        if bookmark:
                            label = Text.assemble(label, "🔖")
                        if child.leaf_node:
                            node.add_leaf(label, child)
                        else:
                            node.add(label, child)
                except Exception as e:
                    self.app.notify(f"Failed to load children for entry {node}: {e}")

    async def _on_tree_node_expanded(self, message: Tree.NodeExpanded[Entry]) -> None:
        message.stop()
        await self._load_children(message.node)
        assert message.node.data
        self._node_states[message.node.data] = True

    def on_tree_node_collapsed(self, message):
        self._node_states.pop(message.node.data, None)

    def action_cursor_back(self) -> None:
        line = self._tree_lines[self.cursor_line]
        node = line.path[-1]
        if node.is_expanded:
            node.collapse()
            self.post_message(self.NodeCollapsed(node))
        elif len(line.path) > 1:
            parent = line.path[-2]
            self.cursor_line = parent.line
            self.scroll_to_line(self.cursor_line)

    async def action_cursor_forward(self) -> None:
        line = self._tree_lines[self.cursor_line]
        node = line.path[-1]
        if not node.allow_expand:
            return
        if node.is_expanded:
            self.cursor_line += 1
        else:
            node.expand()
        self.scroll_to_line(self.cursor_line)

    def _get_label_region(self, line: int) -> Region | None:
        """Returns the region occupied by the label of the node at line `line`."""

        # Override that ensures that the little arrow also gets scrolled into
        # view when a tree node is highlighted. I think it's more intuitive.

        try:
            tree_line = self._tree_lines[line]
        except IndexError:
            return None
        region_x = tree_line._get_guide_width(self.guide_depth, self.show_root)
        region_width = self.get_label_width(tree_line.node)
        return Region(region_x - 2, line, region_width + 2, 1)


class BoogieTree(EntryWidget):
    """
    Display a tree widget for a root entry.
    """

    BINDINGS = [
        Binding("plus", "grow", "Grow", show=False),
        Binding("minus", "shrink", "Shrink", show=False),
        Binding("equals_sign", "reset", "Reset", show=False),
    ]

    class Selected(Message):
        def __init__(self, entry: Entry):
            self.entry = entry
            super().__init__()

    def __init__(self, get_bookmark: Callable | None = None, *args, **kwargs):
        self.get_bookmark = get_bookmark
        super().__init__(*args, **kwargs)

    def on_mount(self):
        self.loading = True

    async def entry_changed(self, entry: EntryType):
        self.loading = True
        self.remove_children()
        tree = CustomTree(entry, self.get_bookmark)
        await self.mount(tree)
        self.loading = False
        tree.focus()

    def on_tree_node_selected(self, event: Tree.NodeSelected[Entry]) -> None:
        event.stop()
        entry = event.node.data
        assert entry
        self.post_message(self.Selected(entry))

    async def select_entry(self, entry: Entry):
        """
        Select the node corresponding to the given entry.
        The path may be deeper than the tree (e.g if something has been
        removed), in which case we stop at the last node available.
        """
        tree = self.query_one(CustomTree)
        try:
            node = await tree.find_node(tree.root, entry.path[1:])
            # Works better if we don't call this immediately, I think
            # because we need to wait for stuff to be mounted properly
            self.call_after_refresh(tree.go_to, node)
        except ValueError:
            self.log("Could not find tree node for", entry=entry, root=tree.root)

    @work(exclusive=True)
    async def reload(self):
        tree = self.query_one(CustomTree)
        await tree.reload()

    def action_grow(self):
        width = self.styles.width
        self.styles.width = f"{width.value + 5}{width.symbol}"

    def action_shrink(self):
        width = self.styles.width
        self.styles.width = f"{max(0, width.value - 5)}{width.symbol}"

    def action_reset(self):
        self.styles.width = None


class TreeApp(App[None]):
    # Just for testing

    CSS = """
    Screen {
        padding: 0;
        margin: 0;
        background: black;
        &:inline {
            border: none;
            height: 50vh;
        }
    }
    """

    BINDINGS = [
        ("plus", "zoom_in", "Zoom in"),
        ("minus", "zoom_out", "Zoom out"),
    ]

    def compose(self) -> ComposeResult:
        yield BoogieTree()

    async def on_mount(self) -> None:
        tree = self.query_one(BoogieTree)
        plugins = get_plugins()
        browsers = plugins[0].BROWSERS
        tree.set_entry(browsers[0]["root"]())


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser()

    args = parser.parse_args()

    app = TreeApp()
    app.run()
