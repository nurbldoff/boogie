from __future__ import annotations
import asyncio
from functools import partial
from inspect import signature
import logging
from typing import Sequence, Generic
from typing import TYPE_CHECKING

from rich.pretty import Pretty
from textual import on
from textual.events import Mount
from textual.widget import Widget
from textual.reactive import reactive, var
from textual.widgets import Static
from textual.containers import VerticalScroll
from textual.css.query import NoMatches
from textual.message import Message

if TYPE_CHECKING:
    from ..app import Boogie

from ..entry import Entry, EntryType
from ..util import get_widget_class


logger = logging.getLogger(__name__)


class EntryWidget(Widget, Generic[EntryType]):
    """
    A very basic widget class that has an Entry (None at init).
    The entry can be changed, and the widget should update
    accordingly.

    Use Details/DetailsList for the main details widgets,
    but subwidgets to those can be EntryWidgets if they need
    an entry.

    The intended way of using this class is to subclass it and set
    the entry_class to whatever entry it's supposed to handle.
    The widget may implement _update() to do whatever needs to
    happen when "entry" is set. E.g. update entry on child widgets,
    etc.
    """

    # Subpath is the part of the current path beginning with the entry.
    # It can be useful e.g. when displaying a list to also be able to
    # highlight the current selected child entry.
    # subpath: var[Sequence[Entry]] = var([], init=False, always_update=True)

    app: Boogie
    entry: EntryType | None
    _subsubpath: Sequence[Entry]

    def __init__(self, *args, **kwargs) -> None:
        self._mounted = asyncio.Event()
        self.entry = None
        self._subsubpath = []
        super().__init__(*args, **kwargs)

    # "entry" is not a reactive var, it's just reflecting "subpath". But for
    # convenience we make it sort of work a bit like one anyway.
    # But typically setting subpath is better, as it allows widgets to reflect
    # selected child too.
    # TODO This seems overcomplicated, there should be a better solution.

    _error: var[Exception | None] = var(None, init=False, always_update=False)

    class Error(Message):
        "Message posted when there's an error to be displayed"

        def __init__(self, error: Exception):
            self.error = error
            super().__init__()

    def set_entry(self, entry: EntryType, *subsubpath: Entry, reload=False) -> None:
        """
        Set the entry for this widget.
        """
        # OK this is a bit messy, naming needs work
        # - subpath = [entry, *subsubpath] :P
        self.log("%r.set_entry(%r) start (reload=%r)" % (self, entry, reload))
        old_subpath = self.subpath
        sig = signature(self.entry_changed)
        nargs = len(sig.parameters)
        subpath: list[Entry] = [entry, *subsubpath]
        if reload or subpath[:nargs] != old_subpath[:nargs]:
            self._subsubpath = subsubpath
            self.entry = entry
            self.run_worker(
                partial(self._update_wrapper, entry, *subsubpath[: nargs - 1]),
                exclusive=True,  # Abort any previous run for the same widget
                group=str(id(self)),
                description=f"_update_wrapper for {type(self).__name__} {subpath}",
            )
        self.log("%r.set_entry(%r) end" % (self, entry))

    def set_error(self, e: Exception) -> None:
        self._error = e

    def reload(self):
        """
        Just redraw the widget.
        """
        # TODO Any cached data is not reloaded, can this be handled somehow?
        self.set_entry(self.entry, reload=True)

    @property
    def subpath(self) -> Sequence[Entry]:
        if self.entry:
            return [self.entry, *self._subsubpath]
        return []

    @on(Mount)
    def __on_mount(self) -> None:
        # It can happen that the widgets aren't available at this point.
        # Let's try deferring the event a little bit more...
        self.call_later(self._mounted.set)

    @property
    def mounted(self):
        return self._mounted.wait()

    async def _update_wrapper(self, entry: EntryType, *subsubpath: Entry):
        """
        Wrapper that runs the entry_changed method as soon as the widget is mounted
        Doing it earlier causes problems since the update method probably needs
        to access child widgets, that won't exist.
        """
        await self._mounted.wait()
        # try:
        logger.debug("%r._update(%r) start", self, entry)
        await self.entry_changed(entry, *subsubpath)
        logger.debug("%r._update(%r) end", self, entry)
        # except Exception as e:
        #     self.log("+++++++++++++++++++++", widget=self, error=e)
        #     # TODO not sure this is a good idea... maybe better to crash?
        #     # These exceptions could be bugs as well as "real" problems.
        #     self._error = e

    def watch__error(self, _, error: Exception) -> None:
        logger.error("%r.error: %r", self, error)
        if error:
            self.add_class("error")
        else:
            self.remove_class("error")
        try:
            self.query_one(Error).error = error
        except NoMatches:
            pass

    async def entry_changed(self, entry: EntryType) -> None:
        """
        Override this to do whatever is needed when entry changes.  You
        could override watch_entry() instead, but it's probably better
        to use entry_changed() as it will be run as a worker, and also
        exclusively.  That means it's fine even for _update to never
        exit, e.g. for updating stuff in a loop. The worker will be
        cancelled automatically, next time entry changes.
        """

    # Helpers

    # TODO there should be a way to type the return type so that
    # it also works for subclasses... only matters for type checking.
    @classmethod
    def get_widget_class(cls, entry_class: type[EntryType]) -> type[EntryWidget] | None:
        return get_widget_class(cls, entry_class)


class Error(VerticalScroll):
    """
    This widget is useful for displaying any errors that occur.
    Normally not visible, it will appear if the error property is
    set on a Details widget.
    """

    DEFAULT_CSS = """
    Error {
        dock: bottom;
        display: none;
        max-height: 8;
        overflow-y: scroll;
    }
    """

    BINDINGS = [
        ("x", "dismiss", "Dismiss"),
        ("c", "copy", "To clipboard"),
    ]

    error: reactive[Exception | None] = reactive(None, always_update=False)

    def compose(self):
        yield Static()

    def watch_error(self, _, error):
        if isinstance(error, Exception):
            self.border_title = "Exception"
            content = Pretty(error)
            self.update(content)
        else:
            self.update("")

    def update(self, content: str):
        self.query_one(Static).update(content)

    def action_dismiss(self):
        self.parent._error = None

    def action_copy(self):
        self.app.copy_to_clipboard(str(self.error))


class MonitorWidget(EntryWidget, Generic[EntryType]):
    history: bool = False
