import asyncio
from collections.abc import Callable
from textwrap import dedent

from textual.widgets import (
    TabPane,
    Static,
    Label,
    Button,
    Collapsible,
    ListView,
    RichLog,
    Header,
)
from textual.reactive import reactive
from textual.widgets._collapsible import CollapsibleTitle
from textual.containers import Grid
from textual.messages import Message
from rich.pretty import Pretty
from rich.console import RenderableType

from ..entry import Entry
from .entry import EntryWidget


class BoogieHeader(Header):
    icon: reactive[str] = reactive("🪩")


class LazyTabPane(TabPane):
    """
    A Tab that defers mounting its content widget until first shown.
    """

    def __init__(self, title: str, **kwargs):
        self.__title = title
        self._loaded = asyncio.Event()
        super().__init__(title=title, **kwargs)

    def set_entry(self, entry: Entry):
        self._entry = entry
        self._loaded.clear()
        if self.visible:
            self._load_child()

    def on_show(self):
        if not self.is_loaded:
            self._load_child()

    def _load_child(self):
        widget = self.children[0]
        widget.set_entry(self._entry)
        self._loaded.set()

    @property
    def is_loaded(self):
        return self._loaded.is_set()

    @property
    def loaded(self):
        return self._loaded.wait()


class InfoTable(Static, can_focus=False):
    """
    Table widget that shows static info from a dict.
    FIELDS can contain all fields that are interesting, but
    only the ones that exist in the given dict are displayed.
    """

    DEFAULT_CSS = """
    InfoTable {
        width: 1fr;
        height: auto;
        layout: grid;
        grid-gutter: 0 1;
        grid-size: 2;
    }
    InfoTable Label {
        text-align: right;
        color: $text-muted;
    }
    InfoTable Static {
    }
    InfoTable Input {
        border: none;
        padding: 0;
    }
    InfoTable Grid {
        height: auto;
        width: 1fr;
        grid-size: 2 1;
        grid-columns: 1fr auto;
    }
    InfoTable Input:focus {
        border: none;
        background: $background-lighten-1;
    }
    """

    FIELDS: list[tuple[str, str] | tuple[str, str, Callable]] = []

    class Changed(Message):
        def __init__(self, name, value):
            self.name = name
            self.value = value
            super().__init__()

    async def set_config(self, values):
        used_labels = set()
        await self.remove_children()
        for title, name, *rest in self.FIELDS:
            if name in values:
                value = values.get(name)
                self.mount(Label(f"{title}:", expand=True, classes=name))
                if rest:
                    (value_type,) = rest
                else:
                    value_type = None
                value_fmt = (
                    dedent(value).strip() if isinstance(value, str) else Pretty(value)
                )
                if value_type:
                    self.mount(
                        Grid(
                            Static(value_fmt, classes=name),
                            Button("Edit", id=name, classes="small"),
                        )
                    )
                else:
                    self.mount(Static(value_fmt, id=name, classes=name))
                used_labels.add(title)
            width = max(len(label) for label in used_labels)
            self.styles.grid_columns = (width + 2, "1fr")


class CollapsibleTitleStyled(CollapsibleTitle):
    def __init__(
        self,
        *,
        label: str,
        collapsed_symbol: str,
        expanded_symbol: str,
        collapsed: bool,
    ) -> None:
        Static.__init__(self, markup=True)
        self.collapsed_symbol = collapsed_symbol
        self.expanded_symbol = expanded_symbol
        self.label = label
        self.collapsed = collapsed
        self._collapsed_label = f"{collapsed_symbol} {label}"
        self._expanded_label = f"{expanded_symbol} {label}"


class StyledCollapsible(Collapsible):
    def __init__(
        self,
        *children,
        title: str = "Toggle",
        collapsed: bool = True,
        collapsed_symbol: str = "▶",
        expanded_symbol: str = "▼",
        name: str | None = None,
        id: str | None = None,
        classes: str | None = None,
        disabled: bool = False,
    ) -> None:
        super().__init__(name=name, id=id, classes=classes, disabled=disabled)
        self._title = CollapsibleTitleStyled(
            label=title,
            collapsed_symbol=collapsed_symbol,
            expanded_symbol=expanded_symbol,
            collapsed=collapsed,
        )
        self.title = title
        self._contents_list = list(children)
        self.collapsed = collapsed


class PagedListView(ListView):
    def action_page_down(self) -> None:
        """Move the cursor down a page's-worth of items."""
        # TODO 3 is just made up, find out how many children fits on a page
        if self.index is not None:
            next_index = min(self.index + 3, len(self.children) - 1)
            child = self.children[next_index]
            self.index = next_index
            self.scroll_to_widget(child)

    def action_page_up(self) -> None:
        """Move the cursor up a page's-worth of nodes."""
        if self.index is not None:
            next_index = max(self.index - 3, 0)
            child = self.children[next_index]
            self.index = next_index
            self.scroll_to_widget(child)


class PersistentLog(EntryWidget):
    """Log widget that keeps track of logs for its entries"""

    DEFAULT_CSS = """
    PersistentLog {
        RichLog {
            background: $background;
        }
        hatch: right $background-lighten-1;
    }
    """

    _log_cache: dict[Entry, list[RenderableType]] = {}

    def compose(self):
        yield RichLog(markup=True)

    async def entry_changed(self, entry):
        old_logs = self._log_cache.get(entry, [])
        log = self.query_one(RichLog)
        with self.app.batch_update():
            log.clear()
            for line in old_logs:
                log.write(line, scroll_end=False)
        log.scroll_end(animate=False)

    def write(self, message: RenderableType):
        self.query_one(RichLog).write(message)
        assert self.entry
        self._log_cache.setdefault(self.entry, []).append(message)

    def clear(self):
        self.query_one(RichLog).clear()
        assert self.entry
        self._log_cache.pop(self.entry, None)
