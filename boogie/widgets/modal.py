from importlib.resources import files
import webbrowser

from textual.app import ComposeResult
from textual.screen import ModalScreen
from textual.containers import Grid, Container
from textual.widgets import (
    Label,
    Button,
    Input,
    TextArea,
    MarkdownViewer,
    Markdown,
    Footer,
)
from textual.events import Key


class ConfirmModal(ModalScreen[bool]):
    """
    Screen with a yes/no dialog.
    """

    DEFAULT_CSS = """
    ConfirmModal {
        align: center middle;
    }
    ConfirmModal #dialog {
        grid-size: 2;
        grid-gutter: 1 2;
        grid-rows: 1fr 3;
        padding: 0 1;
        width: 60;
        height: 11;
        background: $surface;
    }
    ConfirmModal #question {
        column-span: 2;
        height: 1fr;
        width: 1fr;
        content-align: center middle;
    }
    ConfirmModal Button {
        width: 100%;
    }
    """

    def __init__(self, question, *args, **kwargs):
        self.question = question
        super().__init__(*args, **kwargs)

    def compose(self) -> ComposeResult:
        yield Grid(
            Label(self.question, id="question"),
            Button("Cancel", variant="error", id="no"),
            Button("Proceed", variant="primary", id="yes"),
            id="dialog",
            classes="frame",
        )

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "yes":
            self.dismiss(True)
        else:
            self.dismiss(False)

    def on_key(self, event: Key) -> None:
        if event.key == "escape":
            event.prevent_default()
            self.dismiss(False)


class GetValueModal(ModalScreen[str]):
    DEFAULT_CSS = """
    GetValueModal {
        align: center middle;
        #dialog {
            width: 60%;
            height: auto;
            Grid {
                margin: 0 1;
                margin-top: 1;
                grid-size: 2;
                grid-rows: auto 3;
                grid-gutter: 1 2;
                height: auto;
                Input {
                    column-span: 2;
                }
                Button {
                     width: 100%;
                }
            }
        }
    }
    """

    def __init__(
        self,
        message,
        placeholder="",
        initial=None,
        input_type="text",
        *args,
        **kwargs,
    ):
        self.message = message
        self.placeholder = placeholder
        self.initial = initial
        self.input_type = input_type
        super().__init__(*args, **kwargs)

    def compose(self) -> ComposeResult:
        with Container(id="dialog", classes="frame") as c:
            yield Grid(
                # Label(self.message, id="question"),
                Input(self.initial, placeholder=self.placeholder, type=self.input_type),
                Button("Cancel", variant="error", id="no"),
                Button("Proceed", variant="primary", id="yes"),
            )
            c.border_title = self.message

    def on_button_pressed(self, event: Button.Pressed) -> None:
        self.finish(event.button.id == "yes")

    def finish(self, submit: bool):
        if submit:
            name = self.query_one(Input).value
            self.dismiss(name)
        else:
            self.dismiss()

    def on_key(self, event: Key) -> None:
        if event.key == "escape":
            event.prevent_default()
            self.finish(False)
        if event.key == "enter":
            event.prevent_default()
            self.finish(True)


class GetTextModal(GetValueModal):
    DEFAULT_CSS = """
    GetTextModal {
        #dialog {
            width: 60%;
            Grid {
                margin: 0 1;
                margin-top: 1;
                grid-size: 2;
                grid-rows: 10 3;
                grid-gutter: 1 2;
                height: auto;
                TextArea {
                    background: black;
                    column-span: 2;
                    height: 1fr;
                }
                Button {
                     width: 100%;
                }
            }
        }
    }
    """

    def compose(self) -> ComposeResult:
        with Container(id="dialog", classes="frame") as c:
            c.border_title = self.message
            yield Grid(
                TextArea(self.initial),
                Button("Cancel", variant="error", id="no"),
                Button("Proceed", variant="primary", id="yes"),
            )

    def finish(self, submit: bool):
        if submit:
            name = self.query_one(TextArea).text
            self.dismiss(name)
        else:
            self.dismiss()


class MyMarkdownViewer(MarkdownViewer):
    """
    Markdown viewer customized to open HTTP links in a web browser (if possible).
    """

    async def _on_markdown_link_clicked(self, message: Markdown.LinkClicked) -> None:
        self.log(href=message.href)
        if message.href.startswith(("http://", "https://")):
            webbrowser.open(message.href, new=2)
            self.app.notify("Opened HTTP link in your web browser!")
            message.prevent_default()


class HelpBrowser(ModalScreen):
    BINDINGS = [
        ("escape", "close", "Close"),
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.text = files("boogie").joinpath("HELP.md").read_text()

    def compose(self) -> ComposeResult:
        yield MyMarkdownViewer(self.text, show_table_of_contents=False)
        yield Footer()

    def action_close(self):
        self.dismiss()
