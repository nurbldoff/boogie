"""Allow plotting x, y points as lines, with axes"""

from datetime import datetime, timedelta
from itertools import islice
from typing import Sequence, Tuple, Optional, Any

import numpy as np
from rich.style import Style

from .canvas import (
    BrailleCanvas,
    HorizontalBlockCanvas,
    ANSI_RESET_COLOR,
    ANSI_RESET_BACKGROUND,
)


ANSI_RESET = ANSI_RESET_COLOR + ANSI_RESET_BACKGROUND

TimeSeries = Tuple[
    Sequence[datetime],  # X values
    Sequence[float],  # Y values
    str,  # Label
    Optional[str],  # Color (rich style)
]


def get_reasonable_limits(
    a: float, b: float
) -> Tuple[Tuple[float, str], Tuple[float, str]]:
    return (round(a, 3), f"{a:.2e}"), (round(b, 3), f"{b:.2e}")


def format_legend(labels: Sequence[Tuple[str, Optional[str]]], width: int) -> list[str]:
    """
    Takes a list of labels, and formats them into a list of strings
    fitting as many labels as possible on each line. Color is applied.
    """
    lines = []
    current_line = ""
    for label, color in labels:
        if color is not None:
            style = Style(color=color)
            assert style.color is not None
            codes = style.color.get_ansi_codes()
            ansi = f"\x1b[{';'.join(codes)}m"
        else:
            ansi = ""
        if len(current_line) + len(label) < width:
            current_line += f"{ansi}{label}{ANSI_RESET_COLOR}  "
        else:
            lines.append(current_line)
            current_line = ""
    if current_line:
        lines.append(current_line)
    return lines


def get_time_ticks(
    start: datetime, end: datetime
) -> Tuple[Sequence[Tuple[datetime, str]], str]:
    """
    Return a list of reasonable axis ticks for the given range.
    A "tick" is a tuple of a datetime position and a string label.
    Also returns a rough length of the range in human language, e.g. "~4 days".
    """

    ticks = []
    delta = end - start
    dt = delta.total_seconds()
    desc = ""

    if delta.days > 60:
        t0 = datetime(start.year, start.month, start.day).astimezone(tz=None)
        increment = timedelta(days=1)
        t = t0
        while t < end:
            if t.day == 1:
                ticks.append((t, t.strftime("%b")))
            t += increment
        desc = f"~{delta.days // 30} months"

    elif delta.days > 14:
        # find first monday
        t0 = datetime(start.year, start.month, start.day).astimezone(tz=None)
        while t0.weekday() != 0:
            t0 += timedelta(days=1)
        increment = timedelta(days=7)
        t = t0
        while t < end:
            ticks.append((t, t.strftime("%b %d")))
            t += increment
        desc = f"~{delta.days // 7} weeks"

    elif delta.days > 2:
        t0 = datetime(start.year, start.month, start.day).astimezone(tz=None)
        increment = timedelta(days=1)
        t = t0 + increment
        while t < end:
            ticks.append((t, str(t.day)))
            t += increment
        desc = f"~{delta.days} days"

    elif dt > 3600 * 24:
        t0 = datetime(start.year, start.month, start.day, start.hour).astimezone(
            tz=None
        )
        increment = timedelta(hours=1)
        t = t0 + increment
        while t < end:
            if t.hour % 6 == 0:
                ticks.append((t, str(f"{t.hour:02d}:00")))
            t += increment
        desc = f"~{round(dt / 3600)} hours"

    elif dt > 3600 * 6:
        t0 = datetime(start.year, start.month, start.day, start.hour).astimezone(
            tz=None
        )
        increment = timedelta(hours=1)
        t = t0 + increment
        while t < end:
            if t.hour % 3 == 0:
                ticks.append((t, str(f"{t.hour:02d}:00")))
            t += increment
        desc = f"~{round(dt / 3600)} hours"

    elif dt > 3600:
        t0 = datetime(start.year, start.month, start.day, start.hour).astimezone(
            tz=None
        )
        increment = timedelta(hours=1)
        t = t0 + increment
        while t < end:
            ticks.append((t, str(f"{t.hour:02d}:00")))
            t += increment
        desc = f"~{round(dt / 3600)} hours"

    elif dt > 900:
        t0 = datetime(
            start.year, start.month, start.day, start.hour, (start.minute // 15) * 15
        ).astimezone(tz=None)
        increment = timedelta(minutes=15)
        t = t0 + increment
        while t < end:
            ticks.append((t, str(f"{t.hour:02d}:{t.minute:02d}")))
            t += increment
        desc = f"~{round(dt / 60)} minutes"

    elif dt > 300:
        t0 = datetime(
            start.year, start.month, start.day, start.hour, (start.minute // 5) * 5
        ).astimezone(tz=None)
        increment = timedelta(minutes=1)
        t = t0 + increment
        while t < end:
            if t.minute % 5 == 0:
                ticks.append((t, f"{t.hour:02d}:{t.minute:02d}"))
            t += increment
        desc = f"~{round(dt / 60)} minutes"

    elif dt > 60:
        t0 = datetime(
            start.year, start.month, start.day, start.hour, start.minute
        ).astimezone(tz=None)
        increment = timedelta(seconds=30)
        t = t0 + increment
        while t < end:
            ticks.append((t, f":{t.minute:02d}:{t.second:02d}"))
            t += increment
        desc = f"~{round(dt / 60)} minutes"

    else:
        t0 = datetime(
            start.year,
            start.month,
            start.day,
            start.hour,
            start.minute,
            (start.second // 10) * 10,
        ).astimezone(tz=None)
        increment = timedelta(seconds=10)
        t = t0 + increment
        while t < end:
            ticks.append((t, f":{t.minute:02d}:{t.second:02d}"))
            t += increment
        desc = f"~{round(dt)} seconds"

    return ticks, desc


def get_screen_position(scale_width: float, screen_width: int, value: float) -> int:
    return round((value / scale_width) * screen_width)


TIMESTAMP = "%Y-%m-%d %H:%M:%S"


def plot(
    data: np.ndarray,
    color="red",
    width=80,
    height=25,
    y_limits: Tuple[Optional[float], Optional[float]] = (None, None),
    lines: bool = True,
    show_x_axis=True,
    show_y_axis=False,
    show_legend=True,
) -> str:
    """Draw a number of timeseries as lines, along with X and Y axes"""

    ymin = min(data)
    ymax = max(data)

    dy = ymax - ymin or 1
    dx = len(data)

    canvas = BrailleCanvas(width, height)

    # Draw the plots
    prev_p = None
    for x, y in zip(range(len(data)), data):
        if y is None:
            prev_p = None
            continue
        cx = get_screen_position(dx, canvas.w, float(x))
        cy = get_screen_position(dy, canvas.h, ymax - float(y))
        p = (cx, cy)
        if prev_p:
            canvas.draw_line(prev_p, p, color)
        prev_p = p

    # (ymin, ymin_repr), (ymax, ymax_repr) = get_reasonable_limits(ymin, ymax)

    # Overlay the y max/min on the plot
    if data.dtype.kind in "ui":
        canvas.draw_text(0, 0, f"▲{ymax}", None)
        canvas.draw_text(0, canvas.height - 1, f"▼{ymin}", None)
    else:
        canvas.draw_text(0, 0, f"▲{ymax:.2e}", None)
        canvas.draw_text(0, canvas.height - 1, f"▼{ymin:.2e}", None)

    graph = canvas.render()

    # Put it all together
    return "\n".join(line for line in graph) + ANSI_RESET_COLOR


def time_plot(
    data: Sequence[TimeSeries],
    width=80,
    height=25,
    x_limits: Tuple[Optional[datetime], Optional[datetime]] = (None, None),
    y_limits: Tuple[Optional[float], Optional[float]] = (None, None),
    time_ticks: Tuple[Sequence[Tuple[datetime, str]], str] | None = None,
    lines: bool = True,
    show_x_axis=True,
    show_y_axis=False,
    show_legend=True,
) -> str:
    """Draw a number of timeseries as lines, along with X and Y axes"""

    xmin, xmax = x_limits
    ymin, ymax = y_limits

    xmin_set = xmin is not None
    xmax_set = xmax is not None
    ymin_set = ymin is not None
    ymax_set = ymax is not None

    prev_xys: list[tuple[datetime, float] | None] = [None] * len(data)
    first_indexes = [0] * len(data)

    for i, (xs, ys, *_) in enumerate(data):
        first_index = 0
        if xmin is not None:
            for j, (x, y) in enumerate(zip(xs, ys)):
                # Maybe do a binary search to optimize this?
                if x < xmin:
                    # Skip points that are outside of the range
                    # TODO do the same for other limits
                    prev_xys[i] = (x, y)
                    first_index = j
                    first_indexes[i] = first_index
                else:
                    break
        if first_index:
            xs = list(islice(xs, first_index, None))
            ys = list(islice(ys, first_index, None))
        if not xmax_set:
            if len(xs) == 0:
                xmax = datetime.now().astimezone(tz=None)
            else:
                if xmax is None:
                    xmax = max(xs)
                else:
                    xmax = max(xmax, max(xs))
        if not xmin_set:
            if xs:
                if xmin is None:
                    xmin = min(xs[first_index:])
                else:
                    xmin = min(xmin, min(xs))
            else:
                assert xmax is not None
                xmin = xmax - timedelta(hours=1)
        if not ymin_set:
            if ys:
                if ymin is None:
                    ymin = min(ys)
                else:
                    ymin = min(ymin, min(ys))
            else:
                ymin = -1
        if not ymax_set:
            if ys:
                if ymax is None:
                    ymax = max(ys)
                else:
                    ymax = max(ymax, max(ys))
            else:
                ymax = 1

    if xmin is None or xmax is None:
        xmax = datetime.now()
        xmin = xmax - timedelta(hours=1)
    if ymin is None or ymax is None:
        ymin = -1
        ymax = 1

    if ymin == ymax:
        # Flat line; no obvious way to get limits
        # TODO handle this some better way?
        ymin -= 1
        ymax += 1

    dx = xmax - xmin
    dy = ymax - ymin

    if show_legend:
        labels = [(label, color) for (_, _, label, color) in data]
        legend = format_legend(labels, width) if show_legend else []
    else:
        legend = []
    legend_height = len(legend) if show_legend else 0
    x_axis_height = 2 if show_x_axis else 0
    canvas = BrailleCanvas(width, height - x_axis_height - legend_height)

    canvas_w = canvas.w - show_y_axis  # Avoid losing pixels at the edges
    canvas_h = canvas.h - 1
    if not time_ticks:
        time_ticks = get_time_ticks(xmin, xmax)
    x_ticks, x_desc = time_ticks

    # x_ticks, x_desc = get_time_ticks(xmin, xmax)
    for x, tick in x_ticks:
        cx = get_screen_position(
            dx.total_seconds(), canvas_w, (x - xmin).total_seconds()
        )
        # canvas.draw_pixel(cx, 0, "rgb(100,100,100)")
        canvas.draw_pixel(cx, canvas_h, "rgb(100,100,100)")
        # canvas.draw_text(cx // 2 - len(tick) // 2, height-1, tick)

    # Draw the plots
    for i, (xs, ys, _, color) in enumerate(data):
        prev_xy = prev_xys[i]
        prev_p = None
        first_index = first_indexes[i]
        for x, y in zip(islice(xs, first_index, None), islice(ys, first_index, None)):
            # if x < xmin:
            #     # Skip points that are outside of the range
            #     # TODO do the same for other limits
            #     prev_xy = x, y
            #     continue
            if y is None:
                prev_p = None
                continue

            cx = get_screen_position(
                dx.total_seconds(), canvas_w, (x - xmin).total_seconds()
            )
            cy = get_screen_position(dy, canvas_h, ymax - float(y))
            p = (cx, cy)
            if not prev_p and prev_xy:
                # First point on screen. If there's a previous point
                # we should draw the line, though it will be only partially visible
                px, py = prev_xy
                prev_p = (
                    get_screen_position(
                        dx.total_seconds(), canvas_w, (px - xmin).total_seconds()
                    ),
                    get_screen_position(dy, canvas_h, ymax - float(py)),
                )
            if lines and prev_p:
                canvas.draw_line(prev_p, p, color)
            else:
                canvas.draw_pixel(*p, color)
            prev_p = p

    (ymin, ymin_repr), (ymax, ymax_repr) = get_reasonable_limits(ymin, ymax)

    # Overlay the y max/min on the plot
    canvas.draw_text(0, 0, f"▲ {ymax_repr}", None)
    canvas.draw_text(0, canvas.height - 1, f"▼ {ymin_repr}", None)

    graph = canvas.render()

    # Time axis
    if show_x_axis:
        x_axis = draw_time_axis([xmin, xmax], time_ticks, width)
    else:
        x_axis = ""

    # Put it all together
    y_axis = "|" if show_y_axis else ""
    return (
        "\n".join(
            [
                *legend,
                *(y_axis + line for line in graph),
                x_axis,
            ]
        )
        + ANSI_RESET_COLOR
    )


def block_category_time_plot(
    data: Sequence[TimeSeries],
    colors: dict[object, str],
    t_limits,
    width=80,
    height=1,
):
    tmin, tmax = t_limits
    dt = (tmax - tmin).total_seconds()

    canvas = HorizontalBlockCanvas(width, len(data), background=True)

    for i, (ts, vs, *_) in enumerate(data):
        if not vs:
            continue
        prev_t = ts[0]
        prev_v = vs[0]
        # Drawing the line backwards

        for t, v in zip(ts, vs):
            if t < tmin:
                prev_v = v
                continue

            if v != prev_v:
                color = colors.get(prev_v)
                prev_x = get_screen_position(
                    dt, canvas.w, (prev_t - tmin).total_seconds()
                )
                x = get_screen_position(dt, canvas.w, (t - tmin).total_seconds())
                # canvas.draw_line((x-1, i), (prev_x, i), color)
                # canvas.draw_line((prev_x + 1, i), (x, i), color)
                canvas.draw_line((prev_x, i), (x - 1, i), color)
                prev_t = t
                prev_v = v

        if prev_v == v:
            color = colors.get(v)
            prev_x = get_screen_position(dt, canvas.w, (prev_t - tmin).total_seconds())
            x = get_screen_position(dt, canvas.w, (t - tmin).total_seconds())
            # canvas.draw_line((x-1, i), (prev_x, i), color)
            # canvas.draw_line((prev_x + 1, i), (x, i), color)
            canvas.draw_line((prev_x, i), (x, i), color)

    graph = canvas.render()
    return "\n".join(graph)


def category_time_plot(
    data: Sequence[TimeSeries],
    colors: dict[Any, str],
    t_limits,
    width=80,
    height=1,
):
    tmin, tmax = t_limits
    dt = (tmax - tmin).total_seconds()

    canvas = HorizontalBlockCanvas(width, len(data), background=True)

    for i, (ts, vs, *_) in enumerate(data):
        if not vs:
            continue
        prev_t = ts[0]
        prev_v = vs[0]
        # Drawing the line backwards

        for t, v in zip(ts, vs):
            if t < tmin:
                prev_v = v
                continue

            if v != prev_v:
                color = colors.get(prev_v)
                prev_x = get_screen_position(
                    dt, canvas.w, (prev_t - tmin).total_seconds()
                )
                x = get_screen_position(dt, canvas.w, (t - tmin).total_seconds())
                # canvas.draw_line((x-1, i), (prev_x, i), color)
                # canvas.draw_line((prev_x + 1, i), (x, i), color)
                canvas.draw_line((prev_x, i), (x - 1, i), color)
                prev_t = t
                prev_v = v

        if prev_v == v:
            color = colors.get(v)
            prev_x = get_screen_position(dt, canvas.w, (prev_t - tmin).total_seconds())
            x = get_screen_position(dt, canvas.w, (t - tmin).total_seconds())
            # canvas.draw_line((x-1, i), (prev_x, i), color)
            # canvas.draw_line((prev_x + 1, i), (x, i), color)
            canvas.draw_line((prev_x, i), (x, i), color)

    graph = canvas.render()
    return "\n".join(graph)


def draw_time_axis_(canvas, xmin, xmax) -> str:
    x_ticks, x_desc = get_time_ticks(xmin, xmax)
    x_axis_ticks = ""
    x_axis_inner = ""
    dx = xmax - xmin
    if x_ticks:
        d0 = xmin
        # Draw ticks
        for tick, desc in x_ticks:
            d = tick - d0
            pos = get_screen_position(
                dx.total_seconds(), canvas.width, d.total_seconds()
            )
            x_axis_ticks += " " * (pos - len(x_axis_ticks) - (len(desc)) // 2) + desc
            x_axis_inner += " " * (pos - len(x_axis_inner)) + "╵"
        x_axis_inner += " " * (canvas.width - len(x_axis_inner))
    else:
        x_axis_inner = " " * (canvas.width)
    xmin_repr = xmin.astimezone(tz=None).strftime(TIMESTAMP)
    xmax_repr = xmax.astimezone(tz=None).strftime(TIMESTAMP)
    return (
        f"{x_axis_inner}\n"
        + x_axis_ticks
        + "\n"
        + f"◀ {xmin_repr}"
        + f"{x_desc.center(canvas.width - len(xmin_repr) - len(xmax_repr) - 4)}"
        + f"{xmax_repr} ▶"
        + ""
    )


def draw_time_axis(time_window, time_ticks, width):
    "Draw the time axis at the bottom (only visible if trends are shown)"
    canvas = BrailleCanvas(width, 2)
    x_ticks, x_desc = time_ticks
    xmin, xmax = time_window
    dx = xmax - xmin
    for x, tick in x_ticks:
        cx = get_screen_position(
            dx.total_seconds(), canvas.w, (x - xmin).total_seconds()
        )
        canvas.draw_text((cx - len(tick)) // 2, 0, tick)

    xmin_repr = xmin.astimezone(tz=None).strftime(TIMESTAMP)
    xmax_repr = xmax.astimezone(tz=None).strftime(TIMESTAMP)
    canvas.draw_text(0, 1, f"◀ {xmin_repr}")
    canvas.draw_text(canvas.width - len(xmax_repr) - 3, 1, f"{xmax_repr} ▶")
    canvas.draw_text(canvas.width // 2 - len(x_desc) // 2, 1, x_desc)
    return "\n".join(canvas.render())
    # time_axis.update(Text.from_ansi("\n".join(canvas.render())))


# if __name__ == "__main__":
#     from math import sin

#     now = datetime.now().astimezone(tz=None)

#     xs1 = [x / 10 for x in range(100)]
#     ys1 = [sin(x) for x in xs1]
#     ts1 = [now - timedelta(minutes=x) for x in xs1]

#     xs2 = [x / 10 - 2 for x in range(100)]
#     ys2 = [0.5 * sin(2*x-1) for x in xs2]
#     ts2 = [now - timedelta(minutes=x) for x in xs2]

#     print(plot([(ts1, ys1, "wave1", None), (ts2, ys2, "wave2", "magenta")]))


if __name__ == "__main__":
    now = datetime.now().astimezone(tz=None)
    line_colors = [
        "rgb(255,0,0)",
        "blue",
        "green_yellow",
        "color(1)",
        "#908020",
        "rgb(180,0,90)",
    ] * 10

    vs = list(range(len(line_colors)))
    colors = {v: c for v, c in zip(vs, line_colors)}
    ts = [now + timedelta(minutes=i) for i in range(len(colors))]
    print(
        category_time_plot([(ts, vs, "hello", None)], colors, (ts[0], ts[-1]), width=59)
    )
