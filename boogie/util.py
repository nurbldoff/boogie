from contextlib import contextmanager
from functools import cache
import hashlib
import importlib
import json
import sys
import os
import pkgutil
from traceback import format_exception
from typing import Sequence, TypeVar, get_args

import numpy
from .entry import EntryType
from . import plugins


@contextmanager
def swallow_stderr():
    """Context manager that redirects stderr and stdout somewhere else"""
    # This is a hack to get rid of some unwanted output

    stderr_fileno = sys.stderr.fileno()
    stderr_save = os.dup(stderr_fileno)
    stderr_pipe = os.pipe()
    os.dup2(stderr_pipe[1], stderr_fileno)
    os.close(stderr_pipe[1])

    yield

    os.close(stderr_pipe[0])
    os.dup2(stderr_save, stderr_fileno)
    os.close(stderr_save)


def freeze_data(raw_data: dict[str, str | list]):
    "Ensure that the given dict consists of hashable values"
    # TODO this assumes there are no other weird things in there,
    # and that the dict is flat. Hope it holds!
    return {
        key: tuple(value) if isinstance(value, list) else value
        for key, value in raw_data.items()
        if key.lower() not in {"parent", "path"}  # Collision
    }


def persistent_hash(s: str):
    return int(hashlib.sha512(s.encode("utf-8")).hexdigest(), 16)


T = TypeVar("T")


def all_subclasses(cls: type[T]) -> Sequence[type[T]]:
    """
    Recursively find all the subclasses of a given class. Sorted in reverse
    order because we want to prioritize more specialized subclasses.

    This only works as intended if all the subclasses available are imported
    already, so we have to make sure all widgets that are to be used are
    imported at startup. Plugins must take care to do that.
    """
    subclasses = []
    for subclass in cls.__subclasses__():
        subclasses.append(subclass)
        subclasses.extend(all_subclasses(subclass))
    return list(reversed(subclasses))


@cache
def get_widget_class(cls: type[T], entry_class: type[EntryType]) -> type[T] | None:
    """
    Find the proper subclass of cls to display for the given Entry
    class. Does this by inspecting the class hierarchy and matching
    on the type parameter. It will return the most "specific" matching
    widget class it finds. That means that a subclass trumps a class higher
    up in the hierarchy.
    """
    subclasses = all_subclasses(cls)
    matches = []
    for subclass in subclasses:
        if hasattr(subclass, "__orig_bases__"):
            args = get_args(subclass.__orig_bases__[0])  # type: ignore
            if args:
                widget_entry_class = args[0]  # Generic type parameter
                try:
                    if issubclass(entry_class, widget_entry_class):
                        matches.append(subclass)
                except TypeError:
                    pass
    if matches:
        return matches[0]  # TODO or -1?
    return None


class NumpyEncoderClass(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.generic):
            if numpy.isscalar(obj):
                return obj.item()
            return obj.tolist()
        return obj


def iter_namespace(ns_pkg):
    return pkgutil.iter_modules(ns_pkg.__path__, ns_pkg.__name__ + ".")


def _get_internal_plugins():
    return [
        importlib.import_module(name)
        for finder, name, ispkg in iter_namespace(plugins)
        if ispkg
    ]


def _get_external_plugins():
    """
    Check for packages named boogie_plugin_... to load.
    We rely on a simple naming convention, hoping that nobody else has
    exactly the same idea... Pretty crude, but e.g. Flask does the same.
    """
    return [
        importlib.import_module(name)
        for finder, name, ispkg in pkgutil.iter_modules()
        if name.startswith("boogie_plugin_")
    ]


def get_plugins():
    internal = _get_internal_plugins()
    try:
        external = _get_external_plugins()
    except ImportError as e:
        print("Error while loading plugins:", format_exception(e))
        sys.exit(e)
    return sorted(
        internal + external,
        key=lambda m: getattr(m, "INDEX", 100),
    )
