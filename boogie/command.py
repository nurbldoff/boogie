from __future__ import annotations
from functools import partial
from inspect import signature
from typing import TYPE_CHECKING

from rich.text import Text
from textual.command import Hit, Hits, DiscoveryHit, Provider

from .widgets.modal import ConfirmModal

if TYPE_CHECKING:
    from .app import Boogie


class BoogieCommands(Provider):
    """
    Textual command provider that looks for entry methods decorated
    with @command, or classes decorated with @command_class. These
    will be represented in the control+backslash command palette.
    """

    app: Boogie

    async def discover(self):
        for func, name, *args in COMMAND_FUNCTIONS:
            yield DiscoveryHit(
                f"{self.app.entry}: [b]{name or func.__name__}",
                partial(self._run, func, *args),
                help=func.__doc__,
            )

        async def run_class_command(cls):
            if await cls.check(self.app, self.app.entry):
                await cls.run(self.app, self.app.entry)

        for cls in COMMAND_CLASSES:
            yield DiscoveryHit(
                cls.name, partial(run_class_command, cls()), help=cls.__doc__
            )

    async def _run(self, command, confirm, notify, reload):
        app = self.app
        entry = app.entry

        async def maybe_run(doit):
            if doit:
                method = getattr(entry, command.__name__)
                await app.run_entry_method(entry, method)
                if reload:
                    await app.screen.reload(quiet=True)
                if notify:
                    app.notify(notify.format(entry=entry))

        if not confirm:
            maybe_run(True)
        else:
            app.push_screen(ConfirmModal(confirm.format(entry=entry)), maybe_run)

    async def search(self, query: str) -> Hits:
        """Search for Python files."""
        matcher = self.matcher(query)

        def run_class_command(cls):
            if cls.check(self.app, self.app.entry):
                cls.run(self.app, self.app.entry)

        for func, name, *args in COMMAND_FUNCTIONS:
            score = matcher.match(func.__name__)
            if score > 0:
                yield Hit(
                    score,
                    Text.assemble(
                        matcher.highlight(name or func.__name__),
                        (f" {self.app.entry}", "bold"),
                    ),
                    partial(self._run, command, *args),
                    help=func.__doc__,
                )

        for cls in COMMAND_CLASSES:
            score = matcher.match(cls.name)
            if score > 0:
                yield Hit(
                    score,
                    matcher.highlight(
                        f"{self.app.entry}: {cls.__name__}",
                    ),
                    partial(run_class_command, cls()),
                    help=cls.__doc__,
                )


COMMAND_FUNCTIONS = []


def command(
    name: str | None = None,
    confirm: str | None = None,
    notify: str | None = None,
    reload: bool = False,
):
    """
    Simple decorator to mark a method as a 'command'.
    """

    def func_wrapper(func):
        sig = signature(func)
        assert len(sig.parameters) == 1, "Command methods can't take arguments"
        COMMAND_FUNCTIONS.append((func, name, confirm, notify, reload))
        return func

    return func_wrapper


COMMAND_CLASSES = []


def command_class():
    """
    Decorator to mark a class as a 'command'.
    The class should have a "check" method that returns a bool
    and a run method that performs the command.
    """

    def class_wrapper(cls):
        COMMAND_CLASSES.append(cls)
        return cls

    return class_wrapper
