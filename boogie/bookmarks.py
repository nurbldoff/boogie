from datetime import datetime, timezone
import json
import logging
import sqlite3
from typing import Sequence, Any

from textual import on
from textual.app import ComposeResult
from textual.screen import ModalScreen
from textual.widgets import DataTable, Footer, Input, TextArea, Button, Label, Markdown
from textual.containers import Vertical, Grid, Horizontal, VerticalScroll
from textual.reactive import var

from boogie.widgets.modal import ConfirmModal
from .widgets.misc import BoogieHeader


logger = logging.getLogger(__name__)


class Bookmarks:
    """
    Stores bookmarks in a sqlite database.

    A "bookmark" really consists of all "items" in the database that
    point to a particular path. There may be several, each has a
    timestamp and optionally, a text note. The "bookmark" is the sum
    of these items. Its timestamp in the list is the latest timestamp.
    The idea is that it can be useful to have a history of notes on
    a given bookmark, with timestamps.

    Not sure if this makes sense, maybe it's just over complicated and
    we should stick with one item per bookmark...

    Missing features:
    - Removing individual notes
    - Changing the path of a bookmark (e.g. device renamed)
    """

    def __init__(self, db_file):
        self.conn = sqlite3.connect(db_file)
        self.conn.row_factory = sqlite3.Row
        self.conn.execute("PRAGMA journal_mode = wal;")
        self.conn.execute("PRAGMA synchronous = NORMAL;")
        self.conn.execute("PRAGMA cache_size = -20000;")
        self.conn.execute("PRAGMA temp_store = MEMORY;")
        cursor = self.conn.cursor()
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS bookmarks (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                timestamp TEXT DEFAULT CURRENT_TIMESTAMP,
                key TEXT NOT NULL,
                path TEXT,
                data TEXT
            )
            """
        )
        try:
            cursor.execute(
                """
                CREATE UNIQUE INDEX bookmarks_id_key_path
                    ON bookmarks(id, key, path) ORDER BY id DESC
                CREATE UNIQUE INDEX bookmarks_id_time_key
                    ON bookmarks(id, timestamp, key) ORDER BY id DESC
                """
            )
        except sqlite3.OperationalError:
            # This happens if the indices already exist, which is OK.
            # TODO what else can go wrong here?
            pass
        self.conn.commit()
        self._path_id_cache = {}

    def append(self, key: str, path: Sequence[str], data=None):
        """
        Add a bookmark
        The bookmark system is "append only" so adding a new bookmark will
        basically just add a new note.
        """
        cursor = self.conn.cursor()
        if data is not None:
            try:
                data = json.dumps(data)
            except TypeError as e:
                print(f"Failed to JSON encode arguments: {e}")
                data = "{}"
        cursor.execute(
            "INSERT INTO bookmarks (key, path, data) VALUES (?, ?, ?)",
            (key, ":".join(path), data),
        )
        self.conn.commit()
        self._path_id_cache.clear()
        logger.info(f"Added bookmark {key} {path}")

    def delete(self, key: str, path: str):
        cursor = self.conn.cursor()
        cursor.execute(
            "DELETE FROM bookmarks WHERE key = ? AND path = ?",
            (key, path),
        )
        self.conn.commit()
        self._path_id_cache.clear()

    def get_id_by_path(self, key: str, path: str) -> int | None:
        """
        This method is used a lot just to check if a path is bookmarked.
        Since bookmarks don't change all the time, let's cache them.
        Just remember that we must invalidate the cache when they do change!
        """
        if not self._path_id_cache:
            # Fill cache
            items = self.items(key, limit=10000)
            for id_, _, path_, *_ in items:
                self._path_id_cache[key, path_] = id_
        cache_key = (key, path)
        return self._path_id_cache.get(cache_key)

    def items(
        self,
        key: str | None = None,
        path: str | None = None,
        note: str | None = None,
        offset: int = 0,
        limit: int = 100,
    ):
        """Get some path entries"""
        print(f"Getting bookmark items {offset} to {offset + limit} ({path}, {note})")
        cursor = self.conn.cursor()
        query: tuple[str] | tuple[str, Sequence[Any]]
        path = "%" if not path else f"%{path.replace('*', '%')}%"
        if key:
            sql = f"""
                SELECT MAX(id) as latest_id, GROUP_CONCAT(timestamp, '|'), path,
                    '[' || GROUP_CONCAT(COALESCE(data, '{{}}'), ',') || ']'
                FROM bookmarks
                WHERE key = ?
                  AND path LIKE ?
                  {"AND json_extract(data,'$.note') LIKE ?" if note else ""}
                GROUP BY path
                ORDER BY latest_id DESC
                LIMIT {limit}
                OFFSET {offset}
                """
            if note:
                query = (sql, (key, path or "%", note or "%"))
            else:
                query = (sql, (key, path or "%"))
        else:
            query = (
                f"""
                SELECT MAX(id) as latest_id, GROUP_CONCAT(timestamp, '|'), path,
                    '[' || GROUP_CONCAT(COALESCE(data, '{{}}'), ',') || ']'
                FROM bookmarks
                WHERE path LIKE ? AND json_extract(data,'$.note') LIKE ?
                GROUP BY path
                ORDER BY latest_id DESC
                LIMIT {limit}
                OFFSET {offset}
                """,
                (path or "%", note or "%"),
            )
        for row in cursor.execute(*query):
            id_, timestamps, path, datas = row
            yield (id_, timestamps.split("|"), path, json.loads(datas))

    def get_item(self, id_):
        "Get a particular entry"
        cursor = self.conn.cursor()
        *rest, data = next(
            cursor.execute(
                """
                SELECT id, key, timestamp, path, data
                FROM bookmarks
                WHERE id = ?
                ORDER BY id DESC
                LIMIT 1
                """,
                (id_,),
            )
        )
        data = json.loads(data) if data else {}
        return (*rest, data)

    def get_path(self, key, path):
        "Get a particular entry"
        cursor = self.conn.cursor()
        ids, timestamps, datas = next(
            cursor.execute(
                """
                SELECT
                COALESCE(GROUP_CONCAT(id, '|'), ''),
                COALESCE(GROUP_CONCAT(timestamp, '|'), ''),
                '[' || GROUP_CONCAT(COALESCE(data, '{}'), ',') || ']'
                FROM bookmarks
                WHERE key = ? AND path LIKE ?
                ORDER BY timestamp DESC
                """,
                (
                    key,
                    path,
                ),
            )
        )
        return sorted(
            zip(
                ids.split("|"),
                timestamps.split("|"),
                json.loads(datas) if datas else {},
            ),
            key=lambda item: item[0],
        )


class BookmarksBrowser(ModalScreen):
    DEFAULT_CSS = """
    BookmarksBrowser {
        hatch: right $background-lighten-1;
    }
    BookmarksBrowser Grid {
        grid-size: 2;
        height: auto;
    }
    BookmarksBrowser Vertical {
    }
    BookmarksBrowser DataTable {
        height: 1fr;
    }
    """

    BINDINGS = [
        ("escape", "close", "Close"),
        ("m", "load_more", "Load more"),
        ("e", "edit", "Edit"),
        ("delete", "delete", "Delete"),
    ]

    path_filter: var[str | None] = var(None, init=False)
    note_filter: var[str | None] = var(None, init=False)

    def __init__(self, bookmarks: Bookmarks, key: str, *args, **kwargs):
        self.bookmarks = bookmarks
        self.key = key
        self._offset = 0
        self._limit = 100
        super().__init__(*args, **kwargs)

    def compose(self) -> ComposeResult:
        self.title = "Bookmarks"
        self.sub_title = self.key
        yield BoogieHeader()
        filters = Grid(id="filters")
        with filters:
            yield Input(id="path-filter", placeholder="Path filter")
            yield Input(id="note-filter", placeholder="Note filter")
        vertical = Vertical()
        with vertical:
            dt: DataTable[tuple[str, str, str]] = DataTable()
            dt.add_columns("Timestamp", "Path", "Note")
            dt.cursor_type = "row"
            dt.zebra_stripes = True
            yield dt
        yield Footer()

    def on_mount(self):
        # TODO be more clever about this. Maybe some kind of
        # lazy loading?
        self.load()

    def watch_path_filter(self, _, __):
        self.load(restart=True)

    def watch_note_filter(self, _, __):
        self.load(restart=True)

    def load(self, restart=False):
        data_table = self.query_one(DataTable)
        if restart:
            self._offset = 0
            data_table.clear()

        bookmarks = self.bookmarks.items(
            self.key,
            path=self.path_filter,
            note=f"%{self.note_filter}%" if self.note_filter else None,
            offset=self._offset,
            limit=self._limit,
        )
        data_table.fixed_columns = 1
        i = 0
        for i, (id_, times, path, datas) in enumerate(bookmarks):
            time = times[-1]
            timestamp = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
            timestamp = timestamp.replace(tzinfo=timezone.utc).astimezone()
            data = datas[-1] if datas else {}
            note = data.get("note", "")
            path = path.replace(":", " ")  # TODO something more readable?
            data_table.add_row(
                timestamp.strftime("%Y-%m-%d %H:%M:%S"), path, note, key=str(id_)
            )
        # TODO if i < self._history_limit, we reached end of history.
        # Perhaps signal this somehow, e.g. disable the "more" action?
        self._offset += self._limit
        data_table.focus()

    def on_data_table_row_selected(self, message):
        bookmark_id = int(message.row_key.value)
        id_, _, timestamp, path, _ = self.bookmarks.get_item(bookmark_id)
        self.dismiss((id_, path.split(":")))

    def action_close(self):
        self.dismiss()

    def action_load_more(self):
        data_table = self.query_one(DataTable)
        data_table.fixed_columns = 1
        offset = self._offset
        self.load()
        data_table.move_cursor(row=offset)

    def on_input_submitted(self, message):
        value = message.value
        if message.control.id == "path-filter":
            if value:
                self.path_filter = message.value
            else:
                self.path_filter = None
        elif message.control.id == "note-filter":
            if value:
                self.note_filter = value
            else:
                self.note_filter = None

    async def action_edit(self):
        table = self.query_one(DataTable)
        row_key, _ = table.coordinate_to_cell_key((table.cursor_row, 0))
        bookmark_id = int(row_key.value)
        _, _, timestamp, path, _ = self.bookmarks.get_item(bookmark_id)
        items = self.bookmarks.get_path(self.key, path)

        async def maybe_save(config):
            if config:
                key, id_, note = config
                self.bookmarks.append(key, path.split(":"), {"note": note})
            else:
                self.notify("Cancelled edit")

        self.app.push_screen(
            BookmarkModal(self.key, bookmark_id, path, items), maybe_save
        )

    def action_delete(self):
        """
        Delete the current row bookmark.
        """

        def maybe_delete(doit):
            if doit:
                table = self.query_one(DataTable)
                row_key, _ = table.coordinate_to_cell_key((table.cursor_row, 0))
                bookmark_id = int(row_key.value)
                _, _, _, path, _ = self.bookmarks.get_item(bookmark_id)
                self.bookmarks.delete(self.key, path)
                table.remove_row(row_key)

        self.app.push_screen(ConfirmModal("Really delete bookmark?"), maybe_delete)


class BookmarkModal(ModalScreen[tuple[str, str, str] | None]):
    DEFAULT_CSS = """
    BookmarkModal {
        align: center middle;
        .frame {
            height: 80%;
            width: 80%;
        }
        Horizontal {
            height: auto;
        }
        #path {
            padding: 1;
        }
        #note {
            border: heavy $primary-background;
            height: 1fr;
            min-height: 10;
        }
        #note:focus-within {
            border: heavy $accent;
        }
        #buttons {
            height: 3;
            dock: bottom;
        }
        BookmarkHistory {
            max-height: 50%;
        }
        Button {
             width: 1fr;
        }
    }
    """

    BINDINGS = [
        ("ctrl+s", "save", "Save"),
        ("escape", "cancel", "Cancel"),
    ]

    BORDER_TITLE = "Bookmark"

    def __init__(self, key, bookmark_id, path, items, *args, **kwargs):
        self._key = key
        self._path = path
        self._bookmark_id = bookmark_id
        self._items = items
        super().__init__(*args, **kwargs)

    def compose(self):
        with Vertical(classes="frame") as v:
            v.border_title = "Bookmark - " + "/".join(
                f"[b]{p}[/]" for p in self._path.split(":")
            )
            with Vertical(id="note") as notes:
                notes.border_title = "New note"
                notes.border_subtitle = "Markdown"
                text_area = TextArea.code_editor(tab_behavior="focus")
                text_area.language = "markdown"
                yield text_area
            yield BookmarkHistory(self._items)
            with Horizontal(id="buttons"):
                yield Button("Save (Ctrl+s)", id="save", variant="primary")
                yield Button("Cancel (Esc)", id="cancel", variant="warning")

    @on(Button.Pressed, "#save")
    def save(self):
        self._save()

    def _save(self):
        note = self.query_one(TextArea).text
        self.dismiss((self._key, self._bookmark_id, note))

    @on(Button.Pressed, "#cancel")
    def cancel(self):
        self.dismiss(None)

    def action_save(self):
        self._save()

    def action_cancel(self):
        self.dismiss(None)


class BookmarkHistory(VerticalScroll):
    DEFAULT_CSS = """
    BookmarkHistory {
        height: auto;
        .item {
            height: auto;
            hatch: right $background-lighten-2;
            grid-size: 2 2;
            grid-columns: 1fr auto;
            grid-rows: 1 auto;
            .note {
                column-span: 2;
            }
        }
        .timestamp {
            text-style: bold;
            color: $secondary;
            height: 1;
        }
        .note {
            height: auto;
            margin: 0;
        }
    }
    """

    def __init__(self, items, *args, **kwargs):
        self._items = items
        super().__init__(*args, **kwargs)

    def compose(self):
        for id_, time, data in reversed(self._items):
            if data.get("note"):
                with Grid(classes="item"):
                    yield Label(time, classes="timestamp")
                    yield Button("Delete", variant="error", classes="small")
                    yield Markdown(data["note"], classes="note")
