from __future__ import annotations
from argparse import ArgumentParser
import logging
import os
import time
from typing import Callable, Sequence, Any

from platformdirs import user_data_dir
from textual import on, work
from textual.app import App, ComposeResult
from textual.screen import Screen
from textual.widgets import TabbedContent, TabPane, Tabs, Footer
from textual.logging import TextualHandler
from textual.binding import Binding
from textual.events import Key
from textual.css.query import NoMatches

from .util import get_plugins
from .entry import Entry, get_entry
from .messages import NavigateTo, Reload
from .browser import Browser
from .history import History, HistoryBrowser
from .bookmarks import Bookmarks, BookmarksBrowser, BookmarkModal
from .widgets.modal import HelpBrowser
from .widgets.misc import BoogieHeader
from .monitor import Monitors, MonitorScreen
from .path import Path
from .cache import clear_cache
from .command import BoogieCommands

try:
    from ._version import version
except ImportError:
    version = "?.?"


class Boogie(App):
    """
    The top level application object. Just loads and manages some screens.
    """

    TITLE = f"Boogie v{version}"

    BINDINGS = [
        Binding("ctrl+c", "", "Exit", show=False),
        Binding("f10", "app.quit", "Exit", show=False),
        # Binding("space", "command_palette", show=False, priority=True),
        # Binding("ctrl+h", "toggle_help_panel", "Toggle help", show=False),
    ]

    # COMMANDS = set()  # Disable default commands, they aren't useful

    def __init__(
        self,
        args=None,
        plugins=None,
        history_db_path: str | None = None,
        bookmarks_db_path: str | None = None,
    ):
        super().__init__()

        self.initial_path = args.path if args else None
        data_dir = user_data_dir("Boogie")
        os.makedirs(data_dir, exist_ok=True)
        history_db_path = history_db_path or os.path.join(data_dir, "history.db")
        bookmarks_db_path = bookmarks_db_path or os.path.join(data_dir, "bookmarks.db")
        self.history = History(history_db_path)
        # TODO this might better live in the History class
        self._current_history_id: int | None = None
        self.bookmarks = Bookmarks(bookmarks_db_path)

        monitors_db_path = os.path.join(data_dir, "monitors.db")
        self.monitors = Monitors(monitors_db_path)

        # TODO loading plugins a second time here
        self._plugins = plugins or get_plugins()
        # self.log("Found plugins", plugins=[plugin.__name__ for plugin in self._plugins])

        self._plugin_roots = {
            info["root"].name.lower(): info
            for plugin in self._plugins
            for info in getattr(plugin, "BROWSERS", [])
        }

    def on_mount(self) -> None:
        # Plugins may specify custom CSS.
        # TODO this means that plugin CSS could collide, unless carefully name-spaced.
        # For now there's no way to load CSS more dynamically.

        plugin_css_paths = sum(
            [
                [
                    os.path.join(plugin.__path__[0], css_path)
                    for css_path in getattr(plugin, "EXTRA_CSS", [])
                ]
                for plugin in self._plugins
            ],
            [],
        )

        MainScreen.CSS_PATH = [
            *plugin_css_paths,
            "main.tcss",
        ]

        MainScreen.COMMANDS = set.union(
            MainScreen.COMMANDS,
            *(
                plugin.COMMAND_PROVIDERS
                for plugin in self._plugins
                if hasattr(plugin, "COMMAND_PROVIDERS")
            ),
        )

        # TODO create this on demand?

        main_screen = MainScreen(self.initial_path)
        self.install_screen(main_screen, "main")
        self.push_screen("main")

    def get_bookmark(self, path: str) -> int | None:
        key = self.get_key()
        return self.bookmarks.get_id_by_path(key, path)

    async def get_entry(self, path_str: Sequence[str]) -> Entry:
        "Return the Entry corresponding to a path"
        root_name = path_str[0].lower()
        try:
            root = self._plugin_roots[root_name]["root"]
        except KeyError:
            msg = f"Could not find root '{root_name}'; missing plugin?"
            self.log(msg)
            raise KeyError(msg)
        entry, error = await get_entry(root, path_str)
        if error:
            self.log(f"Error finding entry {path_str}", error=error)
            self.notify(f"Error: {error}", severity="error")
        return entry

    async def on_navigate_to(self, message: NavigateTo) -> None:
        await self.navigate_to(message.entry, store=message.store)

    def get_current_browser(self):
        screen = self.get_screen("main")
        tabbed = screen.query_one("#root-tabs", expect_type=TabbedContent)
        pane = tabbed.get_pane(tabbed.active)
        browser = pane.query_one(Browser)
        return browser

    @property
    def entry(self):
        browser = self.get_current_browser()
        return browser.entry

    def get_key(self):
        screen = self.get_screen("main")
        # assert screen.sub_title
        return screen.sub_title

    async def navigate_to(self, entry: Entry, store=True) -> None:
        "Direct the application to the given location (entry)"
        self.log("Navigating to", entry=entry, store=store)
        root = entry.path[0]
        screen = self.get_screen("main")
        self.switch_screen("main")
        tabbed = screen.query_one("#root-tabs", expect_type=TabbedContent)
        tab_id = root.name.lower()
        tabbed.active = tab_id
        pane = tabbed.get_pane(tab_id)
        await screen._load_pane(pane)  # type: ignore  # TODO
        browser = pane.query_one(Browser)
        await browser.mounted
        browser.set_entry(entry)
        if store:
            self.update_history(entry, self.get_key())

    async def action_go_to(self, path_str: str) -> None:
        "Handle clicked links"
        # Links can occur in any widget text, and are specified like this:
        # [@click=screen.go_to('server:dserver:TangoTest')]Linky link[/]
        path = path_str.split(":")
        entry = await self.get_entry(path)
        await self.navigate_to(entry)

    def action_toggle_help_panel(self):
        from textual.widgets import HelpPanel

        try:
            self.query_one(HelpPanel)
        except NoMatches:
            self.mount(HelpPanel())
        else:
            self.query_one(HelpPanel).remove()

    @work
    async def update_history(self, entry: Entry, key: str) -> None:
        "Store a path in the history database"
        path_str = [str(entry) for entry in entry.path]
        id_ = self.history.append(key, path_str, "path")
        self._current_history_id = id_

    async def create_bookmark(self, entry) -> None:
        path_str = [str(entry) for entry in entry.path]
        key = self.get_key()
        if key:
            self.bookmarks.append(key=key, path=path_str)
            # Reload, so that bookmark icons are displayed
            # TODO Guess this could be done in a more efficient way...
            browser = self.get_current_browser()
            browser.query_one("#tree").reload()
            self.notify(f"Created bookmark in {key}")

    async def create_bookmark_with_note(self, entry) -> None:
        key = self.get_key()
        path = entry.path_str()

        async def maybe_create(config):
            if config:
                *_, note = config
                if note:
                    data = {"note": note}
                else:
                    data = None
                path_str = [str(entry) for entry in entry.path]
                self.bookmarks.append(key=key, path=path_str, data=data)
                browser = self.get_current_browser()
                browser.query_one("#tree").reload()
                self.notify(f"Created bookmark in {key}")

        items = self.bookmarks.get_path(key, path)

        self.app.push_screen(BookmarkModal(key, None, path, items), maybe_create)

    async def run_entry_method(self, entry: Entry, method: Callable, **kwargs):
        """
        Run a method on an entry.
        Doing it this way causes it to leave a record in the
        history, so it's probably a good idea for methods that modify
        things. Otherwise it should be the same as running it directly.

        Note that the cmd is assumed to be async.
        TODO maybe better to come up with a more automatic way of handling
        this, like a decorator on methods.
        """
        name = method.__name__
        assert name != "path", f"Sorry, '{name}' is not an allowed method name"
        self.log("Running entry method", path=entry.path, name=name, kwargs=kwargs)
        path_str = [str(entry) for entry in entry.path]
        t0 = time.time()
        result = await method(**kwargs)
        dt = time.time() - t0
        kwargs["duration"] = round(dt, 6)
        key = self.get_key()
        assert key
        self.history.append(key=key, path=path_str, action=name, data=kwargs)
        return result

    def copy_to_clipboard(
        self, text: str, message: str = "Copied to clipboard"
    ) -> None:
        super().copy_to_clipboard(text)
        self.notify(message)

    def _create_monitor_screen(self):
        monitor_screen = MonitorScreen(self.monitors, key=self.get_key())
        self.install_screen(monitor_screen, "monitor")
        self.push_screen("monitor")
        # Immediately switch back; maybe there's a way to create a new screen
        # without switching to it?
        self.switch_screen("main")
        return monitor_screen

    def add_to_monitor(self, entry: Entry, show: bool = False) -> None:
        "Add an entry to the monitor screen."
        try:
            monitor_screen = self.app.get_screen("monitor")
        except KeyError:
            monitor_screen = self._create_monitor_screen()
        # Do this a little later, to ensure that the screen is finished
        self.call_later(self._add_to_monitor, entry, show)

    def _add_to_monitor(self, entry: Entry, show: book = False):
        monitor_screen = self.app.get_screen("monitor")
        monitor_screen.add(entry)
        if show:
            self.switch_screen("monitor")


class MainScreen(Screen):
    """
    "Main" screen, housing the plugin tabs, handling global key bindings,
    and generally wiring things together.
    """

    BINDINGS = [
        Binding("f1", "help", "Help"),
        Binding("f2", "show_history", "History"),
        Binding("f3", "show_bookmarks", "Bookmarks"),
        Binding("f4", "show_monitor", "Monitor"),
        Binding("f5", "reload", "Reload"),
        Binding("ctrl+left", "go_back", "Go back", show=False),
        Binding("ctrl+right", "go_forward", "Go forward", show=False),
        Binding("ctrl+up", "go_up", "Go up", show=False),
        Binding("alt+left", "go_back", "Go back", show=False),
        Binding("alt+right", "go_forward", "Go forward", show=False),
        Binding("alt+up", "go_up", "Go up", show=False),
        Binding("f11", "maximize", "Maximize", show=False),
        Binding("escape", "minimize", "Minimize", show=False),
    ]

    COMMANDS = {BoogieCommands}

    app: Boogie

    def __init__(
        self, path: Sequence[str] | None = None, plugins=None, *args, **kwargs
    ):
        self._path = path
        super().__init__(*args, **kwargs)

    def compose(self) -> ComposeResult:
        yield BoogieHeader()
        initial_tab = self._path[0].lower() if self._path else ""
        with TabbedContent(id="root-tabs", initial=initial_tab):
            for index, (name, info) in enumerate(
                self.app._plugin_roots.items(), start=1
            ):
                get_title = info.get("get_title")
                search = info.get("search")
                root = info["root"]
                yield TabPane(
                    f"[bold #ffaa00]{index}[/] {root.name}",
                    Browser(get_title, self.app.get_bookmark, search, id=name.lower()),
                    classes="root-tabs",
                    id=name.lower(),
                )
        footer = Footer(show_command_palette=False)
        yield footer

    async def on_mount(self) -> None:
        # Prevent from focusing the main tab bar
        self.query_one(Tabs).can_focus = False

        if self._path:
            self.log("Going to path", path=self._path)
            entry = await self.app.get_entry(self._path)

            self.app.call_after_refresh(self.post_message, NavigateTo(entry))

    @property
    async def active_browser(self) -> Browser | None:
        tabbed: TabbedContent = self.query_one("#root-tabs", expect_type=TabbedContent)
        pane = tabbed.active_pane
        if pane:
            browser = pane.query_one(Browser)
            return browser
        return None

    @on(TabbedContent.TabActivated, "#root-tabs")
    async def tab_changed(self, message: Tabs.TabActivated) -> None:
        tabbed: TabbedContent = self.query_one("#root-tabs", expect_type=TabbedContent)
        pane = tabbed.active_pane
        await self._load_pane(pane)
        self._focus_pane(pane)

    async def _load_pane(self, pane) -> None:
        "Make sure the given pane is 'loaded', i.e. provided with an entry"
        browser = pane.query_one(Browser)
        if not browser.entry:
            root_class = self.app._plugin_roots[browser.id]["root"]
            browser.set_entry(root_class())
            # TODO this is a way to avoid a "race condition" at the initial
            # entry setting... Find a better way!
            await browser.rooted.wait()

    def _focus_pane(self, pane) -> None:
        # TODO this is a hack, to ensure that we have focus somewhere after
        # switching tabs. Would be better to find a way to just restore the
        # previous focus.
        browser = pane.query_one(Browser)
        browser.focus()
        self.focus_next()

    def set_sub_title(self, title) -> None:
        # # Try to find a "unique" color for the title, to make it more obvious
        # # which control system you are connected to.
        # h = persistent_hash(title[: len(title) // 2]) % 100
        # s = persistent_hash(title[len(title) // 2:]) % 100
        # color = Color.from_hsl(h / 100, 0.5 + s / 200, 0.5)
        # self.query_one("HeaderTitle").styles.color = "#77aaff"
        self.sub_title = title

    async def action_reload(self) -> None:
        await self.reload()

    def action_maximize(self):
        if self.maximized:
            self.minimize()
        else:
            super().action_maximize()
        
    def action_minimize(self):
        if self.maximized:
            self.minimize()
        
    async def on_reload(self, message: Reload) -> None:
        await self.reload()

    async def reload(self, quiet=False) -> None:
        "Clear cache and refresh all widgets"

        clear_cache()
        # TODO reload all tabs?
        # TODO do we need to clean up the old listener?
        browser = await self.active_browser
        if browser:
            await browser.reload()
            if not quiet:
                self.app.notify("Reloaded")

    @on(Path.NavigateBack)
    async def action_go_back(self) -> None:
        "Step back in history"
        self.log("Action go back", history_id=self.app._current_history_id)
        # This is a bit misplaced... probably this belongs in the app
        try:
            key = self.app.get_key()
            assert key
            history_id, path = self.app.history.get_previous(
                key, self.app._current_history_id
            )
        except (IndexError, AssertionError) as e:
            self.notify(f"Sorry, failed to navigate: {e}", severity="warning")
            return
        try:
            entry = await self.app.get_entry(path)
        except KeyError as ke:
            self.notify(str(ke))
            return
        self.log("Going back to", entry=entry)
        if entry:
            self.post_message(NavigateTo(entry, store=False))
            self.app._current_history_id = history_id
        else:
            self.notify(f"No entry found for {path}..?")

    @on(Path.NavigateForward)
    async def action_go_forward(self) -> None:
        "Step forward in history"
        try:
            key = self.app.get_key()
            assert key
            history_id, path = self.app.history.get_next(
                key, self.app._current_history_id
            )
        except IndexError:
            return
        entry = await self.app.get_entry(path)
        if entry:
            self.post_message(NavigateTo(entry, store=False))
            self.app._current_history_id = history_id

    @on(Path.NavigateUp)
    async def action_go_up(self) -> None:
        "Go to parent"
        browser = await self.active_browser
        if browser and browser.entry and browser.entry.parent:
            self.post_message(NavigateTo(browser.entry.parent, store=False))

    def action_help(self) -> None:
        self.app.push_screen(HelpBrowser())

    def action_show_history(self) -> None:
        "Display the history browser widget"

        async def go_to_path(result=None) -> None:
            "Handle result from the history browser"
            if result:
                history_id, path = result
                entry = await self.app.get_entry(path)
                self.post_message(NavigateTo(entry, store=False))
                self.app._current_history_id = history_id

        if self.sub_title is not None:
            self.app.push_screen(
                HistoryBrowser(
                    self.app.history, self.app.get_key(), self.app._current_history_id
                ),
                go_to_path,
            )
        else:
            self.notify("Sorry, not connected...", severity="error")

    def action_show_bookmarks(self) -> None:
        "Display the bookmark browser widget"

        async def go_to_path(result: tuple | None = None) -> None:
            "Handle result from the bookmark browser"
            if result:
                id_, path = result
                try:
                    entry = await self.app.get_entry(path)
                    self.post_message(NavigateTo(entry))
                except KeyError as e:
                    self.notify(str(e))

        if self.sub_title is not None:
            self.app.push_screen(
                BookmarksBrowser(self.app.bookmarks, self.app.get_key()), go_to_path
            )
        else:
            self.notify("Sorry, not connected...", severity="error")

    def action_show_monitor(self):
        try:
            self.app.get_screen("monitor")
        except KeyError:
            self.app._create_monitor_screen()
        self.app.switch_screen("monitor")

    async def on_key(self, event: Key) -> None:
        if event.key.isnumeric():
            # Switch to top level tab using number keys
            tab_index = int(event.key) - 1
            try:
                tab_id = list(self.app._plugin_roots.keys())[tab_index]
            except IndexError:
                return
            tabbed: TabbedContent = self.query_one(
                "#root-tabs", expect_type=TabbedContent
            )
            tabbed.active = tab_id


def get_app(parser: ArgumentParser) -> tuple[Boogie, dict[str, Any]]:
    def colon_separated(s):
        return s.split(":")

    parser.add_argument(
        "-p",
        "--path",
        type=colon_separated,
        nargs="?",
        help="Starting path. Colon separated, e.g. 'Device:sys:tg_test:1'",
    )
    parser.add_argument(
        "-n",
        "--no-swallow",
        action="store_true",
        help="Don't eat stderr output. Only useful for debugging",
    )
    parser.add_argument(
        "-d", "--debug", action="store_true", help="More verbose logging"
    )
    parser.set_defaults(app=Boogie)
    subparsers = parser.add_subparsers(dest="subparser")

    # Add plugin scripts
    for plugin in get_plugins():
        if hasattr(plugin, "SCRIPTS"):
            for name, module in plugin.SCRIPTS.items():
                subparser = subparsers.add_parser(name)
                subparser.set_defaults(app=module.get_app(subparser))

    args = parser.parse_args()

    logging.basicConfig(
        level=logging.DEBUG if args.debug else logging.INFO,
        format="%(asctime)s %(levelname)s %(name)s %(message)s",
        handlers=[
            TextualHandler(),
        ],
    )

    return args.app(args), dict(inline=getattr(args, "inline", False))


def main() -> None:
    parser = ArgumentParser()
    app, kwargs = get_app(parser)
    app.run(**kwargs)
