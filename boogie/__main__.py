from argparse import ArgumentParser

from .app import get_app


parser = ArgumentParser()
app, kwargs = get_app(parser)
app.run(**kwargs)
