from typing import Sequence

from textual import on
from textual.app import ComposeResult
from textual.widgets import Static, Label, Button
from textual.message import Message
from textual.reactive import reactive

from .entry import Entry


class PathStep(Static):
    DEFAULT_CSS = """
    PathStep {
        height: 1;
        width: auto;
        border: none;
        background: transparent;
        padding: 0;
        margin: 0 1;
        min-width: 0;
        color: white;
    }
    PathStep:hover {
        border-top: none;
        background: $primary-lighten-1;
        color: white;
    }
    """

    class Pressed(Message):
        def __init__(self, entry, *args, **kwargs):
            self.entry = entry
            super().__init__(*args, **kwargs)

    entry: Entry

    def __init__(self, entry: Entry, *args, **kwargs):
        self.entry = entry
        super().__init__(*args, **kwargs)
        self.can_focus = False

    async def on_mount(self) -> None:
        if self.entry.parent:
            self.update(await self.entry.get_label())
        else:
            self.update("🏠")  # Root node

    def on_click(self, _):
        self.post_message(self.Pressed(self.entry))


class Path(Static):
    """
    Widget to represent the current "path".
    """

    can_focus_children = False

    DEFAULT_CSS = """
    Path {
        layout: horizontal;
        #path {
            height: 1;
            layout: horizontal;
            width: 1fr;
        }
        Button.path {
            height: 1;
            border: none;
            width: 4;
            padding: 0;
            margin: 0 0;
            min-width: 4;
        }
        Button.path:hover {
            border: none;
        }
    }
    """

    class Selected(Message):
        def __init__(self, entry: Entry) -> None:
            self.entry = entry
            super().__init__()

    class NavigateBack(Message):
        pass

    class NavigateForward(Message):
        pass

    class NavigateUp(Message):
        pass

    path: reactive[Sequence[Entry]] = reactive(())

    def compose(self) -> ComposeResult:
        yield Static(id="path")
        # back_button = Button("🠈", classes="path", id="back")
        # back_button.can_focus = False
        # yield back_button
        # up_button = Button("🠝", classes="path", id="up")
        # up_button.can_focus = False
        # yield up_button
        # fwd_button = Button("🠊", classes="path", id="forward")
        # fwd_button.can_focus = False
        # yield fwd_button

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.can_focus = False

    def watch_path(self, _, path):
        with self.app.batch_update():
            static = self.query_one("#path")
            static.query("*").remove()
            for i, part in enumerate(path):
                if i > 0:
                    static.mount(Label("▶", classes="separator"))
                static.mount(PathStep(part, classes="part"))

    @on(PathStep.Pressed)
    def click_part(self, message: PathStep.Pressed):
        self.post_message(self.Selected(message.entry))

    @on(Button.Pressed, "#back")
    async def press_go_back(self) -> None:
        "Step back in history"
        self.post_message(self.NavigateBack())

    @on(Button.Pressed, "#forward")
    async def action_go_forward(self) -> None:
        "Step forward in history"
        self.post_message(self.NavigateForward())

    @on(Button.Pressed, "#up")
    async def action_go_up(self) -> None:
        "Go to parent"
        self.post_message(self.NavigateUp())
