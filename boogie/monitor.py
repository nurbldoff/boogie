"""
The "monitor" screen where the user can add stuff to be watched
for changes. It automatically finds monitoring widgets for the given
entries.
"""

from __future__ import annotations
import asyncio
from datetime import datetime, timedelta, timezone
import json
import sqlite3
from time import time
from typing import Any

from rich.text import Text
from textual import work
from textual.app import ComposeResult
from textual.screen import Screen, ModalScreen
from textual.containers import Container, Grid
from textual.widgets import (
    Footer,
    Static,
    ContentSwitcher,
    ListView,
    ListItem,
    TabbedContent,
    TabPane,
    Tabs,
    Button,
    SelectionList,
)
from textual.widgets.selection_list import Selection
from textual.css.query import NoMatches
from textual.binding import Binding
from textual.reactive import reactive
from textual.events import Key

from .entry import Entry
from .messages import NavigateTo
from .widgets.entry import EntryWidget, MonitorWidget
from .widgets.misc import PagedListView, BoogieHeader
from .widgets.modal import GetValueModal
from .plot import draw_time_axis, get_time_ticks
from .util import get_widget_class


class Monitors:

    """
    Database storage for monitors.

    Monitors are only updated, never overwritten.
    There's currently no way to load a previous version, but that
    could be added later.
    """
    
    def __init__(self, db_file: str):
        self.conn = sqlite3.connect(db_file)
        self.conn.row_factory = sqlite3.Row
        self.conn.execute("PRAGMA journal_mode = wal;")
        self.conn.execute("PRAGMA synchronous = NORMAL;")
        self.conn.execute("PRAGMA cache_size = -20000;")
        self.conn.execute("PRAGMA temp_store = MEMORY;")
        cursor = self.conn.cursor()
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS monitors (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                timestamp TEXT DEFAULT CURRENT_TIMESTAMP,
                key TEXT NOT NULL,
                name TEXT,
                data TEXT
            )
            """
        )
        self.conn.commit()

    def save(self, id_: int | None, key: str, name: str | None, data: Any):
        cursor = self.conn.cursor()
        if id_ is None:
            cursor.execute(
                "INSERT INTO monitors (key, name, data) VALUES (?, ?, ?)",
                (key, name, json.dumps(data)),
            )
        else:
            cursor.execute(
                "UPDATE monitors SET key = ?, name = ?, data = ? WHERE sid = ?",
                (key, name, json.dumps(data), id_),
            )
        self.conn.commit()

    def load(self, id_: int) -> Any:
        cursor = self.conn.cursor()
        name, data = next(cursor.execute(
            "SELECT name, data FROM monitors WHERE id = ?", (id_,)))
        return name, json.loads(data)

    def items(self, key: str):
        cursor = self.conn.cursor()
        query = """
            SELECT max(id), name, timestamp, data
            FROM monitors
            WHERE key = ?
            GROUP BY name
            ORDER BY id desc
        """
        for id_, name, timestamp, data in cursor.execute(query, (key,)):
            # UTC -> local time
            dt = datetime.fromisoformat(timestamp).replace(tzinfo=timezone.utc).astimezone()
            yield id_, name, dt, json.loads(data)


class MonitorScreen(Screen):

    """
    Screen managing any number of MonitorPanes, as tabs.
    Only the currently visible pane is "running".
    """
    
    DEFAULT_CSS = """
    MonitorScreen {
        height: 100%;
        width: 100%;
        background: $background;
        Tabs {
            display: none;
        }
    }
    """

    BINDINGS = [
        Binding("escape", "show_main", "Close"),
        ("a", "add_tab", "Add tab"),
        ("o", "open", "Open"),
        ("s", "save", "Save"),
        ("S", "save_all", "Save all"),
        ("r", "rename", "Rename"),
        ("c", "close", "Close"),
    ]

    def __init__(self, monitors: Monitors, key: str, *args, **kwargs):
        self.monitors = monitors
        self.key = key
        super().__init__(*args, **kwargs)
    
    def compose(self):
        yield BoogieHeader()
        self.title = "Monitor"
        with TabbedContent():
            yield MonitorPane("Default")
        yield Footer()

    def on_screen_suspend(self, msg):
        # Stop running; no point in updating non-visible widgets
        pane = self.get_current_pane()
        pane.running.clear()

    def on_screen_resume(self, msg):
        # Start running
        pane = self.get_current_pane()
        pane.running.set()

    def on_tabbed_content_tab_activated(self, msg):
        # Make sure only the visible pane is running
        old_pane = self.get_current_pane()
        for pane in self.query(MonitorPane):
            pane.pause()
        msg.pane.resume()

    def get_current_pane(self) -> MonitorPane:
        tc = self.query_one(TabbedContent)
        return tc.active_pane

    def add(self, entry: Entry) -> None:
        "Start monitoring the entry in the current pane"
        pane = self.get_current_pane()
        pane.add(entry)
        
    def action_show_main(self) -> None:
        self.app.switch_screen("main")
        
    def action_add_tab(self) -> None:
        tc = self.query_one(TabbedContent)
        pane = MonitorPane("New")
        self.query_one(Tabs).display = True
        tc.add_pane(pane)

    def action_save(self) -> None:
        pane = self.get_current_pane()
        self._save_pane(pane)
        self.notify(f"Saved monitor tab '{pane.name}'")

    def action_save_all(self) -> None:
        tc = self.query_one(TabbedContent)
        for pane in tc.query(MonitorPane):
            self._save_pane(pane)
        self.notify("Saved all monitor tabs")

    def _save_pane(self, pane: MonitorPane):
        data = []
        for listitem in pane.query_one("#monitors").children:
            wrapper, = listitem.children
            entry = wrapper.entry
            widget = wrapper.get_widget_by_id(wrapper.current)
            data.append({
                "path": entry.path_str(),
                "widget": type(widget).__name__  # Current widget class
            })
            self.monitors.save(None, data=data, name=pane.name, key=self.key)
        
    def action_open(self) -> None:

        async def maybe_load(ids: list[str] | None) -> None:
            if ids:
                for id in ids:                    
                    name, data = self.monitors.load(id)
                    tc = self.query_one(TabbedContent)
                    pane = MonitorPane(name)
                    self.query_one(Tabs).display = True
                    tc.add_pane(pane)
                    for item in data:
                        entry = await self.app.get_entry(item["path"].split(":"))
                        pane.add(entry, widget=item["widget"])

        items = self.monitors.items(self.key)
        self.app.push_screen(
            LoadMonitorModal("Load", items),
            maybe_load
        )

    def action_close(self) -> None:
        pane = self.get_current_pane()
        self.query_one(TabbedContent).remove_pane(pane.id)
        
    def action_rename(self) -> None:

        pane = self.get_current_pane()
        tab = self.query_one(TabbedContent).get_tab(pane.id)
        
        def maybe_rename(new_name: str | None) -> None:
            if new_name:
                pane.name = new_name
                tab.update(new_name)

        self.app.push_screen(
            GetValueModal("New name", initial=pane.name),
            maybe_rename
        )


class MonitorPane(TabPane):

    "A tab pane displaying some monitor widgets"

    DEFAULT_CSS = """
    MonitorPane {
        #monitors {
            overflow-y: auto;
            display: none;
            hatch: right $background-lighten-1;
            background: transparent;
            ListItem {
                background: transparent;
                border-bottom: solid $background-lighten-2;
            }
            ListItem.-highlight {
            }
            ListItem.-highlight MonitorWidgetWrapper {
                border-left: outer white;
            }
        }
        #time-axis {
            height: 2;
            width: 100%;
            dock: bottom;
            margin-bottom: 1;
            padding: 0 1;
            display: none;
        }
        #placeholder {
            width: 1fr;
            height: 1fr;
            align: center middle;
            text-align: center;
            background: $background;
            hatch: right $background-lighten-1;
            color: $secondary;
        }
        #placeholder Static {
            width: auto;
        }
    }
    """
    
    BINDINGS = [
        ("plus", "zoom_in", "Zoom in"),
        ("minus", "zoom_out", "Zoom out"),
        ("delete", "remove", "Remove"),
        Binding("shift+up", "move_up", "Move up", show=False),
        Binding("shift+down", "move_down", "Move down", show=False),
        (".", "cycle", "Cycle"),
    ]

    time_scale_visible: reactive[bool] = reactive(False, init=False)
    zoom: reactive[int] = reactive(-1, init=False)  # Time range zoom

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.running = asyncio.Event()  # Update trends etc only when this is set
        self.running.clear() 

    @property
    def name(self):
        return str(self._title)

    @name.setter
    def name(self, name):
        self._title = name
    
    def compose(self):
        with Container(id="placeholder"):
            yield Static(
                "Add things (e.g. attributes) to be monitored."
                + "\nPress 'm' when visiting e.g. an attribute."
            )
        yield PagedListView(id="monitors")
        yield Static(id="time-axis")
        
    def on_mount(self):
        self.query_one("#monitors").focus()
        
    def action_zoom_in(self) -> None:
        self.zoom -= 1
        self._run()

    def action_zoom_out(self) -> None:
        self.zoom += 1
        self._run()

    def _move_current_item(self, amount: int) -> None:
        list_view = self.query_exactly_one(ListView)
        index = list_view.index
        w = list_view.children[index]
        list_view.children._remove(w)
        new_index = index + amount
        list_view.children._insert(new_index, w)
        list_view.index = new_index
        list_view.refresh(layout=True)

    def action_move_up(self) -> None:
        self._move_current_item(-1)

    def action_move_down(self) -> None:
        self._move_current_item(1)

    def action_cycle(self) -> None:
        item = self.query_exactly_one(ListView).highlighted_child
        if item:
            item.query_one(MonitorWidgetWrapper).cycle()
        # TODO this is a little ugly, but if we redraw stuff too quickly
        # the widgets don't seem to be mounted yet.
        self.call_after_refresh(self._run)

    def action_remove(self) -> None:
        list_view = self.query_one(ListView)
        item = list_view.highlighted_child
        index = list_view.index
        if item:
            item.remove()
        else:
            self.query_one("#placeholder").display = True

    def check_action(self, action: str, _):
        if action.startswith("zoom_"):
            return self.time_scale_visible
        return True

    def watch_time_scale_visible(self, _, value: bool):
        time_axis = self.query_one("#time-axis")
        time_axis.display = value
        self.refresh_bindings()  # Make sure bindings are updated

    def on_list_view_selected(self, message):
        item = message.item
        entry = item.children[0].entry
        self.post_message(NavigateTo(entry))

    def time_window(self, width: int) -> tuple[datetime, datetime]:
        scale = 2**self.zoom
        now = datetime.fromtimestamp(int(time() / scale) * scale).astimezone(tz=None)
        return (
            now - timedelta(seconds=2 * width * scale - 1),
            now + timedelta(seconds=1),
        )

    def pause(self):
        self.running.clear()

    def resume(self):
        self.running.set()
    
    @work
    async def add(self, entry: Entry, widget: str | None = None) -> None:
        monitor_class = get_widget_class(MonitorWidget, type(entry))
        if monitor_class:
            widget_classes = await monitor_class.get_widgets(entry)
            if not widget_classes:
                self.app.notify("No monitor widget available :(")
                return
            try:
                self.query_one("#placeholder").display = False
            except NoMatches:
                pass
            monitors = self.query_one(ListView)
            monitors.display = True
            widgets = [
                widget_class(id=f"widget-{i}")
                for i, widget_class in enumerate(widget_classes)
            ]
            if widget:
                for i, w in enumerate(widget_classes):
                    if w.__name__ == widget:
                        break
                else:
                    i = 0
                initial = f"widget-{i}"
            else:
                initial = "widget-0"
            wrapper = MonitorWidgetWrapper(*widgets, initial=initial)
            item = ListItem(wrapper)
            await monitors.append(item)
            wrapper.set_entry(entry)
            item.focus()
            self._run()
        else:
            self.app.notify(
                f"Oops, no monitor widget available for {type(entry)}!",
                severity="warning",
            )

    def watch_zoom(self, _, zoom) -> None:
        self._run()

    def _get_trend_widgets(self):
        monitor_widgets = self.query(MonitorWidget)
        return [
            w
            for w in monitor_widgets
            if w.parent and w.parent.visible_content == w and hasattr(w, "draw")
        ]
            
    def _update_trends(self) -> None:
        trend_widgets = self._get_trend_widgets()
        if trend_widgets:
            time_window = self.time_window(self.size.width - 2)
            time_ticks = get_time_ticks(*time_window)
            time_axis = self.query_one("#time-axis", expect_type=Static)
            for widget in trend_widgets:
                widget.draw(time_window, time_ticks)
            axis = draw_time_axis(time_window, time_ticks, time_axis.content_size.width)
            time_axis.update(Text.from_ansi(axis))
            self.time_scale_visible = True
        else:
            self.time_scale_visible = False

    @work(exclusive=True, group="run")
    async def _run(self, timestep=1) -> None:
        while True:
            await self.running.wait()
            t0 = time()
            # Update trends once a second
            self._update_trends()
            dt = time() - t0
            await asyncio.sleep(timestep - dt)


class MonitorWidgetWrapper(EntryWidget, ContentSwitcher):
    """
    General wrapper widget for a Monitor widget
    TODO Container?
    """

    DEFAULT_CSS = """
    MonitorWidgetWrapper {
        height: auto;
        width: 1fr;
        border-left: outer transparent;
        margin-right: 1;
    }
    """

    def __init__(self, *widgets: MonitorWidget, initial: str,
                 **kwargs):
        super().__init__(*widgets, **kwargs, initial=initial)

    async def entry_changed(self, entry):
        for widget in self.children:
            widget.set_entry(self.entry)

    def action_remove(self):
        self.remove()

    def cycle(self):
        widget = self.get_widget_by_id(self.current)
        index = self.children.index(widget)
        next_index = (index + 1) % len(self.children)
        next_widget = self.children[next_index]
        self.current = next_widget.id


class LoadMonitorModal(ModalScreen[tuple[list[str], int]]):

    "Dialog for loading monitors from DB"

    DEFAULT_CSS = """
    LoadMonitorModal {
        align: center middle;
        .frame {
            width: 75%;
            height: 75%;
            border: panel $secondary;
            layout: grid;
            grid-size: 2 4;
            grid-rows: auto 1fr auto auto;
            Label, Input, SelectionList {
                column-span: 2;
            }
            Button {
                width: 1fr;
            }
        }
    }
    """

    def __init__(self, title: str, items: list[str], *args, **kwargs):
        self._title = title
        self._items = items
        super().__init__(*args, **kwargs)

    def compose(self):
        with Grid(classes="frame") as g:
            g.border_title = self._title
            items = [
                Selection(f"[b]{name}[/b] {timestamp} [{len(data)}]", id_)
                for id_, name, timestamp, data in self._items
            ]
            yield SelectionList[str](*items)
            yield Button("Load", id="load", variant="primary")
            yield Button("Cancel", id="cancel", variant="error")

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "load":
            names = self.query_one(SelectionList).selected
            self.dismiss(names)
        else:
            self.dismiss()

    def on_key(self, event: Key) -> None:
        if event.key == "escape":
            event.prevent_default()
            self.dismiss(False)
        if event.key == "enter":
            event.prevent_default()
            sl = self.query_one(SelectionList)
            sel = sl.get_option_at_index(sl.highlighted)
            self.dismiss([sel.value])
