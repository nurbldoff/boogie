"""
Cache for async functions. For performance reasons, we
want to reuse info that is used often but doesn't change
very much. E.g database stuff.

Centralizing the cache allows us to clear the entire cache
at once. So, in order to support the "reload" feature, use
the "cached" decorator from here.
"""

import asyncio
from collections import OrderedDict
from collections.abc import Mapping
from functools import _make_key, wraps
import logging
from typing import Any


logger = logging.getLogger(__name__)

# Global cache
cache: Mapping[str, Any] = OrderedDict()

cache_lock = asyncio.Lock()


def clear_cache():
    "Clear the global cache"
    cache.clear()


def cached(func):
    """Caching decorator, supports async functions."""

    def make_key(args, kwargs):
        """Create a unique key for a function call with the given arguments"""
        return hash((_make_key(args, kwargs, False), id(func)))

    async def run_and_cache(func, args, kwargs):
        """Run func with the specified arguments and store the result
        in cache."""
        result = await func(*args, **kwargs)
        cache[make_key(args, kwargs)] = result
        if len(cache) > 128:  # Hard-coded max cache size
            cache.popitem(False)
        return result

    @wraps(func)
    def decorator(*args, **kwargs):
        key = make_key(args, kwargs)
        if key in cache:
            # Some protection against duplicating calls already in
            # progress: when starting the call cache the future, and if
            # the same thing is requested again return that future.
            if isinstance(cache[key], asyncio.Future):
                return cache[key]
            else:
                f = asyncio.Future()
                f.set_result(cache[key])
                return f
        else:
            task = asyncio.Task(run_and_cache(func, args, kwargs))
            cache[key] = task
            return task

    return decorator
