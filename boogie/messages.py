from textual.message import Message

from .entry import Entry


class NavigateTo(Message):
    def __init__(self, entry: Entry, store: bool = True):
        self.entry = entry
        self.store = store
        super().__init__()


class Reload(Message):
    pass


class AddToMonitor(Message):
    def __init__(self, entry: Entry):
        self.entry = entry
        super().__init__()
