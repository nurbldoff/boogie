import asyncio
from random import random
from time import time

import pytest
import tango

from boogie.plugins.tango.listener import AttributeTracker, Listener


class MockDeviceProxy:
    def __init__(self, devicename, allow_subscription=False):
        self.name = devicename
        self.allow_subscription = allow_subscription
        self._id = 0

    async def read_attribute(self, attribute, extract_as=None):
        da = tango.DeviceAttribute()
        da.name = attribute
        da.value = random()
        da.timestamp = tango.TimeVal(time())
        da.quality = tango.AttrQuality.ATTR_VALID
        return da

    async def subscribe_event(self, attribute, event_type, callback, extract_as=None, stateless=False):
        if self.allow_subscription or event_type == tango.EventType.ATTR_CONF_EVENT:
            self._id += 1
            return self._id
        else:
            raise tango.DevFailed(tango.DevError())


async def get_device_proxy_no_subs(devname):
    return MockDeviceProxy(devname)


@pytest.mark.asyncio
async def test_attribute_tracker_polled_immediate_read():
    attrname = "tango://my.tango.host:10000/my/test/device/attr_a"
    queue = asyncio.Queue()
    tracker = AttributeTracker(
        attrname,
        queue,
        read_period=0.1,
        get_device_proxy=get_device_proxy_no_subs,
    )
    tracker.start()

    full_name, dev_attr = await queue.get()
    assert full_name == "tango://my.tango.host:10000/my/test/device/attr_a"

    # No more data
    assert queue.empty()

    await tracker.stop()


async def test_listener_basic():
    listener = Listener()

    devname = "tango://my.tango.host:10000/my/test/device"
    attrnames = ["attr_a", "attr_b", "attrc"]
    attributes = [f"{devname}/{attr}" for attr in attrnames]

    listener.start()
    events = []
    async for attr, data in listener.listen(attributes):
        # We should get one event per attribute, initially
        events.append(attr)
        if len(events) == 3:
            break
    assert len(set(events)) == 3
