"""
These test run the behavior of the app, with mock plugins that don't
require any external communication.
"""

import asyncio
from copy import deepcopy


from . import test_plugin


async def test_app_basic(app):
    async with app.run_test() as pilot:
        await pilot.pause()
        tree = app.query_one("TabPane#root1").query_one("CustomTree")
        assert tree.has_focus
        tree.select_node(None)
        node = tree.cursor_node
        assert "Apa1" in node.label


async def test_app_navigation(app):
    async with app.run_test() as pilot:
        await pilot.pause()
        tree = app.query_one("TabPane#root1").query_one("CustomTree")
        tree.select_node(None)
        await pilot.press("down")
        node = tree.cursor_node
        assert "Bepa1" in node.label
        await pilot.press("right")
        await pilot.press("right")
        node = tree.cursor_node
        assert "foo1" in node.label


async def test_app_tree_goes_to_node(app):
    async with app.run_test() as pilot:
        path = ["Root1", "Cepa1", "bar1"]
        await app.navigate_to(await app.get_entry(path))
        await pilot.pause()
        await asyncio.sleep(1)

        tree = app.query_one("TabPane#root1").query_one("CustomTree")
        node = tree.cursor_node
        assert "bar1" in node.label


async def test_app_tree_focused_on_tab_change(app):
    async with app.run_test() as pilot:
        await pilot.pause()
        await pilot.press("2")
        await pilot.pause()
        tree = app.query_one("TabPane#root2").query_one("CustomTree")
        assert tree.has_focus


async def test_app_tree_initial_path(app):
    async with app.run_test() as pilot:
        path = ["root1", "Bepa1", "baz1"]
        await app.navigate_to(await app.get_entry(path))
        await pilot.pause()
        tree = app.query_one("TabPane#root1").query_one("CustomTree")
        assert tree.has_focus
        node = tree.cursor_node
        assert "baz1" in node.label
        assert app.query_one("#test1-level2-details")


async def test_app_details_widget(app):
    async with app.run_test() as pilot:
        path = ["root1", "Bepa1", "foo1"]
        await app.navigate_to(await app.get_entry(path))

        await pilot.pause()
        w = app.query_one("#test1-level2-details")
        assert "foo1" in w.render()._renderable


async def test_app_details_list_widget(app):
    async with app.run_test() as pilot:
        path = ["root2", "Bepa2"]
        await app.navigate_to(await app.get_entry(path))
        await pilot.pause()
        table = (
            app.query_one("TabPane#root2")
            .query_one("#details-list")
            .query_one("DataTable")
        )
        # Details list automatically gets focus
        assert table.has_focus
        assert table.get_row_at(1) == [
            "bar2",
        ]


async def test_app_details_list_widget_selected(app):
    async with app.run_test() as pilot:
        path = ["root2", "Bepa2", "bar2"]
        await app.navigate_to(await app.get_entry(path))
        await pilot.pause()
        table = (
            app.query_one("TabPane#root2")
            .query_one("#details-list")
            .query_one("DataTable")
        )
        assert table.cursor_row == 1


async def test_app_details_list_widget_navigation(app):
    async with app.run_test() as pilot:
        await app.navigate_to(await app.get_entry(["root2", "Bepa2"]))
        await pilot.pause()
        await pilot.press("enter")
        await pilot.pause()
        w = app.query_one("#test2-level2-details")
        assert "foo2" in w.render()._renderable


async def test_app_details_list_widget_filter(app):
    async with app.run_test() as pilot:
        await app.navigate_to(await app.get_entry(["root2", "Bepa2"]))
        await pilot.pause()
        table = (
            app.query_one("TabPane#root2")
            .query_one("#details-list")
            .query_one("DataTable")
        )
        assert table.row_count == 3
        await pilot.press("%", "b", "a", "enter")
        await pilot.pause()

        # "foo2" should be filtered out here
        assert table.row_count == 2
        assert table.get_row_at(0)[0] == "bar2"
        assert table.get_row_at(1)[0] == "baz2"


async def test_app_details_list_widget_opens_details(app):
    async with app.run_test() as pilot:
        await app.navigate_to(await app.get_entry(["root2", "Bepa2"]))
        await pilot.pause()
        await pilot.press("down")
        await pilot.press("enter")
        await pilot.pause()
        pane = app.query_one("TabPane#root2")
        w = pane.query_one("#details").query_one("#test2-level2-details")
        assert "bar2" in w.render()._renderable


async def test_app_history_stored(app):
    async with app.run_test() as pilot:
        await pilot.pause()
        await pilot.press("down", "right", "down", "right", "enter")
        pane = app.query_one("TabPane#root1 Browser")
        assert pane.entry.name == "foo1"
        history = list(app.history.items("abc123:10000"))
        assert len(history) == 1


async def test_app_history_back(app):
    async with app.run_test() as pilot:
        await pilot.pause()
        await pilot.press("down", "down", "right", "enter")  #
        await pilot.press("right", "down", "enter")
        pane = app.query_one("TabPane#root1 Browser")
        assert pane.entry.name == "bar1"
        history = list(app.history.items("abc123:10000"))
        assert len(history) == 2

        # Go back
        await pilot.press("ctrl+left")
        history = list(app.history.items("abc123:10000"))
        # Going back does not add a history entry
        print(history)
        assert pane.entry.name == "Bepa1"
        assert len(history) == 2


async def test_app_history_forward(app):
    async with app.run_test() as pilot:
        await pilot.pause()
        await pilot.press("down", "right", "enter")  # Apa
        await pilot.pause()
        await pilot.press("right", "down", "enter")  # bar1

        await pilot.press("ctrl+left")

        # Go forward
        await pilot.press("ctrl+right")
        await pilot.pause()

        pane = app.query_one("TabPane#root1 Browser")
        assert pane.entry.name == "bar1"
        history = list(app.history.items("abc123:10000"))
        # Going back does *not* add a history entry
        assert len(history) == 2


async def test_app_history_up(app):
    async with app.run_test() as pilot:
        path = ["root1", "Bepa1", "baz1"]
        await app.navigate_to(await app.get_entry(path))
        tree = app.query_one("TabPane#root1").query_one("CustomTree")
        await pilot.pause()
        assert tree.has_focus
        node = tree.cursor_node
        assert "baz1" in node.label
        history = list(app.history.items("abc123:10000"))
        assert len(history) == 1

        # Go up (to the parent entry)
        await pilot.press("ctrl+up")
        pane = app.query_one("TabPane#root1 Browser")
        assert pane.entry.name == "Bepa1"
        history = list(app.history.items("abc123:10000"))
        # Going up does *not* add a history entry
        assert len(history) == 1


async def test_app_reload(app, monkeypatch):
    async with app.run_test() as pilot:
        path = ["root2", "Bepa2", "bar2"]
        await app.navigate_to(await app.get_entry(path))
        await pilot.pause()

        # Remove part of the data so that the end of the path is not valid
        new_data = deepcopy(test_plugin.entry.data)
        del new_data["Root2"]["Bepa2"]["bar2"]
        monkeypatch.setattr(test_plugin.entry, "data", new_data)

        # Trigger a reload
        await pilot.press("f5")
        await pilot.pause()

        # Check that we went up the path
        pane = app.query_one("TabPane#root2 Browser")
        assert pane.entry.name == "Bepa2"

        # Check that we updated the widgets
        details = pane.query_one("#details")
        assert details.display is False
