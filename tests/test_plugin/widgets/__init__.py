from textual.widgets import Static

from boogie.widgets.details import Details, DetailsList
from ..entry import Test1Level2Entry, Test2Level1Entry, Test2Level2Entry


class Test1Level2Details(Details[Test1Level2Entry]):
    def compose(self):
        yield Static(id="test1-level2-details")

    async def entry_changed(self, entry):
        self.query_one(Static).update(entry.name)


class Test2Level1Details(DetailsList[Test2Level1Entry]):
    COLUMNS = ("A",)

    async def get_row(self, entry):
        return (entry.name,), {}


class Test2Level2Details(Details[Test2Level2Entry]):
    DEFAULT_CSS = """
    Test2Level2Details {
        border: round $primary;
    }
    """

    def compose(self):
        yield Static(id="test2-level2-details")

    async def entry_changed(self, entry):
        self.query_one(Static).update(entry.name)
