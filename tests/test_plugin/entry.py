from dataclasses import dataclass

from boogie.entry import Entry

data = {
    "Root1": {
        "Apa1": {
            "foo1": None,
            "bar1": None,
            "baz1": None,
        },
        "Bepa1": {
            "foo1": None,
            "bar1": None,
            "baz1": None,
        },
        "Cepa1": {
            "foo1": None,
            "bar1": None,
            "baz1": None,
        },
    },
    "Root2": {
        "Apa2": {
            "foo2": None,
            "bar2": None,
            "baz2": None,
        },
        "Bepa2": {
            "foo2": None,
            "bar2": None,
            "baz2": None,
        },
        "Cepa2": {
            "foo2": None,
            "bar2": None,
            "baz2": None,
        },
    },
}


# Tree 1


@dataclass(frozen=True, repr=False, eq=False)
class Test1RootEntry(Entry):
    parent: Entry | None = None
    name: str = "Root1"

    async def get_children(self):
        return [await self.get_child(name) for name in data[self.name]]

    async def get_child(self, name):
        if name in data[self.name]:
            return Test1Level1Entry(self, name)
        raise KeyError


@dataclass(frozen=True, repr=False, eq=False)
class Test1Level1Entry(Entry):
    parent: Test1RootEntry
    name: str

    async def get_children(self):
        return [await self.get_child(x) for x in data[self.parent.name][self.name]]

    async def get_child(self, name):
        if name in data[self.parent.name][self.name]:
            return Test1Level2Entry(self, name)
        raise KeyError


@dataclass(frozen=True, repr=False, eq=False)
class Test1Level2Entry(Entry):
    parent: Test1Level1Entry
    name: str
    leaf_node = True


# Tree 2


@dataclass(frozen=True, repr=False, eq=False)
class Test2RootEntry(Entry):
    parent: Entry | None = None
    name: str = "Root2"

    async def get_children(self):
        return [await self.get_child(name) for name in data[self.name]]

    async def get_child(self, name):
        if name in data[self.name]:
            return Test2Level1Entry(self, name)
        raise KeyError


@dataclass(frozen=True, repr=False, eq=False)
class Test2Level1Entry(Entry):
    parent: Test2RootEntry
    name: str
    leaf_node = True

    async def get_children(self):
        return [await self.get_child(x) for x in data[self.parent.name][self.name]]

    async def get_child(self, name):
        if name in data[self.parent.name][self.name]:
            return Test2Level2Entry(self, name)
        raise KeyError


@dataclass(frozen=True, repr=False, eq=False)
class Test2Level2Entry(Entry):
    parent: Test2Level1Entry
    name: str
