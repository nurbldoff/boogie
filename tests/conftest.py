import pytest
from pytest import StashKey, CollectReport

from boogie.app import Boogie
from . import test_plugin

phase_report_key = StashKey[dict[str, CollectReport]]()


@pytest.hookimpl(wrapper=True, tryfirst=True)
def pytest_runtest_makereport(item, call):
    # execute all other hooks to obtain the report object
    rep = yield

    # store test results for each phase of a call, which can
    # be "setup", "call", "teardown"
    item.stash.setdefault(phase_report_key, {})[rep.when] = rep

    return rep


@pytest.fixture(scope="function")
async def pilot(request):
    """
    Create an app with the test plugin and in-memory database.
    The fixture returns a test pilot instance for the app, for
    remote control.

    Finally, if the test failed, a screenshot of the current
    state of the app is saved. This can be helpful for figuring
    out the problem.
    """
    app = Boogie(
        history_db_path=":memory:",
        bookmarks_db_path=":memory:",
        plugins=[test_plugin],
    )
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()
        yield pilot

        # await pilot.wait_for_scheduled_animations()
        # report = request.node.stash[phase_report_key]
        # if ("call" not in report) or report["call"].failed:
        #     # Test appears to have failed; let's save a screenshot
        #     test_name = request.node.name
        #     image_path = f"/tmp/boogie_{test_name}.svg"
        #     app.save_screenshot(image_path)
        #     print(f"*** App screenshot available at {image_path} ***")


@pytest.fixture(scope="function")
def app(request):
    """
    Create an app with the test plugin and in-memory database.
    The fixture returns a test pilot instance for the app, for
    remote control.

    Finally, if the test failed, a screenshot of the current
    state of the app is saved. This can be helpful for figuring
    out the problem.
    """
    app = Boogie(
        history_db_path=":memory:",
        bookmarks_db_path=":memory:",
        plugins=[test_plugin],
    )
    yield app
