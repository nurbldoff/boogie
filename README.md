# 🪩 Boogie

*Boogie* is a terminal based Tango (https://www.tango-controls.org/) control system browser that takes inspiration from the standard tool "Jive".

![Screenshot](screenshot.png)

Built with Python (>=3.10), PyTango (>=9.4) and Textual (>=1.0).

Development status: *Beta* - The basics are somewhat stable, but things keep changing and expect bugs.


## Installation

The usual ways should work, e.g. cloning this repo and then:

    $ pip install


## Usage

Start with

    $ boogie

or, to talk to another Tango host than the default:

    $ TANGO_HOST=some.tango.host:10000 boogie

Press `F10` or `Ctrl-Q` to quit. For more details, consult the builtin "help" screen opened by pressing `F1`.

Boogie expects a terminal which supports full (24 bit) color, or it may look a bit strange. Most modern terminal emulators will support this, but it's possible that you will have to tweak some terminal settings, in particular when running over SSH. Also see https://textual.textualize.io/FAQ

Boogie makes use of some unicode and "emoji" characters that might not be present in all terminal fonts. If things look weird, please check if you can upgrade your font, or else create an issue.

Thanks to the `textual-serve` package (not installed by default), Boogie can also be accessed via a web browser. Start it with `python -m boogie.serve`. Take care, as there is no security and anyone that can access your server is able modify your control system!


## Utilities

Boogie contains some smaller utility applications. For now there's only one: the `trend`.
It plots one or more (scalar) attribute values over time, much like the "monitor" 
feature in the main program (or like `taurus trend`).

It's accessed via a "subcommand":

    $ boogie trend sys/tg_test/1/double_scalar sys/tg_test/1/short_scalar

More subcommands may be added in the future. Use `boogie --help` to see what's available.


## Troubleshooting

Since Boogie is not yet packaged properly, after git-pulling it you should always `pip install` as the dependencies may have changed.

Obviously, check your `TANGO_HOST` setting, and that you have network access to all the hosts involved.

Boogie saves its history, bookmarks etc in SQLite databases, located in a standardized place under your home directory. Exactly where will depend on your OS and desktop environment, and we use a library to figure this out. Under Linux (at least mine) it's e.g. `~/.local/share/Boogie/history.db`. The format of these could change and in that case you may have to remove files in order for Boogie to work. On the other hand, these files are fairly easy to manipulate directly, e.g. using the `sqlite3` command line tool.


## Development

Install in local mode so you can edit the files in place, e.g.:

    $ python -m venv env
    $ . env/bin/activate
    $ pip install -e .

To run in "development" mode, start a `textual console` in another terminal, and then:

    $ textual run -c boogie --dev

Now logging and all `print` statements arrive on the console, very useful for debugging. Recommend using `textual console -x SYSTEM -x EVENT` to exclude the most verbose logging of Textual internals.

For documentation on Textual, see https://textual.textualize.io/

It is often convenient to control the intial path. This can be done using a command line argument, like so:

    $ boogie -p server:tangotest:test:tangotest:sys/tg_test/1


### Testing

There are some unit tests for the basic application, that don't require a Tango database.

These tests can be run like this:

    $ TEXTUAL=devtools pytest tests/test_app.py

The `TEXTUAL` env var enables connecting to the `textual console` to get debug logging during tests.


## Features (at various stages of "working")
- Browsing by server, class, device, alias, host
- Showing device state, status, info
- Listing device attributes, properties, commands
- Reading attributes, properties, various info
- Attribute configuration
- Running commands
- Creating, deleting, editing device properties
- Deleting devices, servers
- Live device state, status
- Attribute "live" monitoring
- Saving monitor configs
- Writing attributes
- Persistent navigation history
- Custom panels for device classes
- Bookmarks
- Search
- Polling configuration
- Logging configuration
- Plugin system

## Planned features
- Creating things (servers, devices...)
- Plugins, e.g. Sardana, Panic, HDB++...

## Maybe features
- Saving settings


## Under the hood

### UI

```
   | Foo |  Bar  Baz
  +--------+----------------------+
  |            Path               |
  +--------+----------------------+
  |        |       Banner         |
  |        +----------------------+
  |        |                      |
  |        |       Listing        |
  |  Tree  |                      |
  |        +----------------------|
  |        |                      |
  |        |       Details        |
  |        |                      |
  +--------+----------------------+
```

The UI layout is as above. At the top there's a tab bar, showing all the available "plugins". Below that is the "path" bar, that tells you the location of the thing currently being inspected. The Tree on the left shows a hierarchical view of the entries available.

The above parts are always present. The three stacked containers on the right side however are all optional, and may or may not be shown depending on the current path.

- The "Banner" is intended to be a compact widget, that may be implemented for any given entry type. If there is a banner implementation for an entry in the path it will be shown. If there are several, the last one in the path is shown.

- The "Listing" is useful for entries with multiple children, but which does not fit well in the tree. For example there might be lots of children, or more info to show per child. Then a list view is more appropriate. Implementing a `DetailsList` widget for an entry type accomplishes this. It has some built in functionality, such as filtering.

- Finally the `Details` widget is probably the main thing to implement for most entry types. If the currently selected entry (i.e. what is last in the path) has a Details widget implemented, it will be shown here. It can be completely custom.

### Entries

Boogie works a little bit like a web browser, in the sense that it always keeps track of the current location, a.k.a the "Path" (seen in the widget at the top). It can be e.g. `"Server" (the current tab) -> "TangoTest" (server) -> "test" (instance) -> "TangoTest" (class) -> "sys/tg_test/1" (device)`. It tells you where in the hierarchy you currently are. The main part of this hierarchy is visible in the tree widget on the left. Selecting something there (e.g. by pressing enter och clicking with the mouse) causes the Path to be updated.

Each entry is an object that represents a "level" in the hierarchy, for example a Tango device. It can also be more of an abstract thing like "the properties of a device". This is a convenience thing, as it's used by the UI. Each Entry can have any number of other Entries as children, forming a tree structure. Entries contain all the code for communicating with the actual control system.

The Path is in fact just the current Entry, with its chain of "ancestor" entries; parent, grandparent and so on up until the root entry e.g. "Server".

This entry hierarchy can be seen as the "data model" of the application. Like Jive, there are multiple different trees representing the same database (e.g. "Device", "Class", and so on). Plugins can implement new trees, see below.

There is a history kept of the Path as it changes, so it's possible to go back to previous paths visited.

It's also possible to jump around in the tree using search. There's also an event called `NavigateTo` that can be sent by widgets, in order to navigate the application somewhere else.

### Widgets

Apart from the entry tree on the left, a selected entry may also be represented by widgets that get shown on the right. Most entries are shown in the tree, but then there may be "leaf" entries whose children are not shown in the tree. Widgets are just Textual widgets (inheriting a special baseclass, see below) and can display any information, as well as allow interactions.

A widget generally represents an Entry. There is a widget base class called `EntryWidget` which should be used for all such widgets. It is a "container" (generic) class that contains an `Entry` subclass. The application will use this information to automatically find a widget class, based on the current entry. In the basic case you just need to implement the Textual `compose` method to build your widget, and the `entry_changed` method to hook it up with data whenever its Entry changes.

There are two main variants of `EntryWidget`; `Details` and `DetailsList`. These base classes are respectively used for entries that represent a single "thing" (like `TangoDevice`), and entries that represent collections (like `TangoDeviceAttributes`). These widgets provide some default functionality that should probably be used, to keep the interface consistent. But anything could of course be overridden and customized if really necessary.


### Plugins

Boogie consists of the basic "app" which doesn't do much by itself, and a set of "plugins" that contain all the "business logic". So far, "plugins" are mostly a way to compartmentalize the code in separate subpackages.

A "plugin" consists of one or more Entry hierarchies, together with a set of related widgets. Each hierarchy gets displayed as a "tab" in the main UI. 

The Tango plugin is included with Boogie, and other plugins can reference it. Other plugins can be installed as separate packages (not really avaliable until there's a main "boogie" package). The "sardana" plugin has been removed for now as it was rather buggy, but will return as a separate package.
